process.env.NODE_ENV = 'production'
process.env.BUILD_ENV = 'development'

var ora = require('ora')
var rm = require('rimraf')
var path = require('path')
var chalk = require('chalk')
var webpack = require('webpack')
var config = require('../config')
var webpackConfig = require('../webpack/webpack.dev.conf')

var spinner = ora('building for development server...')
console.log(chalk.green(
  '  Development build.\n'
))
spinner.start()

rm(path.join(config.deploy.assetsRoot, config.deploy.assetsSubDirectory), err => {
  if (err) throw err
  webpack(webpackConfig, function (err, stats) {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      process.exit(1)
    }

    console.log(chalk.cyan(
      '  Build complete.\n' +
      '  All the development files are located in the "Public" folder. \n'
    ))
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ))
  })
})
