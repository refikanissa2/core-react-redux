module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
    node: true,
    jquery: true
  },
  extends: 'eslint:recommended',
  parserOptions: {
    ecmaVersion: 6,
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true
    },
    sourceType: 'module'
  },
  plugins: ['react'],
  rules: {
    'prefer-arrow-callback': 1,
    semi: ['error', 'always'],
    strict: 0,
    indent: [2, 2],
    quotes: [1, 'single'],
    // "linebreak-style": ["error", "windows"],
    'key-spacing': [0, { align: 'value' }],
    'no-underscore-dangle': 0,
    'no-unused-vars': 0,
    'no-console': 0,
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error'
  },
  settings: {
    react: {
      pragma: 'React', // Pragma to use, default to "React"
      version: '15.0', // React version, default to the latest React stable release
      flowVersion: '0.53' // Flow version
    },
    globals: {
      jQuery: true,
      $: true
    }
  }
};
