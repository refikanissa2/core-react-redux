/**
 * Webpack configuraton for local development
 */
var path = require('path')
var utils = require('./tools/utils')
var webpack = require('webpack')
var config = require('./config')
var merge = require('webpack-merge')
var baseWebpackConfig = require('./webpack/webpack.base.conf')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')

module.exports = merge(baseWebpackConfig, {
  // devtool: '#cheap-module-eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': config.dev.env
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new ExtractTextPlugin({
      filename: '[name].css'
    }),
    new HtmlWebpackPlugin({
      favicon : 'favicon.ico',
      filename: 'index.html',
      template: 'index.html',
      inject: true
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module, count) {
        // any required modules inside node_modules are extracted to vendor
        return (
          module.resource &&
          /\.js$/.test(module.resource) &&
          module.resource.indexOf(
            path.join(__dirname, './node_modules')
          ) === 0
        )
      }
    }),
    // optimize momentjs
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de|fr|hu/),
    new FriendlyErrorsPlugin()
  ],
  node: {
    fs: "empty"
  },
  devServer: {
    contentBase: path.join(__dirname, config.dev.assetsPublicPath),
    proxy: {
      "/api": {
        target: "https://titan.syariahbtpn.com/",
        secure: false,
        changeOrigin: true,
        pathRewrite: {"^/api" : ""}
      }
    },
    headers: {
      'Access-Control-Allow-Headers': 'X-CSRF-TOKEN',
    },
    hot: true,
    inline: true,
    historyApiFallback: true,
    port: config.dev.port,
    host: 'localhost'
  }
})

