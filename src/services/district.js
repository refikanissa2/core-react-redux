import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const districtService = {
  list,
  add,
  detail,
  update,
  remove,
  getAll
};

function list(option = {}) {
  const {size, page, sortBy, code, name, direction} = option;
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };
  const params = {
    size: size ? size : 10,
    page: page ? page :  1,
    sortBy: sortBy ? sortBy : 'name',
    code: code ? code : '',
    name: name ? name : '',
    direction: direction ? direction : 'ASC'
  };
  let url = new URL(BASE_URL+'/api/v1/district/');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function add(district) {
  const requestOptions = {
    method: 'POST',
    headers: request.headersWithContentType(),
    body: JSON.stringify(district)
  };

  return fetch(BASE_URL+'/api/v1/district/', requestOptions).then(request.handleResponse);
}

function detail(code) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/district/'+code, requestOptions).then(request.handleResponse);
}

function update(district) {
  const requestOptions = {
    method: 'PATCH',
    headers: request.headersWithContentType(),
    body: JSON.stringify(district)
  };

  return fetch(BASE_URL+'/api/v1/district/', requestOptions).then(request.handleResponse);
}

function remove(districtId) {
  const requestOptions = {
    method: 'DELETE',
    headers: request.headers(),
  };

  return fetch(BASE_URL+`/api/v1/district/${districtId}`, requestOptions).then(request.handleResponse);
}

function getAll(cityId) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };
  var params = {};
  if(cityId != undefined){
    params['cityId'] = cityId;
  }
  let url = new URL(BASE_URL+'/api/v1/district/droplist');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

export default districtService;
