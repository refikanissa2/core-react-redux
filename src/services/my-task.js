import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const myTaskService = {
  list,
  show,
  taskAction
};

function list(option = {}){
  const { size, page} = option;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };
  const params = {
    size: size ? size : 10,
    page: page ? page : 1
  };
  let url = new URL(BASE_URL+'/api/v1/task/maintenance');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function show(id, module){
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/'+module+'/approval/'+id, requestOptions).then(request.handleResponse);
}

function taskAction(data, module) {
  const requestOptions = {
    method: 'POST',
    headers: request.headersWithContentType(),
    body: JSON.stringify(data)
  };

  return fetch(BASE_URL+'/api/v1/'+module+'/approval/', requestOptions).then(request.handleResponse);
}

export default myTaskService;
