import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const roleService = {
  list,
  add,
  detail,
  update,
  remove,
  droplist,
  menuList,
  menuTree,
  channels,
  applicationList,
  droplistAll
};

function list(option = {}){
  const { size, page, sort, code, name , applicationCode} = option;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };
  const params = {
    size: size ? size : 10,
    page: page ? page : 1,
    sort: sort ? sort : '+name',
    code: code ? code : '',
    applicationCode: applicationCode ? applicationCode : '',
    name: name ? name : ''
  };
  let url = new URL(BASE_URL+'/api/v1/role');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function add(role){
  const requestOptions = {
    method: 'POST',
    headers: request.headersWithContentType(),
    body: JSON.stringify(role)
  };

  return fetch(BASE_URL+'/api/v1/role/', requestOptions).then(request.handleResponse);
}

function detail(code) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/role/'+code, requestOptions).then(request.handleResponse);
}

function remove(code) {
  const requestOptions = {
    method: 'DELETE',
    headers: request.headers(),
  };

  return fetch(BASE_URL+`/api/v1/role/${code}`, requestOptions).then(request.handleResponse);
}

function update(role) {
  const requestOptions = {
    method: 'PUT',
    headers: request.headersWithContentType(),
    body: JSON.stringify(role)
  };

  return fetch(BASE_URL+'/api/v1/role/', requestOptions).then(request.handleResponse);
}

function droplist(applicationCode){
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };
  const params = {
    applicationCode: applicationCode,
    search : '',
  };
  let url = new URL(BASE_URL+'/api/v1/role/droplist');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);

}

function droplistAll(){
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/role/droplist/all', requestOptions).then(request.handleResponse);
}

function menuList(code){
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/role/'+code+'/menu/', requestOptions).then(request.handleResponse);
}

function menuTree(applicationCode){
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };
  let url = new URL(BASE_URL+'/api/v1/menu/tree?applicationCode='+applicationCode);

  return fetch(url, requestOptions).then(request.handleResponse);
}

function channels(value){
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  let url = new URL(BASE_URL+'/api/v1/application/'+value+'/channel');

  return fetch(url, requestOptions).then(request.handleResponse);
}

function applicationList(){
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  let url = new URL(BASE_URL+'/api/v1/application/droplist');

  return fetch(url, requestOptions).then(request.handleResponse);
}

export default roleService;
