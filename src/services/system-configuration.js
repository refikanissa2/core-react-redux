import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const sysConfigService = {
  list,
  detail,
  update
};

function list(options = {}){
  const {page, size, sort, direction, code} = options;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  const params = {
    size: size ? size : 10,
    page: page ? page : 1,
    code: code ? code : '',
    sort: sort ? sort : '+code',
    direction: direction ? direction : 'ASC'
  };

  let url = new URL(BASE_URL+'/api/v1/sys_config');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function detail(id) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };

  return fetch(BASE_URL+`/api/v1/sys_config/${id}`, requestOptions).then(request.handleResponse);
}

function update(data) {
  const requestOptions = {
    method: 'PATCH',
    headers: request.headersWithContentType(),
    body: JSON.stringify(data)
  };

  return fetch(BASE_URL+'/api/v1/sys_config/', requestOptions).then(request.handleResponse);
}

export default sysConfigService;
