import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const uploadFileService = {
  upload,
  failed,
  list,
  detail,
  download
};

function upload(data){
  const formData = new FormData();
  formData.append('file', data.file);
  const requestOptions = {
    method: 'POST',
    headers: request.headers(),
    body: formData
  };

  return fetch(BASE_URL+'/api/v1/'+data.type+'/put_file/', requestOptions).then(request.handleResponse);
}

function failed(id, options = {}){
  const {size, page} = options;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  const params = {
    size: size ? size : 10,
    page: page ? page : 1,
    id: id ? id : ''
  };

  let url = new URL(BASE_URL+'/api/v1/upload_file/failed/');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function list(option = {}){
  const {size, page, sort, fromDate, toDate, fileName } = option;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };
  const params = {
    size: size ? size : 10,
    page: page ? page : 1,
    fromDate: fromDate ? fromDate : '',
    toDate: toDate ? toDate : '',
    fileName: fileName ? fileName : '',
    sort: sort ? sort : '-dateCreated'
  };

  let url = new URL(BASE_URL+'/api/v1/upload_file/');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function detail(id, module, options = {}){
  const {size, page} = options;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  const params = {
    size: size ? size : 10,
    page: page ? page : 1,
    id: id ? id : ''
  };

  let url = new URL(BASE_URL+'/api/v1/'+module+'/detail/');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function download(id){
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };
  let url = new URL(BASE_URL+'/api/v1/upload_file/get_file/'+id);

  return fetch(url, requestOptions).then(response => {
    return response.blob();
  });
}

export default uploadFileService;
