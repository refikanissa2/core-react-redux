import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const reportService = {
  list,
  exportData,
  downloadFile
};

function list(options = {}){
  const {page, size, dateCreated, userId, sort} = options;
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };
  const params = {
    size: size ? size : 10,
    page: page ? page :  1,
    sort: sort ? sort : '-dateCreated',
    dateCreated: dateCreated ? dateCreated : '',
    userId: userId ? userId : ''
  };
  let url = new URL(BASE_URL+'/api/v1/reporting_status/list');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function exportData(data){
  const requestOptions = {
    method: 'POST',
    headers: request.headersWithContentType(),
    body: JSON.stringify(data)
  };

  return fetch(BASE_URL+'/api/v1/reporting/generate', requestOptions).then(request.handleResponse);
}

function downloadFile(data = {}){
  const {fileName, userId} = data;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };
  const params = {
    fileName: fileName,
    userId: userId
  };
  let url = new URL(BASE_URL+'/api/v1/reporting/download');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(response => {
    return response.blob();
  });
}

export default reportService;
