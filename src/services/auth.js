import { post } from 'axios';
var base64 = require('base-64');
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const authService = {
  login
};

function login(userid,password){
  const url = BASE_URL+'/oauth/token';
  var clientId = 'backoffice';
  var clientSecret = 'gx9As3Rhjb6KGQa2';
  const token = base64.encode(clientId+':'+clientSecret);
  const formData = new FormData();
  formData.append('grant_type','password');
  formData.append('username',userid);
  formData.append('password',password);
  formData.append('scope','read');
  formData.append('client_id',clientId);
  formData.append('applicationCode','BO');
  const config = {
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
      'authorization':`Basic ${token}`
    }
  };
  return post(url, formData,config);

}

export default authService;
