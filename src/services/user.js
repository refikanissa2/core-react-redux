import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const userService = {
  list,
  add,
  update,
  remove,
  detail,
  getRole
};

function list(option = {}) {
  const {size, page, sort, id, name, roleCode, userStatus} = option;
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };
  const params = {
    size: size ? size : 10,
    page: page ? page : 1,
    sort: sort ? sort : '+name',
    id: id ? id : '',
    name: name ? name : '',
    roleCode: roleCode ? roleCode : '',
    userStatus: userStatus ? userStatus : ''
  };
  let url = new URL(BASE_URL+'/api/v1/user/');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function detail(username) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/user/'+username, requestOptions).then(request.handleResponse);
}

function add(data) {
  const requestOptions = {
    method: 'POST',
    headers: request.headersWithContentType(),
    body: JSON.stringify(data)
  };

  return fetch(BASE_URL+'/api/v1/user/', requestOptions).then(request.handleResponse);
}

function update(data) {
  const requestOptions = {
    method: 'PUT',
    headers: request.headersWithContentType(),
    body: JSON.stringify(data)
  };

  return fetch(BASE_URL+'/api/v1/user/', requestOptions).then(request.handleResponse);
}

function remove(user) {
  const requestOptions = {
    method: 'DELETE',
    headers: request.headers(),
  };

  return fetch(BASE_URL+`/api/v1/user/${user}`, requestOptions).then(request.handleResponse);
}

function getRole(username) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/user/'+username+'/role/', requestOptions).then(request.handleResponse);
}

export default userService;
