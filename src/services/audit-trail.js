import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const auditTrailService = {
  list,
  detail,
  domainList
};

function list(options = {}){
  const {page, size, objectName, dateFrom, dateTo, value} = options;
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };
  const params = {
    size: size ? size : 10,
    page: page ? page :  1,
    objectName: objectName ? objectName : '',
    dateFrom: dateFrom ? dateFrom : '',
    dateTo: dateTo ? dateTo : '',
    value: value ? value : ''
  };

  let url = new URL(BASE_URL+'/api/v1/audit_log');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function detail(options = {}){
  const {objectName, revNumber, primaryKeyEntity, type } = options;
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };
  const params = {
    objectName: objectName ? objectName : '',
    revNumber: revNumber ? revNumber : '',
    primaryKeyEntity: primaryKeyEntity ? primaryKeyEntity : '',
    type: type ? type : ''
  };
  let url = new URL(BASE_URL+'/api/v1/audit_log/detail');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function domainList(){
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };
  let url = new URL(BASE_URL+'/api/v1/audit_log/audit_groups/groupA');
  return fetch(url, requestOptions).then(request.handleResponse);
}

export default auditTrailService;
