import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const provinceService = {
  list,
  add,
  update,
  remove,
  detail,
  getAll
};

function list(option = {}) {
  const {size, page, sort, code, name} = option;
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };
  const params = {
    size: size ? size : 10,
    page: page ? page :  1,
    sort: sort ? sort : '+name',
    code: code ? code : '',
    name: name ? name : ''
  };
  let url = new URL(BASE_URL+'/api/v1/province');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function add(province) {
  const requestOptions = {
    method: 'POST',
    headers: request.headersWithContentType(),
    body: JSON.stringify(province)
  };

  return fetch(BASE_URL+'/api/v1/province/', requestOptions).then(request.handleResponse);
}

function detail(code) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/province/'+code, requestOptions).then(request.handleResponse);
}

function update(province) {
  const requestOptions = {
    method: 'PATCH',
    headers: request.headersWithContentType(),
    body: JSON.stringify(province)
  };

  return fetch(BASE_URL+'/api/v1/province/', requestOptions).then(request.handleResponse);
}

function remove(provinceId) {
  const requestOptions = {
    method: 'DELETE',
    headers: request.headers(),
  };

  return fetch(BASE_URL+`/api/v1/province/${provinceId}`, requestOptions).then(request.handleResponse);
}

function getAll(countryId) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };

  var params = {};
  if(countryId != undefined){
    params['countryId'] = countryId;
  }

  let url = new URL(BASE_URL+'/api/v1/province/droplist');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

export default provinceService;
