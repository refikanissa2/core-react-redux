import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const tecService = {
  list,
  update
};

function list(options = {}){
  const {page, size, sort, mobileMessage, codeFromHost} = options;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  const params = {
    size: size ? size : 10,
    page: page ? page : 1,
    sort: sort ? sort : '-hostName',
    mobileMessage: mobileMessage ? mobileMessage : '',
    codeFromHost: codeFromHost ? codeFromHost : ''
  };

  let url = new URL(BASE_URL+'/api/v1/trx_error_code');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function update(data) {
  const requestOptions = {
    method: 'PATCH',
    headers: request.headersWithContentType(),
    body: JSON.stringify(data)
  };

  return fetch(BASE_URL+'/api/v1/trx_error_code/', requestOptions).then(request.handleResponse);
}

export default tecService;
