import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const customerManagementService = {
  list,
  detail,
};

function list(options = {}){
  const {page, size, mobileNo, name, identityNo, cifKey, sort, branchCode} = options;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  const params = {
    size: size ? size : 10,
    page: page ? page : 1,
    name: name ? name : '',
    mobileNo: mobileNo ? mobileNo : '',
    identityNo: identityNo ? identityNo : '',
    sort: sort ? sort : '+name',
    cifKey: cifKey ? cifKey : '',
    branchCode: branchCode ? branchCode : ''
  };

  let url = new URL(BASE_URL+'/api/v1/wowib_customer/');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function detail(id) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/wowib_customer/'+id, requestOptions).then(request.handleResponse);
}

export default customerManagementService;
