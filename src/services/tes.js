import request from '@/helpers/request';
var API_ENDPOINT = '';
const {DESC_ENV} = process.env;
const {PATH_ENDPOINT} = process.env;

if (DESC_ENV==='productionRelative'){
  API_ENDPOINT = window.location.origin+PATH_ENDPOINT;
}
else{
  API_ENDPOINT = process.env.API_ENDPOINT2;
}

const tesService = {
  getZip
};

function getZip() {
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };

  return fetch(API_ENDPOINT+'/zip/vumNEubaQUaSpUmn7Gcg', requestOptions).then(request.handleResponseZip);
}

export default tesService;
