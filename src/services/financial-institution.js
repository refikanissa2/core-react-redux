import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const fiService = {
  list,
  remove,
  detail,
  add,
  update
};

function list(options = {}){
  const {page, size, sort, code, name, direction} = options;
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  const params = {
    size: size ? size : 10,
    page: page ? page :  1,
    sort: sort ? sort : '+name',
    code: code ? code : '',
    name: name ? name : '',
    direction: direction ? direction : 'ASC'
  };

  let url = new URL(BASE_URL+'/api/v1/financial_institution');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function detail(id) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };

  return fetch(BASE_URL+`/api/v1/financial_institution/${id}`, requestOptions).then(request.handleResponse);
}

function remove(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: request.headers(),
  };

  return fetch(BASE_URL+`/api/v1/financial_institution/${id}`, requestOptions).then(request.handleResponse);
}

function add(data) {
  const requestOptions = {
    method: 'POST',
    headers: request.headersWithContentType(),
    body: JSON.stringify(data)
  };

  return fetch(BASE_URL+'/api/v1/financial_institution/', requestOptions).then(request.handleResponse);
}

function update(data) {
  const requestOptions = {
    method: 'PATCH',
    headers: request.headersWithContentType(),
    body: JSON.stringify(data)
  };

  return fetch(BASE_URL+'/api/v1/financial_institution/', requestOptions).then(request.handleResponse);
}

export default fiService;
