import agentManagementService from './agent-management';
import auditTrailService from './audit-trail';
import authService from './auth';
import branchService from './branch';
import cityService from './city';
import countryService from './country';
import customerManagementService from './customer-management';
import districtService from './district';
import financialInstitutionService from './financial-institution';
import myTaskService from './my-task';
import reportService from './report';
import roleService from './role';
import provinceService from './province';
import subDistrictService from './sub-district';
import systemConfigurationService from './system-configuration';
import transactionErrorCodeService from './transaction-error-code';
import uploadFileService from './upload-file';
import userService from './user';

export {
  agentManagementService,
  auditTrailService,
  authService,
  branchService,
  cityService,
  countryService,
  customerManagementService,
  districtService,
  financialInstitutionService,
  myTaskService,
  reportService,
  roleService,
  provinceService,
  subDistrictService,
  systemConfigurationService,
  transactionErrorCodeService,
  uploadFileService,
  userService
};
