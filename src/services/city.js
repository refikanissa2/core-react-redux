import request from '@/helpers/request';
var BASE_URL = '';
const {DESC_ENV} = process.env;

if (DESC_ENV==='productionRelative'){
  BASE_URL = window.location.origin;
}
else{
  BASE_URL = process.env.API_ENDPOINT;
}

const cityService = {
  list,
  add,
  update,
  remove,
  detail,
  getAll
};

function list(option = {}) {
  const {size, page, sort, code, name, direction} = option;
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };
  const params = {
    size: size ? size : 10,
    page: page ? page :  1,
    sort: sort ? sort : '+name',
    code: code ? code : '',
    name: name ? name : '',
    direction: direction ? direction : 'ASC'
  };
  let url = new URL(BASE_URL+'/api/v1/city');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

function add(city) {
  const requestOptions = {
    method: 'POST',
    headers: request.headersWithContentType(),
    body: JSON.stringify(city)
  };

  return fetch(BASE_URL+'/api/v1/city/', requestOptions).then(request.handleResponse);
}

function detail(code) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers()
  };

  return fetch(BASE_URL+'/api/v1/city/'+code, requestOptions).then(request.handleResponse);
}

function update(city) {
  const requestOptions = {
    method: 'PATCH',
    headers: request.headersWithContentType(),
    body: JSON.stringify(city)
  };

  return fetch(BASE_URL+'/api/v1/city/', requestOptions).then(request.handleResponse);
}

function remove(cityId) {
  const requestOptions = {
    method: 'DELETE',
    headers: request.headers(),
  };

  return fetch(BASE_URL+`/api/v1/city/${cityId}`, requestOptions).then(request.handleResponse);
}

function getAll(provinceId) {
  const requestOptions = {
    method: 'GET',
    headers: request.headers(),
  };

  var params = {};
  if(provinceId != undefined){
    params['provinceId'] = provinceId;
  }

  let url = new URL(BASE_URL+'/api/v1/city/droplist');
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  return fetch(url, requestOptions).then(request.handleResponse);
}

export default cityService;
