import React from 'react';
import ReactDOM from 'react-dom';
import './assets/sass/main.sass';

import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import history from './routes/history';
import configureStore from '@/config/configureStore';

import App from './App.js';

const initialState = {};
const store = configureStore(initialState, history);

ReactDOM.render(<Provider store={store}>
  <ConnectedRouter history={history}>
    <App />
  </ConnectedRouter>
</Provider>, document.getElementById('app'));
