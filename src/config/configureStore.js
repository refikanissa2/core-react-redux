import { createStore, applyMiddleware, compose } from 'redux';
import { fromJS } from 'immutable';
import { routerMiddleware } from 'react-router-redux';
import dynamicMiddlewares, {
  addMiddleware,
  resetMiddlewares,
} from 'redux-dynamic-middlewares';
import createSagaMiddleware from 'redux-saga';
import createReducer from './reducers';

const sagaMiddleware = createSagaMiddleware();
const { EventEmitter } = require('fbemitter');
window.eventEmitter = new EventEmitter();

export default function configureStore(initialState = {}, history) {
  const middlewares = [
    sagaMiddleware,
    routerMiddleware(history),
    dynamicMiddlewares
  ];

  const enhancers = [applyMiddleware(...middlewares)];

  const getMiddleware = middleware => {
    const newMiddleware = [];
    Object.keys(middleware).forEach(key => {
      newMiddleware.push(mapMiddleware(middleware[key]));
    });
    return newMiddleware;
  };

  const mapMiddleware = middleware => {
    const arrayMiddleware = [];
    Object.keys(middleware).forEach(key => {
      arrayMiddleware.push(middleware[key]);
    });
    return arrayMiddleware;
  };

  window.eventEmitter.addListener('onInjectedMiddlewares', middleware => {
    resetMiddlewares();
    const arrayMiddleware = getMiddleware(middleware);
    arrayMiddleware.forEach(item => {
      addMiddleware(...item);
    });
  });

  const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        shouldHotReload: false,
      })
      : compose;

  const store = createStore(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(...enhancers)
  );

  store.runSaga = sagaMiddleware.run;
  store.injectedReducers = {}; // Reducer registry
  store.injectedSagas = {}; // Saga registry
  store.injectedMiddlewares = {}; // Saga registry

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(createReducer(store.injectedReducers));
    });
  }

  return store;
}
