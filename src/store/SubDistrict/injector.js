import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerSubDistrict = injectReducer({
  key: 'subDistrict',
  reducer
});
export const withMiddlewareSubDistrict = injectMiddleware({
  key: 'subDistrict',
  middleware
});
