export const GET_LIST = 'subDistrict/GET_LIST';
export const GET_LIST_SUCCESS = 'subDistrict/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'subDistrict/GET_LIST_FAILED';

export const GET_DETAIL = 'subDistrict/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'subDistrict/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'subDistrict/GET_DETAIL_FAILED';

export const DELETE_DATA = 'subDistrict/DELETE_DATA';
export const DELETE_DATA_SUCCESS = 'subDistrict/DELETE_DATA_SUCCESS';
export const DELETE_DATA_FAILED = 'subDistrict/DELETE_DATA_FAILED';

export const ADD_DATA = 'subDistrict/ADD_DATA';
export const ADD_DATA_SUCCESS = 'subDistrict/ADD_DATA_SUCCESS';
export const ADD_DATA_FAILED = 'subDistrict/ADD_DATA_FAILED';

export const UPDATE_DATA = 'subDistrict/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'subDistrict/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'subDistrict/UPDATE_DATA_FAILED';

export const GET_ALL = 'subDistrict/GET_ALL';
export const GET_ALL_SUCCESS = 'subDistrict/GET_ALL_SUCCESS';
export const GET_ALL_FAILED = 'subDistrict/GET_ALL_FAILED';
