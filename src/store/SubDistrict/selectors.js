import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectSubDistrict = state => state.get('subDistrict', initialState);

const subDistrictSelector = item =>
  createSelector(selectSubDistrict, substate =>
    isUndefined(item) || isNull(item) || item === ''
      ? substate.toJS()
      : substate.get(item)
  );

export { selectSubDistrict, subDistrictSelector };
