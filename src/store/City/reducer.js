import { fromJS } from 'immutable';

import {
  GET_LIST,
  GET_LIST_SUCCESS,
  GET_LIST_FAILED,
  GET_DETAIL,
  GET_DETAIL_SUCCESS,
  GET_DETAIL_FAILED,
  DELETE_DATA,
  DELETE_DATA_SUCCESS,
  DELETE_DATA_FAILED,
  ADD_DATA,
  ADD_DATA_SUCCESS,
  ADD_DATA_FAILED,
  UPDATE_DATA,
  UPDATE_DATA_SUCCESS,
  UPDATE_DATA_FAILED,
  GET_ALL,
  GET_ALL_SUCCESS,
  GET_ALL_FAILED
} from './constants';

export const initialState = fromJS({});

function cityReducer(state = initialState, action) {
  switch (action.type) {
  case GET_LIST:
    return state
      .set('getListSuccess', false)
      .set('getListFailed', false)
      .set('getListLoading', true);
  case GET_LIST_SUCCESS:
    return state
      .set('getListSuccess', true)
      .set('getListLoading', false)
      .set('data', action.data);
  case GET_LIST_FAILED:
    return state.set('getListFailed', true).set('getListLoading', false);

  case GET_DETAIL:
    return state
      .set('getDetailSuccess', false)
      .set('getDetailLoading', true)
      .set('getDetailFailed', false);
  case GET_DETAIL_SUCCESS:
    return state
      .set('getDetailSuccess', true)
      .set('getDetailLoading', false)
      .set('detailData', action.data);
  case GET_DETAIL_FAILED:
    return state.set('getDetailFailed', true).set('getDetailLoading', false);

  case DELETE_DATA:
    return state
      .set('deleteDataLoading', true)
      .set('deleteDataSuccess', false)
      .set('deleteDataFailed', false);
  case DELETE_DATA_SUCCESS:
    return state
      .set('deleteDataLoading', false)
      .set('deleteDataSuccess', true);
  case DELETE_DATA_FAILED:
    return state
      .set('deleteDataLoading', false)
      .set('deleteDataFailed', true);

  case ADD_DATA:
    return state
      .set('addDataLoading', true)
      .set('addDataSuccess', false)
      .set('addDataFailed', false);
  case ADD_DATA_SUCCESS:
    return state.set('addDataLoading', false).set('addDataSuccess', true);
  case ADD_DATA_FAILED:
    return state.set('addDataLoading', false).set('addDataFailed', true);

  case UPDATE_DATA:
    return state
      .set('updateDataLoading', true)
      .set('updateDataSuccess', false)
      .set('updateDataFailed', false);
  case UPDATE_DATA_SUCCESS:
    return state
      .set('updateDataLoading', false)
      .set('updateDataSuccess', true);
  case UPDATE_DATA_FAILED:
    return state
      .set('updateDataLoading', false)
      .set('updateDataFailed', true);

  case GET_ALL:
    return state
      .set('getAllLoading', true)
      .set('getAllSuccess', false)
      .set('getAllFailed', false);
  case GET_ALL_SUCCESS:
    return state
      .set('getAllLoading', false)
      .set('getAllSuccess', true)
      .set('allData', action.data);
  case GET_ALL_FAILED:
    return state
      .set('getAllLoading', false)
      .set('getAllFailed', true);

  default:
    return state;
  }
}

export default cityReducer;
