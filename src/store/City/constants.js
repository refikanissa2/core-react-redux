export const GET_LIST = 'city/GET_LIST';
export const GET_LIST_SUCCESS = 'city/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'city/GET_LIST_FAILED';

export const GET_DETAIL = 'city/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'city/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'city/GET_DETAIL_FAILED';

export const DELETE_DATA = 'city/DELETE_DATA';
export const DELETE_DATA_SUCCESS = 'city/DELETE_DATA_SUCCESS';
export const DELETE_DATA_FAILED = 'city/DELETE_DATA_FAILED';

export const ADD_DATA = 'city/ADD_DATA';
export const ADD_DATA_SUCCESS = 'city/ADD_DATA_SUCCESS';
export const ADD_DATA_FAILED = 'city/ADD_DATA_FAILED';

export const UPDATE_DATA = 'city/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'city/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'city/UPDATE_DATA_FAILED';

export const GET_ALL = 'city/GET_ALL';
export const GET_ALL_SUCCESS = 'city/GET_ALL_SUCCESS';
export const GET_ALL_FAILED = 'city/GET_ALL_FAILED';
