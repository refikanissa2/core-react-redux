import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerCity = injectReducer ({key: 'city', reducer});
export const withMiddlewareCity = injectMiddleware ({key: 'city', middleware});
