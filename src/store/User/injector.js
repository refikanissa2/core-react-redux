import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerUser = injectReducer({ key: 'user', reducer });
export const withMiddlewareUser = injectMiddleware({ key: 'user', middleware });
