import history from '@/routes/history';
import {userService} from '@/services';
import request from '@/helpers/request';
import language from '@/helpers/messages';
import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED,
  GET_DETAIL, GET_DETAIL_SUCCESS, GET_DETAIL_FAILED,
  DELETE_DATA, DELETE_DATA_SUCCESS, DELETE_DATA_FAILED,
  ADD_DATA, ADD_DATA_SUCCESS, ADD_DATA_FAILED,
  UPDATE_DATA, UPDATE_DATA_SUCCESS, UPDATE_DATA_FAILED
} from './constants';
const messages = language.getLanguage();

export default {
  onGetList: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_LIST){
      const payload = action.payload;
      userService.list(payload).then(
        resp => {
          dispatch({
            type: GET_LIST_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({type: GET_LIST_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onGetDetail: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_DETAIL){
      const data = action.payload;
      userService.detail(data.id).then(
        resp => {
          dispatch({
            type: GET_DETAIL_SUCCESS,
            detailData: resp
          });
        },
        error => {
          dispatch({type: GET_DETAIL_FAILED});
          request.handleError(error, {path: 'security/user', dataDetail: data.dataDetail});
        }
      );
    }
  },
  onDeleteData: ({dispatch}) => next => action => {
    next(action);
    if(action.type === DELETE_DATA){
      userService.remove(action.id).then(
        () => {
          dispatch({type: DELETE_DATA_SUCCESS});
          history.push('/security/user', { alert: true, message: messages.success.waiting });
        },
        error => {
          dispatch({type: DELETE_DATA_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onAddData: ({dispatch}) => next => action => {
    next(action);
    if(action.type === ADD_DATA){
      userService.add(action.payload).then(
        resp => {
          dispatch({type: ADD_DATA_SUCCESS});
          history.push('/security/user/showApproval', {
            alert: true,
            message: messages.success.waiting,
            data: resp
          });
        },
        error => {
          dispatch({type: ADD_DATA_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onUpdateData: ({dispatch}) => next => action => {
    next(action);
    if(action.type === UPDATE_DATA){
      userService.update(action.payload).then(
        resp => {
          dispatch({type: UPDATE_DATA_SUCCESS});
          history.push('/security/user/showApproval', {
            alert: true,
            message: messages.success.waiting,
            data: resp
          });
        },
        error => {
          dispatch({type: UPDATE_DATA_FAILED});
          request.handleError(error);
        }
      );
    }
  },
};
