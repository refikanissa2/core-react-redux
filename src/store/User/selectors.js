import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectUser = state => state.get('user', initialState);

const userSelector = item =>
  createSelector(
    selectUser,
    substate =>
      isUndefined(item) || isNull(item) || item === ''
        ? substate.toJS()
        : substate.get(item)
  );

export { selectUser, userSelector };
