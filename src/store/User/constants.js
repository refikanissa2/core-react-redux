export const GET_LIST = 'user/GET_LIST';
export const GET_LIST_SUCCESS = 'user/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'user/GET_LIST_FAILED';

export const GET_DETAIL = 'user/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'user/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'user/GET_DETAIL_FAILED';

export const DELETE_DATA = 'user/DELETE_DATA';
export const DELETE_DATA_SUCCESS = 'user/DELETE_DATA_SUCCESS';
export const DELETE_DATA_FAILED = 'user/DELETE_DATA_FAILED';

export const ADD_DATA = 'user/ADD_DATA';
export const ADD_DATA_SUCCESS = 'user/ADD_DATA_SUCCESS';
export const ADD_DATA_FAILED = 'user/ADD_DATA_FAILED';

export const UPDATE_DATA = 'user/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'user/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'user/UPDATE_DATA_FAILED';