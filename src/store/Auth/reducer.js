import { fromJS } from 'immutable';

import { LOGIN, LOGIN_FAILED, LOGIN_SUCCESS } from './constants';

export const initialState = fromJS({});

function authReducer(state = initialState, action){
  switch(action.type){
  case LOGIN:
    return state
      .set('loginSuccess', false)
      .set('loginFailed', false)
      .set('loginLoading', true);
  case LOGIN_FAILED:
    return state
      .set('loginFailed', true)
      .set('loginLoading', false);
  case LOGIN_SUCCESS:
    return state
      .set('loginSuccess', true)
      .set('loginLoading', false)
      .set('data', action.data);
  default:
    return state;
  }
}

export default authReducer;
