import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerAuth = injectReducer({ key: 'auth', reducer });
export const withMiddlewareAuth = injectMiddleware({ key: 'auth', middleware });
