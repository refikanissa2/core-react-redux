import moment from 'moment';

import history from '@/routes/history';
import {authService} from '@/services';
import {Alert, AuthenticationFunc} from 'component-ui-web-teravin';
import { LOGIN, LOGIN_FAILED, LOGIN_SUCCESS } from './constants';

export default {
  onLogin: ({ dispatch }) => next => action => {
    next(action);
    if(action.type === LOGIN){
      const {form} = action;
      const {userid, password} = form;
      authService.login(userid, password).then(
        resp => {
          dispatch({
            type: LOGIN_SUCCESS,
            data: resp
          });
        },
        () => {
          dispatch({type: LOGIN_FAILED});
        }
      );
    }
  }
};
