import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectAuth = state => state.get('auth', initialState);

const authSelector = item =>
  createSelector(
    selectAuth,
    substate =>
      isUndefined(item) || isNull(item) || item === ''
        ? substate.toJS()
        : substate.get(item)
  );

export { selectAuth, authSelector };
