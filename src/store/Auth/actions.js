import { LOGIN} from './constants';

export const login = (form) => ({
  type: LOGIN,
  form: form
});
