import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectFI = state => state.get('financialInstitution', initialState);

const FISelector = item =>
  createSelector(selectFI, substate =>
    isUndefined(item) || isNull(item) || item === ''
      ? substate.toJS()
      : substate.get(item)
  );

export { selectFI, FISelector };
