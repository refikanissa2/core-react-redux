export const GET_LIST = 'financialInstitution/GET_LIST';
export const GET_LIST_SUCCESS = 'financialInstitution/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'financialInstitution/GET_LIST_FAILED';

export const GET_DETAIL = 'financialInstitution/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'financialInstitution/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'financialInstitution/GET_DETAIL_FAILED';

export const DELETE_DATA = 'financialInstitution/DELETE_DATA';
export const DELETE_DATA_SUCCESS = 'financialInstitution/DELETE_DATA_SUCCESS';
export const DELETE_DATA_FAILED = 'financialInstitution/DELETE_DATA_FAILED';

export const ADD_DATA = 'financialInstitution/ADD_DATA';
export const ADD_DATA_SUCCESS = 'financialInstitution/ADD_DATA_SUCCESS';
export const ADD_DATA_FAILED = 'financialInstitution/ADD_DATA_FAILED';

export const UPDATE_DATA = 'financialInstitution/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'financialInstitution/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'financialInstitution/UPDATE_DATA_FAILED';
