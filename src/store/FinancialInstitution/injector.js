import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerFI = injectReducer({
  key: 'financialInstitution',
  reducer
});
export const withMiddlewareFI = injectMiddleware({
  key: 'financialInstitution',
  middleware
});
