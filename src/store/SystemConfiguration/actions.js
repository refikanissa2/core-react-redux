import { GET_LIST, GET_DETAIL, UPDATE_DATA } from './constants';

export const getList = data => ({
  type: GET_LIST,
  payload: data
});

export const getDetail = data => ({
  type: GET_DETAIL,
  payload: data
});

export const updateData = data => ({
  type: UPDATE_DATA,
  payload: data
});
