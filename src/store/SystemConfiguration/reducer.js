import { fromJS } from 'immutable';

import {
  GET_LIST,
  GET_LIST_SUCCESS,
  GET_LIST_FAILED,
  GET_DETAIL,
  GET_DETAIL_SUCCESS,
  GET_DETAIL_FAILED,
  UPDATE_DATA,
  UPDATE_DATA_SUCCESS,
  UPDATE_DATA_FAILED
} from './constants';

export const initialState = fromJS({});

function sysConfigReducer(state = initialState, action) {
  switch (action.type) {
  case GET_LIST:
    return state
      .set('getListSuccess', false)
      .set('getListFailed', false)
      .set('getListLoading', true);
  case GET_LIST_SUCCESS:
    return state
      .set('getListSuccess', true)
      .set('getListLoading', false)
      .set('data', action.data);
  case GET_LIST_FAILED:
    return state.set('getListFailed', true).set('getListLoading', false);

  case GET_DETAIL:
    return state
      .set('getDetailSuccess', false)
      .set('getDetailLoading', true)
      .set('getDetailFailed', false);
  case GET_DETAIL_SUCCESS:
    return state
      .set('getDetailSuccess', true)
      .set('getDetailLoading', false)
      .set('detailData', action.data);
  case GET_DETAIL_FAILED:
    return state.set('getDetailFailed', true).set('getDetailLoading', false);

  case UPDATE_DATA:
    return state
      .set('updateDataLoading', true)
      .set('updateDataSuccess', false)
      .set('updateDataFailed', false);
  case UPDATE_DATA_SUCCESS:
    return state
      .set('updateDataLoading', false)
      .set('updateDataSuccess', true);
  case UPDATE_DATA_FAILED:
    return state
      .set('updateDataLoading', false)
      .set('updateDataFailed', true);

  default:
    return state;
  }
}

export default sysConfigReducer;
