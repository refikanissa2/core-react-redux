import history from '@/routes/history';
import { systemConfigurationService } from '@/services';
import request from '@/helpers/request';
import language from '@/helpers/messages';
import {
  GET_LIST,
  GET_LIST_SUCCESS,
  GET_LIST_FAILED,
  GET_DETAIL,
  GET_DETAIL_SUCCESS,
  GET_DETAIL_FAILED,
  UPDATE_DATA,
  UPDATE_DATA_SUCCESS,
  UPDATE_DATA_FAILED
} from './constants';
const messages = language.getLanguage();

export default {
  onGetList: ({ dispatch }) => next => action => {
    next(action);
    if (action.type === GET_LIST) {
      const payload = action.payload;
      systemConfigurationService.list(payload).then(
        resp => {
          dispatch({
            type: GET_LIST_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({ type: GET_LIST_FAILED });
          request.handleError(error);
        }
      );
    }
  },
  onGetDetail: ({ dispatch }) => next => action => {
    next(action);
    if (action.type === GET_DETAIL) {
      const data = action.payload;
      systemConfigurationService.detail(data.id).then(
        resp => {
          dispatch({
            type: GET_DETAIL_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({ type: GET_DETAIL_FAILED });
          request.handleError(error, {
            path: 'maintenance/system_configuration',
            dataDetail: data.dataDetail
          });
        }
      );
    }
  },
  onUpdateData: ({ dispatch }) => next => action => {
    next(action);
    if (action.type === UPDATE_DATA) {
      systemConfigurationService.update(action.payload).then(
        resp => {
          dispatch({ type: UPDATE_DATA_SUCCESS });
          history.push('/maintenance/system_configuration/showApproval', {
            alert: true,
            message: messages.success.waiting,
            data: resp
          });
        },
        error => {
          dispatch({ type: UPDATE_DATA_FAILED });
          request.handleError(error);
        }
      );
    }
  }
};
