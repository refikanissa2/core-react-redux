export const GET_LIST = 'sysConfig/GET_LIST';
export const GET_LIST_SUCCESS = 'sysConfig/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'sysConfig/GET_LIST_FAILED';

export const GET_DETAIL = 'sysConfig/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'sysConfig/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'sysConfig/GET_DETAIL_FAILED';

export const UPDATE_DATA = 'sysConfig/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'sysConfig/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'sysConfig/UPDATE_DATA_FAILED';
