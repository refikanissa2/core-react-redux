import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerSysConfig = injectReducer({
  key: 'sysConfig',
  reducer
});
export const withMiddlewareSysConfig = injectMiddleware({
  key: 'sysConfig',
  middleware
});
