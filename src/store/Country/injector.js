import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerCountry = injectReducer({
  key: 'country',
  reducer
});

export const withMiddlewareCountry = injectMiddleware({
  key: 'country',
  middleware
});
