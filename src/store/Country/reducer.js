import { fromJS } from 'immutable';
import * as consts from './constants';

export const initialState = fromJS({});

export default function provinceReducer(state = initialState, action) {
  switch (action.type) {
  case consts.GET_LIST:
    return state
      .set('getListLoading', true)
      .set('getListSuccess', false)
      .set('getListFailed', false);
  case consts.GET_LIST_SUCCESS:
    return state
      .set('getListLoading', false)
      .set('getListSuccess', true)
      .set('data', action.payload);
  case consts.GET_LIST_FAILED:
    return state.set('getListLoading', false).set('getListFailed', true);

  case consts.GET_DETAIL:
    return state
      .set('getDetailLoading', true)
      .set('getDetailSuccess', false)
      .set('getDetailFailed', false);
  case consts.GET_DETAIL_SUCCESS:
    return state
      .set('getDetailLoading', false)
      .set('getDetailSuccess', true)
      .set('detailData', action.payload);
  case consts.GET_DETAIL_FAILED:
    return state.set('getDetailLoading', false).set('getDetailFailed', true);

  case consts.GET_ALL:
    return state
      .set('getAllLoading', true)
      .set('getAllSuccess', false)
      .set('getAllFailed', false);
  case consts.GET_ALL_SUCCESS:
    return state
      .set('getAllLoading', false)
      .set('getAllSuccess', true)
      .set('allData', action.payload);
  case consts.GET_ALL_FAILED:
    return state.set('getAllLoading', false).set('getAllFailed', true);

  case consts.GET_ALL_NATIONALITY:
    return state
      .set('getAllNationalityLoading', true)
      .set('getAllNationalitySuccess', false)
      .set('getAllNationalityFailed', false);
  case consts.GET_ALL_NATIONALITY_SUCCESS:
    return state
      .set('getAllNationalityLoading', false)
      .set('getAllNationalitySuccess', true)
      .set('allDataNationality', action.payload);
  case consts.GET_ALL_NATIONALITY_FAILED:
    return state
      .set('getAllNationalityLoading', false)
      .set('getAllNationalityFailed', true);

  case consts.ADD_DATA:
    return state
      .set('addDataLoading', true)
      .set('addDataSuccess', false)
      .set('addDataFailed', false);
  case consts.ADD_DATA_SUCCESS:
    return state
      .set('addDataLoading', false)
      .set('addDataSuccess', true)
      .set('addDataSuccessResponse', action.payload);
  case consts.ADD_DATA_FAILED:
    return state
      .set('addDataLoading', false)
      .set('addDataFailed', true);

  case consts.UPDATE_DATA:
    return state
      .set('updateDataLoading', true)
      .set('updateDataSuccess', false)
      .set('updateDataFailed', false);
  case consts.UPDATE_DATA_SUCCESS:
    return state
      .set('updateDataLoading', false)
      .set('updateDataSuccess', true)
      .set('updateDataSuccessResponse', action.payload);
  case consts.UPDATE_DATA_FAILED:
    return state
      .set('updateDataLoading', false)
      .set('updateDataFailed', true);

  case consts.DELETE_DATA:
    return state
      .set('deleteDataLoading', true)
      .set('deleteDataSuccess', false)
      .set('deleteDataFailed', false);
  case consts.DELETE_DATA_SUCCESS:
    return state
      .set('deleteDataLoading', false)
      .set('deleteDataSuccess', true);
  case consts.DELETE_DATA_FAILED:
    return state
      .set('deleteDataLoading', false)
      .set('deleteDataFailed', true);
  default:
    return state;
  }
}
