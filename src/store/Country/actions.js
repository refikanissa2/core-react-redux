import {
  GET_LIST,
  GET_DETAIL,
  GET_ALL,
  GET_ALL_NATIONALITY,
  ADD_DATA,
  UPDATE_DATA,
  DELETE_DATA
} from './constants';

export const getList = data => ({
  type: GET_LIST,
  payload: data
});

export const getDetail = data => ({
  type: GET_DETAIL,
  payload: data
});

export const getAll = data => ({
  type: GET_ALL,
  payload: data
});

export const getAllNationality = data => ({
  type: GET_ALL_NATIONALITY,
  payload: data
});

export const addData = data => ({
  type: ADD_DATA,
  payload: data
});

export const updateData = data => ({
  type: UPDATE_DATA,
  payload: data
});

export const deleteData = data => ({
  type: DELETE_DATA,
  payload: data
});
