import { createSelector } from 'reselect';
import _ from 'lodash';

import { initialState } from './reducer';

const selectCountry = state => state.get('country', initialState);

const countrySelector = item =>
  createSelector(selectCountry, substate =>
    _.isUndefined(item) || _.isNull(item) || item === ''
      ? substate.toJS()
      : substate.get(item)
  );

export { selectCountry, countrySelector };
