export const GET_LIST = 'Country/GET_LIST';
export const GET_LIST_SUCCESS = 'Country/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'Country/GET_LIST_FAILED';

export const GET_DETAIL = 'Country/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'Country/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'Country/GET_DETAIL_FAILED';

export const GET_ALL = 'Country/GET_ALL';
export const GET_ALL_SUCCESS = 'Country/GET_ALL_SUCCESS';
export const GET_ALL_FAILED = 'Country/GET_ALL_FAILED';

export const GET_ALL_NATIONALITY = 'Country/GET_ALL_NATIONALITY';
export const GET_ALL_NATIONALITY_SUCCESS = 'Country/GET_ALL_NATIONALITY_SUCCESS';
export const GET_ALL_NATIONALITY_FAILED = 'Country/GET_ALL_NATIONALITY_FAILED';

export const ADD_DATA = 'Country/ADD_DATA';
export const ADD_DATA_SUCCESS = 'Country/ADD_DATA_SUCCESS';
export const ADD_DATA_FAILED = 'Country/ADD_DATA_FAILED';

export const UPDATE_DATA = 'Country/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'Country/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'Country/UPDATE_DATA_FAILED';

export const DELETE_DATA = 'Country/DELETE_DATA';
export const DELETE_DATA_SUCCESS = 'Country/DELETE_DATA_SUCCESS';
export const DELETE_DATA_FAILED = 'Country/DELETE_DATA_FAILED';
