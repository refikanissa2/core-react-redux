import request from '@/helpers/request';
import countryService from '@/services/country';

import {
  GET_LIST,
  GET_LIST_SUCCESS,
  GET_LIST_FAILED,
  GET_DETAIL,
  GET_DETAIL_SUCCESS,
  GET_DETAIL_FAILED,
  GET_ALL,
  GET_ALL_SUCCESS,
  GET_ALL_FAILED,
  GET_ALL_NATIONALITY,
  GET_ALL_NATIONALITY_SUCCESS,
  GET_ALL_NATIONALITY_FAILED,
  ADD_DATA,
  ADD_DATA_SUCCESS,
  ADD_DATA_FAILED,
  UPDATE_DATA,
  UPDATE_DATA_SUCCESS,
  UPDATE_DATA_FAILED,
  DELETE_DATA,
  DELETE_DATA_SUCCESS,
  DELETE_DATA_FAILED
} from './constants';

export default {
  onGetList: ({ dispatch }) => next => action => {
    next(action);

    if (action.type === GET_LIST) {
      countryService.list(action.payload).then(
        resp => {
          dispatch({
            type: GET_LIST_SUCCESS,
            payload: resp
          });
        },
        error => {
          dispatch({
            type: GET_LIST_FAILED
          });
          request.handleError(error);
        }
      );
    }
  },
  onGetDetail: ({ dispatch }) => next => action => {
    next(action);

    if (action.type === GET_DETAIL) {
      countryService.detail(action.payload).then(
        resp => {
          dispatch({
            type: GET_DETAIL_SUCCESS,
            payload: resp
          });
        },
        error => {
          dispatch({
            type: GET_DETAIL_FAILED
          });
          request.handleError(error);
        }
      );
    }
  },
  onGetAll: ({ dispatch }) => next => action => {
    next(action);

    if (action.type === GET_ALL) {
      countryService.getAll(action.payload).then(
        resp => {
          dispatch({
            type: GET_ALL_SUCCESS,
            payload: resp
          });
        },
        error => {
          dispatch({
            type: GET_ALL_FAILED
          });
          request.handleError(error);
        }
      );
    }
  },
  onGetAllNationality: ({ dispatch }) => next => action => {
    next(action);

    if (action.type === GET_ALL_NATIONALITY) {
      countryService.getAllNationality(action.payload).then(
        resp => {
          dispatch({
            type: GET_ALL_NATIONALITY_SUCCESS,
            payload: resp
          });
        },
        error => {
          dispatch({
            type: GET_ALL_NATIONALITY_FAILED
          });
          request.handleError(error);
        }
      );
    }
  },
  onAddData: ({ dispatch }) => next => action => {
    next(action);

    if (action.type === ADD_DATA) {
      countryService.add(action.payload).then(
        resp => {
          dispatch({
            type: ADD_DATA_SUCCESS,
            payload: resp
          });
        },
        error => {
          dispatch({
            type: ADD_DATA_FAILED
          });
          request.handleError(error);
        }
      );
    }
  },
  onUpdateData: ({ dispatch }) => next => action => {
    next(action);

    if (action.type === UPDATE_DATA) {
      countryService.update(action.payload).then(
        resp => {
          dispatch({
            type: UPDATE_DATA_SUCCESS,
            payload: resp
          });
        },
        error => {
          dispatch({
            type: UPDATE_DATA_FAILED
          });
          request.handleError(error);
        }
      );
    }
  },
  onDeleteData: ({ dispatch }) => next => action => {
    next(action);

    if (action.type === DELETE_DATA) {
      countryService.remove(action.payload).then(
        resp => {
          dispatch({
            type: DELETE_DATA_SUCCESS,
            payload: resp
          });
        },
        error => {
          dispatch({
            type: DELETE_DATA_FAILED
          });
          request.handleError(error);
        }
      );
    }
  }
};
