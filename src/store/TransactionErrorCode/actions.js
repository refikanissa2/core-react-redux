import { GET_LIST, UPDATE_DATA} from './constants';

export const getList = (data) => ({
  type: GET_LIST,
  payload: data
});

export const updateData = data => ({
  type: UPDATE_DATA,
  payload: data
});
