import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerTrxErrorCode = injectReducer({
  key: 'trxErrorCode',
  reducer
});
export const withMiddlewareTrxErrorCode = injectMiddleware({
  key: 'trxErrorCode',
  middleware
});
