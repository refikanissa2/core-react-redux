import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectTEC = state => state.get('trxErrorCode', initialState);

const tecSelector = item =>
  createSelector(selectTEC, substate =>
    isUndefined(item) || isNull(item) || item === ''
      ? substate.toJS()
      : substate.get(item)
  );

export { selectTEC, tecSelector };
