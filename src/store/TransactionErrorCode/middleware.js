import history from '@/routes/history';
import { transactionErrorCodeService } from '@/services';
import request from '@/helpers/request';
import language from '@/helpers/messages';
import {
  GET_LIST,
  GET_LIST_SUCCESS,
  GET_LIST_FAILED,
  UPDATE_DATA,
  UPDATE_DATA_SUCCESS,
  UPDATE_DATA_FAILED
} from './constants';
const messages = language.getLanguage();

export default {
  onGetList: ({ dispatch }) => next => action => {
    next(action);
    if (action.type === GET_LIST) {
      const payload = action.payload;
      transactionErrorCodeService.list(payload).then(
        resp => {
          dispatch({
            type: GET_LIST_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({ type: GET_LIST_FAILED });
          request.handleError(error);
        }
      );
    }
  },
  onUpdateData: ({ dispatch }) => next => action => {
    next(action);
    if (action.type === UPDATE_DATA) {
      transactionErrorCodeService.update(action.payload).then(
        resp => {
          dispatch({ type: UPDATE_DATA_SUCCESS });
          history.push('/maintenance/transaction_error_code/showApproval', {
            alert: true,
            message: messages.success.waiting,
            data: resp
          });
        },
        error => {
          dispatch({ type: UPDATE_DATA_FAILED });
          request.handleError(error);
        }
      );
    }
  }
};
