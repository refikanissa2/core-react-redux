export const GET_LIST = 'trxErrorCode/GET_LIST';
export const GET_LIST_SUCCESS = 'trxErrorCode/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'trxErrorCode/GET_LIST_FAILED';

export const UPDATE_DATA = 'trxErrorCode/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'trxErrorCode/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'trxErrorCode/UPDATE_DATA_FAILED';
