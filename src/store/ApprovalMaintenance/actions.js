import { GET_LIST, GET_PENDING_TASK, ACTION_PENDING_TASK} from './constants';

export const getList = (data) => ({
  type: GET_LIST,
  payload: data
});

export const getPendingTask = data => ({
  type: GET_PENDING_TASK,
  payload: data
});

export const actionPendingTask = data => ({
  type: ACTION_PENDING_TASK,
  payload: data
});
