import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectApprovalMaintenance = state => state.get('approvalMaintenance', initialState);

const approvalMaintenanceSelector = item =>
  createSelector(
    selectApprovalMaintenance,
    substate =>
      isUndefined(item) || isNull(item) || item === ''
        ? substate.toJS()
        : substate.get(item)
  );

export { selectApprovalMaintenance, approvalMaintenanceSelector };
