import { fromJS } from 'immutable';

import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED,
  GET_PENDING_TASK, GET_PENDING_TASK_SUCCESS, GET_PENDING_TASK_FAILED,
  ACTION_PENDING_TASK, ACTION_PENDING_TASK_SUCCESS, ACTION_PENDING_TASK_FAILED,
  GET_MENU_TREE_SUCCESS, GET_MENU_TREE_FAILED, GET_MENU_TREE
} from './constants';

export const initialState = fromJS({});

function myTaskReducer(state = initialState, action){
  switch(action.type){
  case GET_LIST:
    return state
      .set('getListLoading', true)
      .set('getListSuccess', false)
      .set('getListFailed', false);
  case GET_LIST_SUCCESS:
    return state
      .set('getListLoading', false)
      .set('getListSuccess', true)
      .set('data', action.data);
  case GET_LIST_FAILED:
    return state
      .set('getListLoading', false)
      .set('getListFailed', true);

  case GET_PENDING_TASK:
    return state
      .set('getPendingTaskLoading', true)
      .set('getPendingTaskSuccess', false)
      .set('getPendingTaskFailed', false);
  case GET_PENDING_TASK_SUCCESS:
    return state
      .set('getPendingTaskLoading', false)
      .set('getPendingTaskSuccess', true)
      .set('dataWf', action.dataWf)
      .set('detailPendingTask', action.detailPendingTask)
      .set('oldDetailPendingTask', action.oldDetailPendingTask);
  case GET_PENDING_TASK_FAILED:
    return state.set('getPendingTaskFailed', true).set('getPendingTaskLoading', false);

  case ACTION_PENDING_TASK:
    return state
      .set('actionPendingTaskLoading', true)
      .set('actionPendingTaskSuccess', false)
      .set('actionPendingTaskFailed', false)
      .set('taskAction', '');
  case ACTION_PENDING_TASK_SUCCESS:
    return state
      .set('actionPendingTaskLoading', false)
      .set('actionPendingTaskSuccess', true)
      .set('taskAction', action.taskAction);
  case ACTION_PENDING_TASK_FAILED:
    return state
      .set('actionPendingTaskLoading', false)
      .set('actionPendingTaskFailed', true)
      .set('errorResp', action.error);

  default:
    return state;
  }
}

export default myTaskReducer;
