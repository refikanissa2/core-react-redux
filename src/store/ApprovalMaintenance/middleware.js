import history from '@/routes/history';
import {myTaskService} from '@/services';
import request from '@/helpers/request';
import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED,
  GET_PENDING_TASK, GET_PENDING_TASK_SUCCESS, GET_PENDING_TASK_FAILED,
  ACTION_PENDING_TASK, ACTION_PENDING_TASK_SUCCESS, ACTION_PENDING_TASK_FAILED,
} from './constants';

export default {
  onGetList: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_LIST){
      const payload = action.payload;
      myTaskService.list({page: payload.page}).then(
        resp => {
          dispatch({
            type: GET_LIST_SUCCESS,
            data: resp
          });
          if(payload.status == 'page'){
            history.push(`/my_task/approval_maintenance?page=${payload.page}`);
          }
        },
        error => {
          dispatch({type: GET_LIST_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onGetPendingTask: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_PENDING_TASK){
      const {payload} = action;
      myTaskService.show(payload.id, payload.module).then(
        resp => {
          var dataServer ={
            id: resp.id,
            wfName: resp.wfName,
            objectName: resp.objectName,
            procInstId: resp.procInstId,
            taskId: resp.taskId,
            taskName: resp.taskName
          };
          dispatch({
            type: GET_PENDING_TASK_SUCCESS,
            dataWf: dataServer,
            detailPendingTask: resp.object,
            oldDetailPendingTask: resp.oldObject
          });
        },
        error => {
          dispatch({type: GET_PENDING_TASK_FAILED});
          request.handleError(error, {path: 'my_task/approval_maintenance', dataDetail: payload.dataDetail});
        }
      );
    }
  },
  onActionPendingTask: ({dispatch}) => next => action => {
    next(action);
    if(action.type === ACTION_PENDING_TASK){
      const {payload} = action;
      myTaskService.taskAction(payload.data, payload.module).then(
        () => {
          dispatch({
            type: ACTION_PENDING_TASK_SUCCESS,
            taskAction: payload.data.taskAction
          });
        },
        error => {
          dispatch({
            type: ACTION_PENDING_TASK_FAILED,
            error: error
          });
          request.handleError(error);
        }
      );
    }
  }
};
