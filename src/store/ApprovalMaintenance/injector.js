import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerApprovalMaintenance = injectReducer({ key: 'approvalMaintenance', reducer });
export const withMiddlewareApprovalMaintenance = injectMiddleware({ key: 'approvalMaintenance', middleware });
