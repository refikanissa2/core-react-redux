export const GET_LIST = 'approvalMaintenance/GET_LIST';
export const GET_LIST_SUCCESS = 'approvalMaintenance/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'approvalMaintenance/GET_LIST_FAILED';

export const GET_PENDING_TASK = 'approvalMaintenance/GET_PENDING_TASK';
export const GET_PENDING_TASK_SUCCESS = 'approvalMaintenance/GET_PENDING_TASK_SUCCESS';
export const GET_PENDING_TASK_FAILED = 'approvalMaintenance/GET_PENDING_TASK_FAILED';

export const ACTION_PENDING_TASK = 'approvalMaintenance/ACTION_PENDING_TASK';
export const ACTION_PENDING_TASK_SUCCESS = 'approvalMaintenance/ACTION_PENDING_TASK_SUCCESS';
export const ACTION_PENDING_TASK_FAILED = 'approvalMaintenance/ACTION_PENDING_TASK_FAILED';
