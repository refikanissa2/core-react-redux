import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerBranch = injectReducer({ key: 'branch', reducer });
export const withMiddlewareBranch = injectMiddleware({
  key: 'branch',
  middleware,
});
