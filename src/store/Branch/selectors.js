import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectBranch = (state) => state.get('branch', initialState);

const branchSelector = (item) =>
  createSelector(selectBranch, (substate) =>
    isUndefined(item) || isNull(item) || item === ''
      ? substate.toJS()
      : substate.get(item)
  );

export { selectBranch, branchSelector };
