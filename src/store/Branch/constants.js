export const GET_LIST = 'branch/GET_LIST';
export const GET_LIST_SUCCESS = 'branch/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'branch/GET_LIST_FAILED';

export const GET_DETAIL = 'branch/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'branch/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'branch/GET_DETAIL_FAILED';

export const DELETE_DATA = 'branch/DELETE_DATA';
export const DELETE_DATA_SUCCESS = 'branch/DELETE_DATA_SUCCESS';
export const DELETE_DATA_FAILED = 'branch/DELETE_DATA_FAILED';

export const ADD_DATA = 'branch/ADD_DATA';
export const ADD_DATA_SUCCESS = 'branch/ADD_DATA_SUCCESS';
export const ADD_DATA_FAILED = 'branch/ADD_DATA_FAILED';

export const UPDATE_DATA = 'branch/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'branch/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'branch/UPDATE_DATA_FAILED';

export const GET_ALL = 'branch/GET_ALL';
export const GET_ALL_SUCCESS = 'branch/GET_ALL_SUCCESS';
export const GET_ALL_FAILED = 'branch/GET_ALL_FAILED';
