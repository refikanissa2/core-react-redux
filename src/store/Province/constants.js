export const GET_LIST = 'province/GET_LIST';
export const GET_LIST_SUCCESS = 'province/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'province/GET_LIST_FAILED';

export const GET_DETAIL = 'province/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'province/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'province/GET_DETAIL_FAILED';

export const DELETE_DATA = 'province/DELETE_DATA';
export const DELETE_DATA_SUCCESS = 'province/DELETE_DATA_SUCCESS';
export const DELETE_DATA_FAILED = 'province/DELETE_DATA_FAILED';

export const ADD_DATA = 'province/ADD_DATA';
export const ADD_DATA_SUCCESS = 'province/ADD_DATA_SUCCESS';
export const ADD_DATA_FAILED = 'province/ADD_DATA_FAILED';

export const UPDATE_DATA = 'province/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'province/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'province/UPDATE_DATA_FAILED';

export const GET_ALL = 'province/GET_ALL';
export const GET_ALL_SUCCESS = 'province/GET_ALL_SUCCESS';
export const GET_ALL_FAILED = 'province/GET_ALL_FAILED';
