import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerProvince = injectReducer({ key: 'province', reducer });
export const withMiddlewareProvince = injectMiddleware({ key: 'province', middleware });
