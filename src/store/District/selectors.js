import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectDistrict = state => state.get('district', initialState);

const districtSelector = item =>
  createSelector(selectDistrict, substate =>
    isUndefined(item) || isNull(item) || item === ''
      ? substate.toJS()
      : substate.get(item)
  );

export { selectDistrict, districtSelector };
