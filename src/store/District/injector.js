import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerDistrict = injectReducer({ key: 'district', reducer });
export const withMiddlewareDistrict = injectMiddleware({
  key: 'district',
  middleware
});
