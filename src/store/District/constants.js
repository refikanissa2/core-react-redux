export const GET_LIST = 'district/GET_LIST';
export const GET_LIST_SUCCESS = 'district/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'district/GET_LIST_FAILED';

export const GET_DETAIL = 'district/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'district/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'district/GET_DETAIL_FAILED';

export const DELETE_DATA = 'district/DELETE_DATA';
export const DELETE_DATA_SUCCESS = 'district/DELETE_DATA_SUCCESS';
export const DELETE_DATA_FAILED = 'district/DELETE_DATA_FAILED';

export const ADD_DATA = 'district/ADD_DATA';
export const ADD_DATA_SUCCESS = 'district/ADD_DATA_SUCCESS';
export const ADD_DATA_FAILED = 'district/ADD_DATA_FAILED';

export const UPDATE_DATA = 'district/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'district/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'district/UPDATE_DATA_FAILED';

export const GET_ALL = 'district/GET_ALL';
export const GET_ALL_SUCCESS = 'district/GET_ALL_SUCCESS';
export const GET_ALL_FAILED = 'district/GET_ALL_FAILED';
