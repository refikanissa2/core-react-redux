import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerUploadFile = injectReducer({ key: 'uploadFile', reducer });
export const withMiddlewareUploadFile = injectMiddleware({ key: 'uploadFile', middleware });
