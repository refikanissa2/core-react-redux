export const GET_LIST = 'uploadFile/GET_LIST';
export const GET_LIST_SUCCESS = 'uploadFile/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'uploadFile/GET_LIST_FAILED';

export const GET_DETAIL_FILE = 'uploadFile/GET_DETAIL_FILE';
export const GET_DETAIL_FILE_SUCCESS = 'uploadFile/GET_DETAIL_FILE_SUCCESS';
export const GET_DETAIL_FILE_FAILED = 'uploadFile/GET_DETAIL_FILE_FAILED';

export const GET_FAILED_FILE = 'uploadFile/GET_FAILED_FILE';
export const GET_FAILED_FILE_SUCCESS = 'uploadFile/GET_FAILED_FILE_SUCCESS';
export const GET_FAILED_FILE_FAILED = 'uploadFile/GET_FAILED_FILE_FAILED';

export const DOWNLOAD_FILE = 'uploadFile/DOWNLOAD_FILE';
export const DOWNLOAD_FILE_SUCCESS = 'uploadFile/DOWNLOAD_FILE_SUCCESS';
export const DOWNLOAD_FILE_FAILED = 'uploadFile/DOWNLOAD_FILE_FAILED';

export const EXPORT_DATA = 'uploadFile/EXPORT_DATA';
export const EXPORT_DATA_SUCCESS = 'uploadFile/EXPORT_DATA_SUCCESS';
export const EXPORT_DATA_FAILED = 'uploadFile/EXPORT_DATA_FAILED';

export const UPLOAD_FILE = 'uploadFile/UPLOAD_FILE';
export const UPLOAD_FILE_SUCCESS = 'uploadFile/UPLOAD_FILE_SUCCESS';
export const UPLOAD_FILE_FAILED = 'uploadFile/UPLOAD_FILE_FAILED';