import { fromJS } from 'immutable';
import { AuthenticationFunc } from 'component-ui-web-teravin';
import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED, GET_LIST_FILTER_SUCCESS, GET_LIST_FILTER_FAILED,
  DOWNLOAD_FILE, DOWNLOAD_FILE_SUCCESS, DOWNLOAD_FILE_FAILED,
  EXPORT_DATA, EXPORT_DATA_SUCCESS, EXPORT_DATA_FAILED,
  GET_DETAIL_FILE, GET_DETAIL_FILE_SUCCESS, GET_DETAIL_FILE_FAILED,
  GET_FAILED_FILE, GET_FAILED_FILE_SUCCESS, GET_FAILED_FILE_FAILED
} from './constants';

var dataDecrypt = AuthenticationFunc.decryptData('teravin');
var decoded = AuthenticationFunc.getDecodedJwttoken(dataDecrypt.token);

export const initialState = fromJS({
  userId: decoded.user_name
});

function reportReducer(state = initialState, action){
  switch(action.type){
  case GET_LIST:
    return state
      .set('getListSuccess', false)
      .set('getListFailed', false)
      .set('getListLoading', true);
  case GET_LIST_SUCCESS:
    return state
      .set('getListSuccess', true)
      .set('getListLoading', false)
      .set('data', action.data);
  case GET_LIST_FAILED:
    return state
      .set('getListFailed', true)
      .set('getListLoading', false);

  case DOWNLOAD_FILE:
    return state
      .set('downloadFileSuccess', false)
      .set('downloadFileFailed', false)
      .set('downloadFileLoading', true);
  case DOWNLOAD_FILE_SUCCESS:
    return state
      .set('downloadFileSuccess', true)
      .set('downloadFileLoading', false);
  case DOWNLOAD_FILE_FAILED:
    return state
      .set('downloadFileFailed', true)
      .set('downloadFileLoading', false);

  case EXPORT_DATA:
    return state
      .set('exportDataSuccess', false)
      .set('exportDataFailed', false)
      .set('exportDataLoading', true);
  case EXPORT_DATA_SUCCESS:
    return state
      .set('exportDataSuccess', true)
      .set('exportDataLoading', false);
  case EXPORT_DATA_FAILED:
    return state
      .set('exportDataFailed', true)
      .set('exportDataLoading', false);

  case GET_DETAIL_FILE:
    return state
      .set('getDetailFileSuccess', false)
      .set('getDetailFileFailed', false)
      .set('getDetailFileLoading', true);
  case GET_DETAIL_FILE_SUCCESS:
    return state
      .set('getDetailFileSuccess', true)
      .set('getDetailFileLoading', false)
      .set('detailData', action.data);
  case GET_DETAIL_FILE_FAILED:
    return state
      .set('getDetailFileFailed', true)
      .set('getDetailFileLoading', false);

  case GET_FAILED_FILE:
    return state
      .set('getFailedFileSuccess', false)
      .set('getFailedFileFailed', false)
      .set('getFailedFileLoading', true);
  case GET_FAILED_FILE_SUCCESS:
    return state
      .set('getFailedFileSuccess', true)
      .set('getFailedFileLoading', false)
      .set('failedData', action.data);
  case GET_FAILED_FILE_FAILED:
    return state
      .set('getFailedFileFailed', true)
      .set('getFailedFileLoading', false);

  default:
    return state;
  }
}

export default reportReducer;
