import { GET_LIST, DOWNLOAD_FILE, EXPORT_DATA, GET_DETAIL_FILE, GET_FAILED_FILE, UPLOAD_FILE} from './constants';

export const getList = (data) => ({
  type: GET_LIST,
  payload: data
});

export const getDetailFile = (data) => ({
  type: GET_DETAIL_FILE,
  payload: data
});

export const getFailedFile = (data) => ({
  type: GET_FAILED_FILE,
  payload: data
});

export const downloadFile = (data) => ({
  type: DOWNLOAD_FILE,
  payload: data
});

export const exportData = (data) => ({
  type: EXPORT_DATA,
  payload: data
});

export const uploadFile = (data) => ({
  type: UPLOAD_FILE,
  payload: data
});
