import history from '@/routes/history';
import {uploadFileService, reportService} from '@/services';
import download from 'downloadjs';
import request from '@/helpers/request';
import {ArrayFunc} from 'component-ui-web-teravin';
import language from '@/helpers/messages';
import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED,
  DOWNLOAD_FILE, DOWNLOAD_FILE_SUCCESS, DOWNLOAD_FILE_FAILED,
  EXPORT_DATA, EXPORT_DATA_SUCCESS, EXPORT_DATA_FAILED,
  GET_DETAIL_FILE, GET_DETAIL_FILE_SUCCESS, GET_DETAIL_FILE_FAILED,
  GET_FAILED_FILE, GET_FAILED_FILE_SUCCESS, GET_FAILED_FILE_FAILED, UPLOAD_FILE, UPLOAD_FILE_SUCCESS, UPLOAD_FILE_FAILED,
} from './constants';
const messages = language.getLanguage();

export default {
  onGetList: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_LIST){
      const payload = action.payload;
      uploadFileService.list(payload).then(
        resp => {
          dispatch({
            type: GET_LIST_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({type: GET_LIST_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onGetDetailFile: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_DETAIL_FILE){
      const {id, module, page, status, state} = action.payload;
      uploadFileService.detail(id, module, {page: page}).then(
        resp => {
          dispatch({
            type: GET_DETAIL_FILE_SUCCESS,
            data: resp
          });
          if(status == 'page'){
            var newModule = module.split('_');
            history.push(`/service/status_upload_bulk/${newModule[0]}/detail?id=${id}&page=${page}`, Object.assign(state));
          }
        },
        error => {
          dispatch({type: GET_DETAIL_FILE_FAILED});
          request.handleError(error, {path: 'service/status_upload_bulk', id: id});
        }
      );
    }
  },
  onGetFailedFile: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_FAILED_FILE){
      const {id, page, status, state} = action.payload;
      uploadFileService.failed(id, {page: page}).then(
        resp => {
          var data = resp;
          const {content} = data;
          data.content = content.sort(ArrayFunc.dynamicSort('rows'));
          dispatch({
            type: GET_FAILED_FILE_SUCCESS,
            data: data
          });
          if(status == 'page'){
            history.push(`/service/status_upload_bulk/failed?id=${id}&page=${page+1}`, Object.assign(state));
          }
        },
        error => {
          dispatch({type: GET_FAILED_FILE_FAILED});
          request.handleError(error, {path: 'service/status_upload_bulk', id: id});
        }
      );
    }
  },
  onDownloadFile: ({dispatch}) => next => action => {
    next(action);
    if(action.type === DOWNLOAD_FILE){
      const data = action.payload;
      uploadFileService.download(data.id).then(
        resp => {
          download(resp, data.fileName);
          dispatch({type: DOWNLOAD_FILE_SUCCESS});
        },
        error => {
          dispatch({type: DOWNLOAD_FILE_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onExportData: ({dispatch}) => next => action => {
    next(action);
    if(action.type === EXPORT_DATA){
      reportService.exportData(action.payload).then(
        () => {
          dispatch({type: EXPORT_DATA_SUCCESS});
          history.push('/report/report_status', {
            alert: true,
            message: messages.success.export
          });
        },
        error => {
          dispatch({type: EXPORT_DATA_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onUploadFile: ({dispatch}) => next => action => {
    next(action);
    if(action.type === UPLOAD_FILE){
      uploadFileService.upload(action.payload).then(
        () => {
          dispatch({type: UPLOAD_FILE_SUCCESS});
          history.push('/service/status_upload_bulk', {
            alert: true, message: messages.success.waiting
          });
        },
        error => {
          dispatch({type: UPLOAD_FILE_FAILED});
          request.handleError(error);
        }
      );
    }
  },
};
