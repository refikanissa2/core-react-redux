import { GET_LIST, GET_DETAIL } from './constants';

export const getList = (data) => ({
  type: GET_LIST,
  payload: data,
});

export const getDetail = (data) => ({
  type: GET_DETAIL,
  payload: data,
});
