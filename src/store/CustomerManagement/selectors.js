import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectCustManagement = (state) =>
  state.get('custManagement', initialState);

const custManagementSelector = (item) =>
  createSelector(selectCustManagement, (substate) =>
    isUndefined(item) || isNull(item) || item === ''
      ? substate.toJS()
      : substate.get(item)
  );

export { selectCustManagement, custManagementSelector };
