import { customerManagementService } from '@/services';
import request from '@/helpers/request';
import {
  GET_LIST,
  GET_LIST_SUCCESS,
  GET_LIST_FAILED,
  GET_DETAIL,
  GET_DETAIL_SUCCESS,
  GET_DETAIL_FAILED,
} from './constants';

export default {
  onGetList: ({ dispatch }) => (next) => (action) => {
    next(action);
    if (action.type === GET_LIST) {
      const payload = action.payload;
      customerManagementService.list(payload).then(
        resp => {
          dispatch({
            type: GET_LIST_SUCCESS,
            data: resp,
          });
        },
        error => {
          dispatch({ type: GET_LIST_FAILED });
          request.handleError(error);
        }
      );
    }
  },
  onGetDetail: ({ dispatch }) => (next) => (action) => {
    next(action);
    if (action.type === GET_DETAIL) {
      const data = action.payload;
      customerManagementService.detail(data.id).then(
        resp => {
          dispatch({
            type: GET_DETAIL_SUCCESS,
            data: resp,
          });
        },
        error => {
          dispatch({ type: GET_DETAIL_FAILED });
          request.handleError(error, {
            path: 'customer/customer_management',
            dataDetail: data.dataDetail,
          });
        }
      );
    }
  },
};
