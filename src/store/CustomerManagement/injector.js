import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerCustManagement = injectReducer({
  key: 'custManagement',
  reducer,
});
export const withMiddlewareCustManagement = injectMiddleware({
  key: 'custManagement',
  middleware,
});
