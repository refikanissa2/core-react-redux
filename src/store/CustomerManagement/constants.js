export const GET_LIST = 'custManagement/GET_LIST';
export const GET_LIST_SUCCESS = 'custManagement/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'custManagement/GET_LIST_FAILED';

export const GET_DETAIL = 'custManagement/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'custManagement/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'custManagement/GET_DETAIL_FAILED';
