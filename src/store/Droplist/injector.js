import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerDroplist = injectReducer({ key: 'droplist', reducer });
export const withMiddlewareDroplist = injectMiddleware({ key: 'droplist', middleware });
