import * as consts from './constants';

export const getAllBranch = () => ({
  type: consts.GET_ALL_BRANCH
});

export const getAllCity = (data) => ({
  type: consts.GET_ALL_CITY,
  payload: data
});

export const getAllCountry = (data) => ({
  type: consts.GET_ALL_COUNTRY,
  payload: data
});

export const getAllNationality = (data) => ({
  type: consts.GET_ALL_NATIONALITY,
  payload: data
});

export const getAllDistrict = (data) => ({
  type: consts.GET_ALL_DISTRICT,
  payload: data
});

export const getAllProvince = (data) => ({
  type: consts.GET_ALL_PROVINCE,
  payload: data
});

export const getAllRole = () => ({
  type: consts.GET_ALL_ROLE
});

export const getRoleByApp = (data) => ({
  type: consts.GET_ROLE_BY_APP,
  payload: data
});

export const getAllApplication = () => ({
  type: consts.GET_ALL_APPLICATION
});

export const getAllSubdistrict = (data) => ({
  type: consts.GET_ALL_SUBDISTRICT,
  payload: data
});

export const getChannelByApp = (data) => ({
  type: consts.GET_CHANNEL_BY_APP,
  payload: data
});
