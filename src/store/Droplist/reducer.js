import { fromJS } from 'immutable';

import * as consts from './constants';

export const initialState = fromJS({});

function droplistReducer(state = initialState, action){
  switch(action.type){
  case consts.GET_ALL_BRANCH:
    return state
      .set('getAllBranchLoading', true)
      .set('getAllBranchSuccess', false)
      .set('getAllBranchFailed', false);
  case consts.GET_ALL_BRANCH_SUCCESS:
    return state
      .set('getAllBranchLoading', false)
      .set('getAllBranchSuccess', true)
      .set('branchList', action.data);
  case consts.GET_ALL_BRANCH_FAILED:
    return state
      .set('getAllBranchLoading', false)
      .set('getAllBranchFailed', true);

  case consts.GET_ALL_CITY:
    return state
      .set('getAllCityLoading', true)
      .set('getAllCitySuccess', false)
      .set('getAllCityFailed', false);
  case consts.GET_ALL_CITY_SUCCESS:
    return state
      .set('getAllCityLoading', false)
      .set('getAllCitySuccess', true)
      .set('cityList', action.data);
  case consts.GET_ALL_CITY_FAILED:
    return state
      .set('getAllCityLoading', false)
      .set('getAllCityFailed', true);

  case consts.GET_ALL_COUNTRY:
    return state
      .set('getAllCountryLoading', true)
      .set('getAllCountrySuccess', false)
      .set('getAllCountryFailed', false);
  case consts.GET_ALL_COUNTRY_SUCCESS:
    return state
      .set('getAllCountryLoading', false)
      .set('getAllCountrySuccess', true)
      .set('countryList', action.data);
  case consts.GET_ALL_COUNTRY_FAILED:
    return state
      .set('getAllCountryLoading', false)
      .set('getAllCountryFailed', true);

  case consts.GET_ALL_NATIONALITY:
    return state
      .set('getAllNationalityLoading', true)
      .set('getAllNationalitySuccess', false)
      .set('getAllNationalityFailed', false);
  case consts.GET_ALL_NATIONALITY_SUCCESS:
    return state
      .set('getAllNationalityLoading', false)
      .set('getAllNationalitySuccess', true)
      .set('nationalityList', action.data);
  case consts.GET_ALL_NATIONALITY_FAILED:
    return state
      .set('getAllNationalityLoading', false)
      .set('getAllNationalityFailed', true);
    
  case consts.GET_ALL_DISTRICT:
    return state
      .set('getAllDistrictLoading', true)
      .set('getAllDistrictSuccess', false)
      .set('getAllDistrictFailed', false);
  case consts.GET_ALL_DISTRICT_SUCCESS:
    return state
      .set('getAllDistrictLoading', false)
      .set('getAllDistrictSuccess', true)
      .set('districtList', action.data);
  case consts.GET_ALL_DISTRICT_FAILED:
    return state
      .set('getAllDistrictLoading', false)
      .set('getAllDistrictFailed', true);
   
  case consts.GET_ALL_PROVINCE:
    return state
      .set('getAllProvinceLoading', true)
      .set('getAllProvinceSuccess', false)
      .set('getAllProvinceFailed', false);
  case consts.GET_ALL_PROVINCE_SUCCESS:
    return state
      .set('getAllProvinceLoading', false)
      .set('getAllProvinceSuccess', true)
      .set('provinceList', action.data);
  case consts.GET_ALL_PROVINCE_FAILED:
    return state
      .set('getAllProvinceLoading', false)
      .set('getAllProvinceFailed', true);

  case consts.GET_ALL_ROLE:
    return state
      .set('getAllRoleLoading', true)
      .set('getAllRoleSuccess', false)
      .set('getAllRoleFailed', false);
  case consts.GET_ALL_ROLE_SUCCESS:
    return state
      .set('getAllRoleLoading', false)
      .set('getAllRoleSuccess', true)
      .set('roleList', action.data);
  case consts.GET_ALL_ROLE_FAILED:
    return state
      .set('getAllRoleLoading', false)
      .set('getAllRoleFailed', true);
    
  case consts.GET_ROLE_BY_APP:
    return state
      .set('getRoleByAppLoading', true)
      .set('getRoleByAppSuccess', false)
      .set('getRoleByAppFailed', false);
  case consts.GET_ROLE_BY_APP_SUCCESS:
    return state
      .set('getRoleByAppLoading', false)
      .set('getRoleByAppSuccess', true)
      .set('roleByAppList', action.data);
  case consts.GET_ROLE_BY_APP_FAILED:
    return state
      .set('getRoleByAppLoading', false)
      .set('getRoleByAppFailed', true);

  case consts.GET_ALL_APPLICATION:
    return state
      .set('getAllApplicationLoading', true)
      .set('getAllApplicationSuccess', false)
      .set('getAllApplicationFailed', false);
  case consts.GET_ALL_APPLICATION_SUCCESS:
    return state
      .set('getAllApplicationLoading', false)
      .set('getAllApplicationSuccess', true)
      .set('applicationList', action.data);
  case consts.GET_ALL_APPLICATION_FAILED:
    return state
      .set('getAllApplicationLoading', false)
      .set('getAllApplicationFailed', true);
    
  case consts.GET_ALL_SUBDISTRICT:
    return state
      .set('getAllSubdistrictLoading', true)
      .set('getAllSubdistrictSuccess', false)
      .set('getAllSubdistrictFailed', false);
  case consts.GET_ALL_SUBDISTRICT_SUCCESS:
    return state
      .set('getAllSubdistrictLoading', false)
      .set('getAllSubdistrictSuccess', true)
      .set('subdistrictList', action.data);
  case consts.GET_ALL_SUBDISTRICT_FAILED:
    return state
      .set('getAllSubdistrictLoading', false)
      .set('getAllSubdistrictFailed', true);

  case consts.GET_CHANNEL_BY_APP:
    return state
      .set('getChannelByAppLoading', true)
      .set('getChannelByAppSuccess', false)
      .set('getChannelByAppFailed', false);
  case consts.GET_CHANNEL_BY_APP_SUCCESS:
    return state
      .set('getChannelByAppLoading', false)
      .set('getChannelByAppSuccess', true)
      .set('channelByAppList', action.data);
  case consts.GET_CHANNEL_BY_APP_FAILED:
    return state
      .set('getChannelByAppLoading', false)
      .set('getChannelByAppFailed', true);
  default:
    return state;
  }
}

export default droplistReducer;
