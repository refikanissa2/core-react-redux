import {
  branchService, 
  cityService, 
  countryService,
  districtService,
  provinceService,
  roleService,
  subDistrictService
} from '@/services';
import request from '@/helpers/request';
import * as consts from './constants';

export default {
  onGetAllBranch: ({dispatch}) => next => action => {
    next(action);
    if(action.type === consts.GET_ALL_BRANCH){
      branchService.getAll().then(
        resp => {
          dispatch({
            type: consts.GET_ALL_BRANCH_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({type: consts.GET_ALL_BRANCH_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onGetAllCity: ({ dispatch }) => next => action => {
    next(action);
    if (action.type === consts.GET_ALL_CITY) {
      cityService.getAll(action.payload).then(
        resp => {
          dispatch({
            type: consts.GET_ALL_CITY_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({
            type: consts.GET_ALL_CITY_FAILED
          });
          request.handleError(error);
        }
      );
    }
  },
  onGetAllCountry: ({ dispatch }) => next => action => {
    next(action);
    if (action.type === consts.GET_ALL_COUNTRY) {
      countryService.getAll(action.payload).then(
        resp => {
          dispatch({
            type: consts.GET_ALL_COUNTRY_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({
            type: consts.GET_ALL_COUNTRY_FAILED
          });
          request.handleError(error);
        }
      );
    }
  },
  onGetAllNationality: ({ dispatch }) => next => action => {
    next(action);
    if (action.type === consts.GET_ALL_NATIONALITY) {
      countryService.getAllNationality(action.payload).then(
        resp => {
          dispatch({
            type: consts.GET_ALL_NATIONALITY_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({
            type: consts.GET_ALL_NATIONALITY_FAILED
          });
          request.handleError(error);
        }
      );
    }
  },
  onGetAllDistrict: ({ dispatch }) => (next) => (action) => {
    next(action);
    if (action.type === consts.GET_ALL_DISTRICT) {
      districtService.getAll(action.payload).then(
        resp =>{
          console.log('resp :>> ', resp);
          dispatch({
            type: consts.GET_ALL_DISTRICT_SUCCESS,
            data: resp,
          });
        },
        error => {
          dispatch({ type: consts.GET_ALL_DISTRICT_FAILED });
          request.handleError(error);
        }
      );
    }
  },
  onGetAllProvince: ({ dispatch }) => (next) => (action) => {
    next(action);
    if (action.type === consts.GET_ALL_PROVINCE) {
      provinceService.getAll(action.payload).then(
        resp =>
          dispatch({
            type: consts.GET_ALL_PROVINCE_SUCCESS,
            data: resp,
          }),
        error => {
          dispatch({ type: consts.GET_ALL_PROVINCE_FAILED });
          request.handleError(error);
        }
      );
    }
  },
  onGetAllRole: ({dispatch}) => next => action => {
    next(action);
    if(action.type === consts.GET_ALL_ROLE){
      roleService.droplistAll().then(
        resp => {
          dispatch({
            type: consts.GET_ALL_ROLE_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({type: consts.GET_ALL_ROLE_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onGetRoleByApp: ({dispatch}) => next => action => {
    next(action);
    if(action.type === consts.GET_ROLE_BY_APP){
      roleService.droplist(action.payload).then(
        resp => {
          dispatch({
            type: consts.GET_ROLE_BY_APP_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({type: consts.GET_ROLE_BY_APP_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onGetAllApplication: ({dispatch}) => next => action => {
    next(action);
    if(action.type === consts.GET_ALL_APPLICATION){
      roleService.applicationList().then(
        resp => {
          dispatch({
            type: consts.GET_ALL_APPLICATION_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({type: consts.GET_ALL_APPLICATION_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onGetAllSubdistrict: ({ dispatch }) => (next) => (action) => {
    next(action);
    if (action.type === consts.GET_ALL_SUBDISTRICT) {
      subDistrictService.getAll(action.payload).then(
        (resp) =>
          dispatch({
            type: consts.GET_ALL_SUBDISTRICT,
            data: resp,
          }),
        (error) => {
          dispatch({ type: consts.GET_ALL_SUBDISTRICT_FAILED });
          request.handleError(error);
        }
      );
    }
  },
  onGetChannelByApp: ({dispatch}) => next => action => {
    next(action);
    if(action.type === consts.GET_CHANNEL_BY_APP){
      roleService.channels(action.payload).then(
        resp => {
          dispatch({
            type: consts.GET_CHANNEL_BY_APP_SUCCESS,
            data: resp
          });
        },
        error => {
          dispatch({type: consts.GET_CHANNEL_BY_APP_FAILED});
          request.handleError(error);
        }
      );
    }
  },
};
