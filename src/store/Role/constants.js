export const GET_LIST = 'role/GET_LIST';
export const GET_LIST_SUCCESS = 'role/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'role/GET_LIST_FAILED';

export const GET_DETAIL = 'role/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'role/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'role/GET_DETAIL_FAILED';

export const DELETE_DATA = 'role/DELETE_DATA';
export const DELETE_DATA_SUCCESS = 'role/DELETE_DATA_SUCCESS';
export const DELETE_DATA_FAILED = 'role/DELETE_DATA_FAILED';

export const ADD_DATA = 'role/ADD_DATA';
export const ADD_DATA_SUCCESS = 'role/ADD_DATA_SUCCESS';
export const ADD_DATA_FAILED = 'role/ADD_DATA_FAILED';

export const UPDATE_DATA = 'role/UPDATE_DATA';
export const UPDATE_DATA_SUCCESS = 'role/UPDATE_DATA_SUCCESS';
export const UPDATE_DATA_FAILED = 'role/UPDATE_DATA_FAILED';

export const GET_MENU_TREE = 'role/GET_MENU_TREE';
export const GET_MENU_TREE_SUCCESS = 'role/GET_MENU_TREE_SUCCESS';
export const GET_MENU_TREE_FAILED = 'role/GET_MENU_TREE_FAILED';
