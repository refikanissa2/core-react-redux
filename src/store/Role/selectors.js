import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectRole = state => state.get('role', initialState);

const roleSelector = item =>
  createSelector(
    selectRole,
    substate =>
      isUndefined(item) || isNull(item) || item === ''
        ? substate.toJS()
        : substate.get(item)
  );

export { selectRole, roleSelector };
