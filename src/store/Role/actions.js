import { GET_LIST, GET_DETAIL, DELETE_DATA, ADD_DATA, UPDATE_DATA, GET_ALL_APPLICATION, GET_MENU_TREE} from './constants';

export const getList = (data) => ({
  type: GET_LIST,
  payload: data
});

export const getDetail = data => ({
  type: GET_DETAIL,
  payload: data
});

export const deleteData = data => ({
  type: DELETE_DATA,
  id: data
});

export const addData = data => ({
  type: ADD_DATA,
  payload: data
});

export const updateData = data => ({
  type: UPDATE_DATA,
  payload: data
});

export const getMenuTree = data => ({
  type: GET_MENU_TREE,
  payload: data
});
