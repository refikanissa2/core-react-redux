import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducerRole = injectReducer({ key: 'role', reducer });
export const withMiddlewareRole = injectMiddleware({ key: 'role', middleware });
