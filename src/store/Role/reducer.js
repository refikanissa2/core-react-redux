import { fromJS } from 'immutable';

import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED, GET_LIST_FILTER_SUCCESS, GET_LIST_FILTER_FAILED,
  GET_DETAIL, GET_DETAIL_SUCCESS, GET_DETAIL_FAILED,
  DELETE_DATA, DELETE_DATA_SUCCESS, DELETE_DATA_FAILED,
  ADD_DATA, ADD_DATA_SUCCESS, ADD_DATA_FAILED,
  UPDATE_DATA, UPDATE_DATA_SUCCESS, UPDATE_DATA_FAILED,
  GET_ALL_APPLICATION, GET_ALL_APPLICATION_SUCCESS, GET_ALL_APPLICATION_FAILED,
  GET_MENU_TREE, GET_MENU_TREE_SUCCESS, GET_MENU_TREE_FAILED,
} from './constants';

export const initialState = fromJS({});

function userReducer(state = initialState, action){
  switch(action.type){
  case GET_LIST:
    return state
      .set('getListSuccess', false)
      .set('getListFailed', false)
      .set('getListLoading', true);
  case GET_LIST_SUCCESS:
    return state
      .set('getListSuccess', true)
      .set('getListLoading', false)
      .set('data', action.data);
  case GET_LIST_FAILED:
    return state
      .set('getListFailed', true)
      .set('getListLoading', false);

  case GET_DETAIL:
    return state
      .set('getDetailSuccess', false)
      .set('getDetailFailed', false)
      .set('getDetailLoading', true);
  case GET_DETAIL_SUCCESS:
    return state
      .set('getDetailSuccess', true)
      .set('getDetailLoading', false)
      .set('detailData', action.detailData);
  case GET_DETAIL_FAILED:
    return state
      .set('getDetailFailed', true)
      .set('getDetailLoading', false);

  case DELETE_DATA:
    return state
      .set('deleteDataSuccess', false)
      .set('deleteDataFailed', false)
      .set('deleteDataLoading', true);
  case DELETE_DATA_SUCCESS:
    return state
      .set('deleteDataSuccess', true)
      .set('deleteDataLoading', false);
  case DELETE_DATA_FAILED:
    return state
      .set('deleteDataFailed', true)
      .set('deleteDataLoading', false);

  case ADD_DATA:
    return state
      .set('addDataSuccess', false)
      .set('addDataFailed', false)
      .set('addDataLoading', true);
  case ADD_DATA_SUCCESS:
    return state.set('addDataSuccess', true)
      .set('addDataLoading', false);
  case ADD_DATA_FAILED:
    return state.set('addDataFailed', true)
      .set('addDataLoading', false);

  case UPDATE_DATA:
    return state
      .set('updateDataSuccess', false)
      .set('updateDataFailed', false)
      .set('updateDataLoading', true);
  case UPDATE_DATA_SUCCESS:
    return state
      .set('updateDataSuccess', true)
      .set('updateDataLoading', false);
  case UPDATE_DATA_FAILED:
    return state
      .set('updateDataFailed', true)
      .set('updateDataLoading', false);
  
  case GET_MENU_TREE:
    return state
      .set('getMenuTreeSuccess', false)
      .set('getMenuTreeFailed', false)
      .set('getMenuTreeLoading', true);
  case GET_MENU_TREE_SUCCESS:
    return state
      .set('getMenuTreeSuccess', true)
      .set('getMenuTreeLoading', false)
      .set('menuTree', action.allData);
  case GET_MENU_TREE_FAILED:
    return state
      .set('getMenuTreeFailed', true)
      .set('getMenuTreeLoading', false);

  default:
    return state;
  }
}

export default userReducer;
