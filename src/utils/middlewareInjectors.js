import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';
import invariant from 'invariant';

import checkStore from './checkStore';

const checkKey = key =>
  invariant(
    isString(key) && !isEmpty(key),
    '(app/utils...) injectMiddleware: Expected `key` to be a non empty string'
  );

export function injectMiddlewareFactory(store, isValid) {
  return function injectMiddleware(key, middleware) {
    if (!isValid) checkStore(store);

    checkKey(key);

    let hasMiddleware = Reflect.has(store.injectedMiddlewares, key);

    if (!hasMiddleware) {
      store.injectedMiddlewares[key] = middleware;
      window.eventEmitter.emit('onInjectedMiddlewares', store.injectedMiddlewares);
    }
  };
}

export default function getInjectors(store) {
  checkStore(store);

  return {
    injectMiddleware: injectMiddlewareFactory(store, true)
  };
}
