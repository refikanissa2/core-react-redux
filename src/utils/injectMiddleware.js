import React from 'react';
import PropTypes from 'prop-types';
import hoistNonReactStatics from 'hoist-non-react-statics';

import getInjectors from './middlewareInjectors';

/**
 * Dynamically injects a middleware
 *
 * @param {string} key A key of the middleware
 * @param {function} middleware A middleware that will be injected
 *
 */
export default ({ key, middleware }) => WrappedComponent => {

  class MiddlewareInjector extends React.Component {
    componentWillMount() {
      const { injectMiddleware } = getInjectors(this.context.store);

      injectMiddleware(key, middleware);
    }
    
    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  MiddlewareInjector.WrappedComponent = WrappedComponent;
  MiddlewareInjector.contextTypes = {
    store: PropTypes.object.isRequired,
  };
  MiddlewareInjector.displayName = `withMiddleware(${WrappedComponent.displayName ||
    WrappedComponent.name ||
    'Component'})`;

  return hoistNonReactStatics(MiddlewareInjector, WrappedComponent);
};
