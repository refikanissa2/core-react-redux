import React, { Component } from 'react';
import 'babel-polyfill';
import Router from 'react-router-dom/Router';
import Route from 'react-router-dom/Route';
import Switch from 'react-router-dom/Switch';
import history from '@/routes/history';
import PrivateRoute from '@/helpers/PrivateRoute';
import Bundle from '@/helpers/Bundle';
import routes from '@/routes/mapping-url';

const NotFound = (props) => (
  <Bundle load={require('bundle-loader?lazy!./pages/Errors/404')}>
    {(Comp) => <Comp {...props}/>}
  </Bundle>
);

class App extends Component {

  render() {
    return (
      <Router history={history}>
        <Switch>
          {Object.keys(routes).map((route, key) =>
            routes[route].private ? (
              <PrivateRoute
                key={key}
                exact={routes[route].exact}
                path={routes[route].path}
                component={routes[route].component}
                title={routes[route].title}
              />
            ):(
              <Route
                key={key}
                exact={routes[route].exact}
                path={routes[route].path}
                component={routes[route].component}
              />
            )
          )}
          <Route path="*" component={NotFound}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
