import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import history from '@/routes/history';
import {FlagIcon, SideNav, AuthenticationFunc, ConvertFunc} from 'component-ui-web-teravin';

class SideNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '',
      name: 'Administrator',
      menu: []
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
    var name = this.state.name, menu = [], menus = [];
    if(AuthenticationFunc.cekDataStorages('teravin')){
      var dataDecrypt = AuthenticationFunc.decryptData('teravin');
      var decoded = AuthenticationFunc.getDecodedJwttoken(dataDecrypt.token);
      name = decoded.name;
    }
    if(AuthenticationFunc.cekDataStorages('mn')){
      menu = AuthenticationFunc.getSessionStorages('mn');
      menu.map((item) => {
        if(item.level == 0){
          var parentType = 'item';
          var children = [];
          menu.map((childItem) => {
            if(childItem.level == 1 && childItem.parentCode == item.code){
              var newChild = {
                name: childItem.name,
                url: `${ConvertFunc.spasiToSnakeCase(item.name)}/${ConvertFunc.spasiToSnakeCase(childItem.name)}`,
                type: 'item'
              };
              children.push(newChild);
              parentType = 'dropdown';
            }
          });
          children.sort((a,b) => {
            return (a.name > b.name) ? 1 : (
              (b.name > a.name) ? -1 : 0
            );
          });
          var parent = {
            name: item.name,
            url: ConvertFunc.spasiToSnakeCase(item.name),
            type: parentType,
            child: children
          };
          menus.push(parent);
        }
      });
    }
    this.setState({
      name: name,
      menu: menus
    });
  }

  componentWillUnmount(){
    clearInterval(this.interval);
  }

  tick(){
    const today = moment().format('MMMM DD, YYYY HH:mm');
    this.setState({
      date: today
    });
  }

  onChangeLang(lang, e){
    e.preventDefault();
    localStorage.setItem('_locale', lang);
    window.location.reload();
  }

  render () {
    const {widthNav} = this.props;
    const {menu, date, name} = this.state;
    const lang = localStorage.getItem('_locale');
    return (
      <SideNav widthNav={widthNav} closeNav={this.props.closeNav}>
        <SideNav.Start
          logo={require('@/assets/img/logo.png')}
          altLogo="Logo Teravin"
          name={name}
          date={date}
        />
        <SideNav.Container>
          {menu.map((item, index) => {
            if(item.type == 'dropdown'){
              return(
                <SideNav.Dropdown key={index} id={`dropdown${index}`} title={item.name}>
                  {item.child.map((childItem, childIndex) =>
                    <SideNav.Item name={childItem.name} onClick={() => history.replace(`/${childItem.url}`)} key={childIndex} />
                  )}
                </SideNav.Dropdown>
              );
            }else if(item.type == 'item'){
              return(
                <SideNav.Item name={item.name} onClick={() => history.replace(`/${item.url}`)} key={index} />
              );
            }
          })}
        </SideNav.Container>
        <SideNav.End
          height="35%"
          onLogout={() => {
            sessionStorage.removeItem('teravin');
            sessionStorage.removeItem('mn');
            history.push('/login');
          }}
        >
          <div style={{paddingLeft: 32}}>
            <ul className="lang-switcher lang-switcher--pill">
              <li><a href="#" onClick={this.onChangeLang.bind(this,'en')} className={lang === 'en' ? 'is-active' : ''}>
                <FlagIcon code="gb"/>
              </a></li>
              <li><a href="#" onClick={this.onChangeLang.bind(this,'id')} className={lang === 'id' ? 'is-active' : ''}>
                <FlagIcon code="id"/>
              </a></li>
            </ul>
          </div>
        </SideNav.End>
      </SideNav>
    );
  }
}

SideNavigation.propTypes = {
  closeNav: PropTypes.func,
  widthNav: PropTypes.number
};

export default SideNavigation;
