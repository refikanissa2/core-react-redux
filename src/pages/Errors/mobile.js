import React, { Component } from 'react';

import HomeBase from '@/pages/home';
import {Container, Image} from 'component-ui-web-teravin';

import language from '@/helpers/messages';
const messages = language.getLanguage();

class MobilePage extends Component {
  render() {
    return (
      <HomeBase>
        <Container fluid>
          <div style={{display: 'grid', paddingTop: 100, justifyContent: 'center'}}>
            <div style={{display: 'flex', justifyContent: 'center'}}>
              <Image
                placeholder={require('@/assets/img/logo_ph.png')}
                src={require('@/assets/img/logo.png')}
                alt="Logo Teravin"
                width={200}
              />
            </div>
            <div style={{display: 'flex', justifyContent: 'center', marginTop: 30}}>
              <h1 style={{fontSize: 33, fontWeight: 600}}>{messages.text.mobilePage}</h1>
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

export default MobilePage;
