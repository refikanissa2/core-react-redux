import React, { Component } from 'react';

import history from '@/routes/history';
import {Layout, Button, Image} from 'component-ui-web-teravin';

import language from '@/helpers/messages';
const messages = language.getLanguage();

class NoService extends Component {

  render() {
    return (
      <Layout background="cloud">
        <div className="loader-bg" style={{background: '#ecf0f1', top: 0}}>
          <div className="loader-content" style={{background: '#ecf0f1', padding: 194}}>
            <div style={{display: 'grid', justifyContent: 'center'}}>
              <div style={{display: 'flex', justifyContent: 'center'}}>
                <Image
                  placeholder={require('@/assets/img/logo_ph.png')}
                  src={require('@/assets/img/logo.png')}
                  alt="Logo Teravin"
                  width={200}
                />
              </div>
              <div>
                <h1 style={{fontSize: 33, fontWeight: 600}}>{messages.text.serverNotAvailable}</h1>
              </div>
              <div>
                <span>Good job, Captain Kirk! You've boldly gone where no man has gone before.</span><br/><br/>
              </div>
              <div>
                <Button
                  onClick={() => history.go(-3)}
                  outlined={true}
                >{messages.button.homePage}</Button>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export default NoService;
