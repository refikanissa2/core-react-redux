import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { withReducerAuth, withMiddlewareAuth } from '@/store/Auth/injector';
import { login } from '@/store/Auth/actions';
import { authSelector } from '@/store/Auth/selectors';

import history from '@/routes/history';
import language from '@/helpers/messages';
import {
  Layout, Button, Input, Container, Image,
  AuthenticationFunc, CaptchaFunc, Alert
} from 'component-ui-web-teravin';
import moment from 'moment';
const messages = language.getLanguage();

const styles = {
  input: {
    border: '0px',
    boxShadow: '0px 2px 1px -1px rgba(10, 10, 10, 0.5)',
    fontSize: '1rem',
    textAlign: 'center'
  }
};

const mapStateToProps = createStructuredSelector({
  loginSuccess: authSelector('loginSuccess'),
  loginFailed: authSelector('loginFailed'),
  loginLoading: authSelector('loginLoading'),
  data: authSelector('data')
});

const mapDispatchToProps = dispatch => ({
  login: (form) => dispatch(login(form))
});

class Login extends Component {
  constructor(){
    super();
    this.state = {
      form: {
        userid: '',
        password: ''
      },
      disabledLogin: true,
      total: '',
      captcha: '',
      inputCaptcha: '',
      spinner: false
    };
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.createCaptcha = this.createCaptcha.bind(this);
  }

  componentDidMount() {
    this.createCaptcha();
    var status=false;
    var exp = 0;
    if(AuthenticationFunc.cekDataStorages('teravin')){
      var dataDecrypt = AuthenticationFunc.decryptData('teravin');
      var decoded = AuthenticationFunc.getDecodedJwttoken(dataDecrypt.token);
      status = dataDecrypt.status;
      exp = decoded.exp;
    }
    else{
      status = false;
      exp = 0;
    }
    if(status!==false && exp !== 0){
      history.push('/');
    }
  }

  createCaptcha(){
    clearInterval(this.interval);
    const data = CaptchaFunc.randomAritmatika();
    this.setState({
      total: data.total,
      captcha: data.strCaptcha,
      spinner: false,
      disabledLogin: true
    });
  }

  componentWillReceiveProps(nextProps) {
    const isPropChange = (name) =>
      nextProps[name] && nextProps[name] !== this.props[name];
    const { data, location } = nextProps;
      
    if(isPropChange('loginSuccess')){
      let token = data.data.access_token;
      var decoded = AuthenticationFunc.getDecodedJwttoken(token);
      var expTime = moment().add(data.data.expires_in, 's');
      var dataToSave = {
        token: token,
        status: true,
        expTime: expTime
      };
      if(decoded.exp > 0){
        AuthenticationFunc.setJwttokenToStorage(dataToSave, 'teravin', data.data.menu);

        if(location.search){
          const reg = new RegExp('[?&]redirect=([^&#]*)', 'i');
          const string = reg.exec(location.search);
          history.push(string[1]);
          return;
        }
        history.push('/');
      }else{
        Alert.error({messages: 'Token expired'});
      }
    }

    if(isPropChange('loginFailed')){
      Alert.error({message: 'Login Failed, please use your email NIK and Password and ensure your User ID is not "Expired" or "locked"'});
    }
  }

  onChangeForm(data){
    const {form, total, disabledLogin} = this.state;
    const {name, value} = data;

    var inputCaptcha, disabled = disabledLogin;
    if(name == 'inputCaptcha'){
      inputCaptcha = value;
      if(value == total){
        disabled = false;
      }else{
        disabled = true;
      }
    }else{
      form[name] = value;
    }
    this.setState({
      form: form,
      inputCaptcha: inputCaptcha,
      disabledLogin: disabled
    });
  }

  onSubmitForm(e){
    e.preventDefault();
    const {form} = this.state;

    if(form.userid == '' || form.password == ''){
      Alert.error({message: 'Please Input Username and password'});
    }else{
      if(form.password.length < 8){
        Alert.error({message: 'Password Minimum 8 Character'});
      }else{
        this.props.login(form);
      }
    }
  }

  render() {
    const {form, disabledLogin, captcha, inputCaptcha, spinner} = this.state;
    const { loginLoading } = this.props;
    const {userid, password} = form;
    var classNameIcon;
    if(spinner) {
      classNameIcon = 'fa fa-sync-alt fa-spin fa-md';
    }else{
      classNameIcon = 'fa fa-sync-alt fa-md';
    }

    return (
      <Layout>
        <Container fluid style={{padding:0}}>
          <div className="columns" style={{marginBottom: 20}}>
            <div className="column is-4 is-offset-4" style={{marginTop: '7%'}}>
              <Image
                // placeholder={require('@/assets/img/logo_ph.png')}
                src={require('@/assets/img/logo.png')}
                alt="Teravin"
                height={89}
                // width={70}
              />
            </div>
          </div>
          <div className="columns is-mobile" style={{marginTop: 25}}>
            <div className="column is-4 is-offset-4" style={{marginTop: '2%'}}>
              <form onSubmit={this.onSubmitForm}>
                <div className="columns">
                  <div className="column" style={{padding: '0% 15%'}}>
                    <Input.Login
                      name="userid"
                      value={userid}
                      onChange={this.onChangeForm}
                      size="normal"
                      style={styles.input}
                      placeholder="Masukkan ID"
                      maxLength={30}
                      language={messages}
                    />
                  </div>
                </div>
                <div className="columns">
                  <div className="column" style={{padding: '0% 15%'}}>
                    <Input.Login
                      name="password"
                      value={password}
                      type={'password'}
                      onChange={this.onChangeForm}
                      size="normal"
                      style={styles.input}
                      placeholder="Masukkan Sandi"
                      maxLength={20}
                      language={messages}
                    />
                  </div>
                </div>
                <div className="columns">
                  <div className="column" style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    <div style={{margin: 10}}>
                      <span style={{fontSize: 18, cursor: 'context-menu'}}>{captcha}</span>
                    </div>
                    <div>
                      <Input
                        name="inputCaptcha"
                        value={inputCaptcha}
                        onChange={this.onChangeForm}
                        language={messages}
                      />
                    </div>
                    <div style={{margin: 10}}>
                      <span className="icon has-text-dark">
                        <i className={classNameIcon} style={{cursor: 'pointer'}} onClick={() => {
                          this.setState({spinner: true});
                          this.interval = setInterval(() => this.createCaptcha(), 3000);
                        }}></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div className="columns">
                  <div className="column"/>
                </div>
                <div className="columns">
                  <div className="column" style={{padding: '0% 15%'}}>
                    <Button
                      role="submit"
                      size="medium"
                      loading={loginLoading}
                      block
                      disabled={disabledLogin}
                      type="primary"
                    >{messages.button.login}</Button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </Container>
      </Layout>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func
};

export default compose(
  withReducerAuth,
  withMiddlewareAuth,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Login);
