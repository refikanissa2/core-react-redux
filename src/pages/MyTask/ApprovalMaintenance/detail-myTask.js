import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { approvalMaintenanceSelector } from '@/store/ApprovalMaintenance/selectors';
import { getPendingTask, actionPendingTask } from '@/store/ApprovalMaintenance/actions';
import { withReducerApprovalMaintenance, withMiddlewareApprovalMaintenance } from '@/store/ApprovalMaintenance/injector';
import { uploadFileSelector } from '@/store/UploadFile/selectors';
import { downloadFile } from '@/store/UploadFile/actions';
import { withReducerUploadFile, withMiddlewareUploadFile } from '@/store/UploadFile/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';
import colors from '@/assets/sass/colors.scss';
import HomeBase from '@/pages/home';
import { Container, Button, ModalConfirmation, UrlQueryFunc, /*LoadIndicator*/  } from 'component-ui-web-teravin';
const messages = language.getLanguage();

class DetailTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmation: {
        status: false,
        message: '',
        onConfirm() {},
        colorBtn: 'primary'
      }
    };
    this.actionApprove = this.actionApprove.bind(this);
    this.actionReject = this.actionReject.bind(this);
  }

  componentWillMount() {
    const { state } = this.props.location;
    const id = UrlQueryFunc.getQueryString('id');
    this.props.getPendingTask({
      id: id,
      module: this.props.module,
      dataDetail: state ? state.dataDetail : ''
    });
  }

  componentWillReceiveProps(nextProps) {
    const {
      taskAction, taskType
    } = nextProps;
    const isPropChange = name =>
      nextProps[name] && nextProps[name] !== this.props[name];

    if (isPropChange('actionPendingTaskSuccess')) {
      if(taskAction == 'approve'){
        history.push('/my_task/approval_maintenance', { alert: true, message: messages.success[taskType] });
      }else if(taskAction == 'reject'){
        history.push('/my_task/approval_maintenance', { alert: true, message: messages.success.rejected });
      }
    }

    if (isPropChange('actionPendingTaskFailed')) {
      const { confirmation } = this.state;
      confirmation.status = false;
      this.setState({ confirmation: confirmation });
    }
  }

  actionApprove() {
    const { dataWf } = this.props;
    const dataToSend = {
      id: dataWf.id,
      procInstId: dataWf.procInstId,
      taskId: dataWf.taskId,
      taskAction: 'approve'
    };
    this.props.actionPendingTask({
      data: dataToSend,
      module: this.props.module
    });
  }

  actionReject() {
    const { dataWf } = this.props;
    const dataToSend = {
      id: dataWf.id,
      procInstId: dataWf.procInstId,
      taskId: dataWf.taskId,
      taskAction: 'reject'
    };
    this.props.actionPendingTask({
      data: dataToSend,
      module: this.props.module
    });
  }

  confirmAction(action) {
    var { confirmation } = this.state;
    confirmation['status'] = true;
    if (action == 'approve') {
      confirmation['message'] = messages.text.confirmApprove;
      confirmation['onConfirm'] = this.actionApprove;
      confirmation.colorBtn = 'primary';
    } else if (action == 'reject') {
      confirmation['message'] = messages.text.confirmReject;
      confirmation['onConfirm'] = this.actionReject;
      confirmation.colorBtn = 'danger';
    }
    this.setState({ confirmation: confirmation });
  }

  render() {
    const { confirmation } = this.state;
    const { title, location, actionPendingTaskLoading, isDownload } = this.props;
    const { title: headTitle } = location;

    const nProps = Object.assign({}, this.props);
    delete nProps.children;
    delete nProps.location;
    delete nProps.title;
    delete nProps.module;

    if(isDownload){
      nProps.onDownload = (data) => this.props.downloadFile(data);
    }

    return (
      <HomeBase
        breadcrumb={location}
        title={title}
        headTitle={headTitle}
      >
        <Container fluid>
          {this.props.children(nProps)}
          <div className='footer-form'>
            <Button
              size='small'
              onClick={() => this.confirmAction('approve')}
            >
              {messages.button.approve}
            </Button>
            &nbsp;
            <Button
              size='small'
              type='danger'
              onClick={() => this.confirmAction('reject')}
            >
              {messages.button.reject}
            </Button>
            &nbsp;
            <Button
              size='small'
              onClick={() => history.push('/my_task/approval_maintenance')}
            >
              {messages.button.back}
            </Button>
          </div>
        </Container>
        <ModalConfirmation
          message={confirmation.message}
          active={confirmation.status}
          loading={actionPendingTaskLoading}
          headerColor={colors.primary}
          onConfirm={confirmation.onConfirm}
          colorBtnYes={confirmation.colorBtn}
          onClose={() => {
            confirmation['status'] = false;
            this.setState({ confirmation: confirmation });
          }}
        />
      </HomeBase>
    );
  }
}

DetailTask.propTypes = {
  module: PropTypes.string.isRequired,
  location: PropTypes.any.isRequired,
  title: PropTypes.string
};

const mapStateToProps = createStructuredSelector({
  getPendingTaskLoading: approvalMaintenanceSelector('getPendingTaskLoading'),
  getPendingTaskSuccess: approvalMaintenanceSelector('getPendingTaskSuccess'),
  dataWf: approvalMaintenanceSelector('dataWf'),
  data: approvalMaintenanceSelector('detailPendingTask'),
  oldData: approvalMaintenanceSelector('oldDetailPendingTask'),
  actionPendingTaskLoading: approvalMaintenanceSelector('actionPendingTaskLoading'),
  actionPendingTaskSuccess: approvalMaintenanceSelector('actionPendingTaskSuccess'),
  actionPendingTaskFailed: approvalMaintenanceSelector('actionPendingTaskFailed'),
  taskAction: approvalMaintenanceSelector('taskAction'),
  downloadFileLoading: uploadFileSelector('downloadFileLoading')
});

const mapDispatchToProps = dispatch => ({
  getPendingTask: data => dispatch(getPendingTask(data)),
  actionPendingTask: data => dispatch(actionPendingTask(data)),
  downloadFile: (data) => dispatch(downloadFile(data))
});

export default compose(
  withReducerApprovalMaintenance,
  withMiddlewareApprovalMaintenance,
  withReducerUploadFile,
  withMiddlewareUploadFile,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(DetailTask);
