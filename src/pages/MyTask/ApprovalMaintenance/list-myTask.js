import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { withReducerApprovalMaintenance, withMiddlewareApprovalMaintenance } from '@/store/ApprovalMaintenance/injector';
import { getList } from '@/store/ApprovalMaintenance/actions';
import { approvalMaintenanceSelector } from '@/store/ApprovalMaintenance/selectors';

import language from '@/helpers/messages';
import history from '@/routes/history';

import HomeBase from '@/pages/home';
import {
  Container, Pagination, Message, Table,
  UrlQueryFunc,
  ConvertFunc,
  LoadIndicator
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class MyTask extends Component {
  componentDidMount(){
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    let page = 1;
    if(pageOnUrl){
      page = parseInt(pageOnUrl);
    }
    var optionData = {
      page: page,
      status: 'list'
    };
    this.props.getList(Object.assign(optionData));
  }

  setPages(page) {
    var optionData = {
      page: page+1,
      status: 'page'
    };
    this.props.getList(Object.assign(optionData));
  }

  render () {
    const {location, data = {}, getListLoading} = this.props;
    const {state, title} = location;
    return (
      <HomeBase breadcrumb={location} title={messages.menu.approvalMaintenance.list} headTitle={title}>
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <div className="columns">
            <div className="column is-12">
              <Table fullwidth>
                <Table.Head>
                  <Table.Label width={200} label={messages.label.dateNTime} />
                  <Table.Label label={messages.label.details} />
                  <Table.Label label={messages.label.type} />
                </Table.Head>
                <Table.Body>
                  {getListLoading ? (
                    <Table.Values>
                      <Table.Value colspan="3">
                        <LoadIndicator />
                      </Table.Value>
                    </Table.Values>
                  ):(
                    data.content && data.content.length > 0 ? (
                      data.content.map((item, index) =>{
                        var taskType = ConvertFunc.upperCaseFirstLetter(item.taskType);
                        if(item.taskType == 'create'){
                          taskType = 'Add';
                        }else if(item.taskType == 'update'){
                          taskType = 'Edit';
                        }
                        var objectName = item.objectName;
                        var objService = item.objectService;
                        var detailsVal = ConvertFunc.snakeToSpasiUpperCase(objService)+' : '+objectName;
                        if(objectName == ''){
                          detailsVal = ConvertFunc.snakeToSpasiUpperCase(objService);
                        }
                        return (
                          <Table.Values key={index} onClick={() => history.push(`/my_task/approval_maintenance/${objService}/${taskType.toLowerCase()}?id=${item.id}`, {dataDetail: detailsVal})} >
                            <Table.Value width={200} value={moment(item.dateCreated).format('DD/MM/YYYY hh:mm:ss A')} />
                            <Table.Value value={detailsVal} />
                            <Table.Value value={taskType} />
                          </Table.Values>
                        );
                      })
                    ):(
                      <Table.Values>
                        <Table.Value
                          value={messages.text.empty}
                          colspan="3"
                          bold
                        />
                      </Table.Values>
                    )
                  )}
                </Table.Body>
              </Table>
              {data.totalPages > 1 &&
                <Pagination
                  setPages = {(page) => this.setPages(page)}
                  {...data}
                  language={messages}
                />
              }
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

MyTask.propTypes = {
  getList: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: approvalMaintenanceSelector('data'),
  getListLoading: approvalMaintenanceSelector('getListLoading'),
});

const mapDispatchToProps = dispatch => ({
  getList: (optionData) => dispatch(getList(optionData))
});

export default compose(
  withReducerApprovalMaintenance,
  withMiddlewareApprovalMaintenance,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(MyTask);
