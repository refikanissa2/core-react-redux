import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
  withReducerCountry,
  withMiddlewareCountry
} from '@/store/Country/injector';
import { countrySelector } from '@/store/Country/selectors';
import { getDetail, deleteData } from '@/store/Country/actions';

import history from '@/routes/history';
import language from '@/helpers/messages';
import DetailColumn from './detail-column-country';
import HomeBase from '@/pages/home';
import {
  Container,
  Button,
  ModalConfirmation,
  UrlQueryFunc
} from 'component-ui-web-teravin';
import colors from '@/assets/sass/colors.scss';
const messages = language.getLanguage();

class DetailCountry extends Component {
  constructor() {
    super();
    this.state = {
      modalId: ''
    };
    this.onDeleteData = this.onDeleteData.bind(this);
  }

  componentDidMount() {
    const { location, data = {} } = this.props;
    const { number = 0 } = data;
    const { state } = location;
    if (state) {
      if (state.filter) {
        window.onpopstate = this.onBackBrowser.bind(
          this,
          state.filter,
          number + 1
        );
      }
    }
    const id = UrlQueryFunc.getQueryString('id');
    this.props.getDetail(id);
  }

  onBackBrowser(filter, page) {
    history.push('/maintenance/country?page=' + page, { filter: filter });
  }

  componentWillReceiveProps(nextProps) {
    const isPropChange = name =>
      nextProps[name] && nextProps[name] !== this.props[name];

    if (isPropChange('deleteDataSuccess')) {
      history.push('/maintenance/country', {
        alert: true,
        message: messages.success.waiting
      });
    }
  }

  onDeleteData() {
    this.setState({ isDeleting: true });
    const { data } = this.props;
    this.props.deleteData(data.id);
  }

  render() {
    const { modalId } = this.state;
    const {
      location,
      getDetailLoading,
      deleteDataLoading,
      detailData = {},
      data = {}
    } = this.props;
    const { number = 0 } = data;
    const { viewOnly, title, state } = location;
    return (
      <HomeBase
        loader={getDetailLoading}
        breadcrumb={location}
        title={messages.menu.country.detail}
        headTitle={title}
      >
        <Container fluid>
          <DetailColumn data={detailData} createdInfo />
          <div className="footer-form">
            {viewOnly == false && (
              <span>
                <Button
                  size="small"
                  onClick={() =>
                    history.push(
                      `/maintenance/country/update?id=${detailData.id}`,
                      {
                        dataDetail: state.dataDetail,
                        filter: state.filter
                      }
                    )
                  }
                >
                  {messages.button.edit}
                </Button>
                &nbsp;
                <Button
                  size="small"
                  type="danger"
                  onClick={() => this.setState({ modalId: 'delete' })}
                >
                  {messages.button.delete}
                </Button>
                &nbsp;
              </span>
            )}
            <Button
              size="small"
              onClick={() =>
                history.push('/maintenance/country?page=' + (number + 1), {
                  filter: state.filter
                })
              }
            >
              {messages.button.back}
            </Button>
          </div>
        </Container>
        {modalId === 'delete' && (
          <ModalConfirmation
            message={messages.text.confirmDelete}
            active
            loading={deleteDataLoading}
            headerColor={colors.primary}
            onConfirm={this.onDeleteData}
            onClose={() => this.setState({ modalId: '' })}
          />
        )}
      </HomeBase>
    );
  }
}

DetailCountry.propTypes = {
  getDetail: PropTypes.func,
  deleteData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: countrySelector('data'),
  detailData: countrySelector('detailData'),
  getDetailLoading: countrySelector('getDetailLoading'),
  getDetailSuccess: countrySelector('getDetailSuccess'),
  getDetailFailed: countrySelector('getDetailFailed'),
  deleteDataLoading: countrySelector('deleteDataLoading'),
  deleteDataSuccess: countrySelector('deleteDataSuccess'),
  deleteDataFailed: countrySelector('deleteDataFailed')
});

const mapDispatchToProps = dispatch => ({
  getDetail: data => dispatch(getDetail(data)),
  deleteData: id => dispatch(deleteData(id))
});

export default compose(
  withReducerCountry,
  withMiddlewareCountry,
  connect(mapStateToProps, mapDispatchToProps)
)(DetailCountry);
