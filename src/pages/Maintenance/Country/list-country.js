import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import {
  withReducerCountry,
  withMiddlewareCountry
} from '@/store/Country/injector';
import { getList } from '@/store/Country/actions';
import { countrySelector } from '@/store/Country/selectors';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container,
  Message,
  TableWithSelect,
  Button,
  Pagination,
  Card,
  Input,
  UrlQueryFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class ListCountry extends Component {
  constructor() {
    super();
    this.state = {
      loadingPage: false,
      isSearching: false,
      filter: {
        code: '',
        name: ''
      }
    };
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
  }

  componentDidMount() {
    this.setState({ loadingPage: true });
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    let page = 1;
    var optionData = {
      page: page,
      status: 'list'
    };
    if (pageOnUrl) {
      page = parseInt(pageOnUrl);
      optionData.page = page;
      const { state } = this.props.location;
      if (state) {
        if (state.filter) {
          optionData.filter = state.filter;
          this.setState({ filter: state.filter });
        }
      }
    }
    this.props.getList(Object.assign(optionData));
  }

  componentWillReceiveProps(nextProps) {
    const {
      getListSuccess,
      getListFailed,
      getListFilterSuccess,
      getListFilterFailed
    } = nextProps;
    if (getListFailed || getListSuccess) {
      this.setState({ loadingPage: false });
    }
    if (getListFilterFailed || getListFilterSuccess) {
      this.setState({ isSearching: false });
    }
  }

  setPages(page) {
    const { filter } = this.state;
    var optionData = {
      page: page + 1,
      filter: filter,
      status: 'page'
    };
    this.props.getList(Object.assign(optionData));
  }

  onChangeFilter(data) {
    const { filter } = this.state;
    const { value, name } = data;
    filter[name] = value;
    this.setState({
      filter: filter
    });
  }

  onSubmitFilter(e) {
    e.preventDefault();
    this.setState({ isSearching: true });
    const { filter } = this.state;
    const { state } = this.props.location;
    var optionData = {
      filter: filter,
      state: state,
      status: 'filter'
    };
    this.props.getList(Object.assign(optionData));
  }

  render() {
    const { isSearching, filter } = this.state;
    const { location, getListLoading, data = { content: [] } } = this.props;
    const { state, viewOnly, title } = location;
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.country.list}
        headTitle={title}
      >
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <div className="columns">
            <div className="column is-9">
              <TableWithSelect
                headers={[
                  {
                    label: messages.label.code,
                    value: 'code'
                  },
                  {
                    label: messages.label.name,
                    value: 'name'
                  },
                  {
                    label: messages.label.phoneCode,
                    value: 'phoneCode'
                  },
                  {
                    label: messages.label.nationality,
                    value: 'nationality'
                  }
                ]}
                loading={getListLoading || isSearching}
                data={data.content}
                fullwidth
                selectable={false}
                onClick={item => {
                  history.push(`/maintenance/country/detail?id=${item.id}`, {
                    dataDetail: `${item.code} - ${item.name}`,
                    filter
                  });
                }}
              />
              {data.totalPages > 1 && (
                <Pagination
                  setPages={page => this.setPages(page)}
                  {...data}
                  language={messages}
                />
              )}
            </div>
            <div className="column is-one-quarter is-hidden-mobile">
              <Card>
                <Card.Header title={messages.label.filter} />
                <Card.Content>
                  <div className="content">
                    <form onSubmit={this.onSubmitFilter}>
                      <div className="field">
                        <label>
                          <h6>{messages.label.code}</h6>
                        </label>
                        <Input.InputCode
                          name="code"
                          value={filter.code}
                          onChange={this.onChangeFilter}
                          size="small"
                          maxLength={5}
                          language={messages}
                        />
                      </div>
                      <div className="field">
                        <label>
                          <h6>{messages.label.name}</h6>
                        </label>
                        <Input
                          name="name"
                          value={filter.name}
                          onChange={this.onChangeFilter}
                          size="small"
                          maxLength={40}
                          language={messages}
                        />
                      </div>
                      <div className="columns">
                        <div className="column is-6">
                          <div className="field">
                            <p className="control">
                              <Button
                                size="small"
                                role="submit"
                                loading={isSearching}
                                block
                              >
                                {messages.button.search}
                              </Button>
                            </p>
                          </div>
                        </div>
                        {viewOnly == false && (
                          <div className="column is-6">
                            <div className="field">
                              <p className="control">
                                <Button
                                  size="small"
                                  onClick={() =>
                                    history.push('/maintenance/country/add')
                                  }
                                  block
                                >
                                  {messages.button.add}
                                </Button>
                              </p>
                            </div>
                          </div>
                        )}
                      </div>
                    </form>
                  </div>
                </Card.Content>
              </Card>
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ListCountry.propTypes = {
  getList: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: countrySelector('data'),
  getListLoading: countrySelector('getListLoading'),
  getListSuccess: countrySelector('getListSuccess'),
  getListFailed: countrySelector('getListFailed')
});

const mapDispatchToProps = dispatch => ({
  getList: optionData => dispatch(getList(optionData))
});

export default compose(
  withReducerCountry,
  withMiddlewareCountry,
  connect(mapStateToProps, mapDispatchToProps)
)(ListCountry);
