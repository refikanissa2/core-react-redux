import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { countrySelector } from '@/store/Country/selectors';
import { getDetail, addData, updateData } from '@/store/Country/actions';
import {
  withReducerCountry,
  withMiddlewareCountry
} from '@/store/Country/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container,
  Button,
  Column,
  Input,
  Textarea,
  ValidationsFunc,
  UrlQueryFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class AddEditCountry extends Component {
  constructor(props) {
    super(props);
    const editPath = '/maintenance/country/update';
    const { location = {} } = props;
    const { pathname = '' } = location;
    this.state = {
      form: {
        code: '',
        name: '',
        phoneCode: '',
        nationality: '',
        description: ''
      },
      errors: {},
      isEdit: pathname === editPath
    };

    this.onChangeForm = this.onChangeForm.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
  }

  componentWillMount() {
    const { isEdit } = this.state;
    if (isEdit) {
      const id = UrlQueryFunc.getQueryString('id');
      this.props.getDetail(id);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      addDataSuccessResponse,
      updateDataSuccessResponse,
      detailData = {}
    } = nextProps;
    const isPropChange = name =>
      nextProps[name] && nextProps[name] !== this.props[name];

    if (isPropChange('getDetailSuccess')) {
      this.setState({
        form: {
          code: detailData.code,
          name: detailData.name,
          phoneCode: detailData.phoneCode,
          nationality: detailData.nationality,
          description: detailData.description
        }
      });
    }

    if (isPropChange('addDataSuccess')) {
      history.push('/maintenance/country/showApproval', {
        alert: true,
        message: messages.success.waiting,
        data: addDataSuccessResponse
      });
    }
    if (isPropChange('updateDataSuccess')) {
      history.push('/maintenance/country/showApproval', {
        alert: true,
        message: messages.success.waiting,
        data: updateDataSuccessResponse
      });
    }
  }

  validationOptions() {
    const validationOptions = {
      code: {
        validator: [ValidationsFunc.Type.required()]
      },
      name: {
        validator: [ValidationsFunc.Type.required()]
      },
      phoneCode: {
        validator: [ValidationsFunc.Type.required()]
      },
      nationality: {
        validator: [ValidationsFunc.Type.required()]
      },
      description: {
        validator: []
      }
    };
    return validationOptions;
  }

  onChangeForm(data) {
    const { form } = this.state;
    const { name, value } = data;

    form[name] = value;

    const errors = ValidationsFunc.RunValidate(
      name,
      form,
      this.validationOptions()
    );

    this.setState({
      form,
      errors
    });
  }

  onChangeFormMultiple(data) {
    const { form } = this.state;

    let errors = {};
    Object.entries(data).forEach(([name, value]) => {
      form[name] = value;
      errors = Object.assign(
        errors,
        ValidationsFunc.RunValidate(name, form, this.validationOptions())
      );
    });

    this.setState({
      form,
      errors
    });
  }

  onSubmitForm(event) {
    event.preventDefault();
    const { form, isEdit } = this.state;
    const errors = ValidationsFunc.RunValidateAll(
      form,
      this.validationOptions()
    );
    const errorsKeys = Object.keys(errors);
    if (errorsKeys.length === 0) {
      if (isEdit) {
        this.props.updateData(form);
      } else {
        this.props.addData(
          Object.assign({}, form, {
            hostSourceMappingDto: [
              {
                hostCode: 'esb',
                moduleValue: form.code
              }
            ]
          })
        );
      }
    } else {
      this.setState({
        errors
      });
    }
  }

  render() {
    const { form, errors, isEdit } = this.state;
    const {
      location,
      data = {},
      getDetailLoading,
      addDataLoading,
      updateDataLoading
    } = this.props;
    const { number } = data;
    const { title } = location;
    return (
      <HomeBase
        loader={getDetailLoading}
        breadcrumb={location}
        title={isEdit ? messages.menu.country.edit : messages.menu.country.add}
        headTitle={title}
      >
        <Container fluid>
          <form onSubmit={this.onSubmitForm}>
            <Column>
              <Column.Content>
                <Column.Name text={`${messages.label.code} *`} />
                <Column.Value>
                  <Input.InputCode
                    name="code"
                    value={form.code}
                    maxLength={5}
                    onChange={this.onChangeForm}
                    error={errors.code}
                    width={300}
                    language={messages}
                    disabled={isEdit}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.label.name} *`} />
                <Column.Value>
                  <Input
                    name="name"
                    value={form.name}
                    error={errors.name}
                    onChange={this.onChangeForm}
                    width={300}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.label.phoneCode} *`} />
                <Column.Value>
                  <Input.Number
                    name="phoneCode"
                    value={form.phoneCode}
                    error={errors.phoneCode}
                    maxLength={3}
                    onChange={this.onChangeForm}
                    width={300}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.label.nationality} *`} />
                <Column.Value>
                  <Input
                    name="nationality"
                    value={form.nationality}
                    error={errors.nationality}
                    onChange={this.onChangeForm}
                    width={300}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.label.description}`} />
                <Column.Value>
                  <Textarea
                    name="description"
                    value={form.description}
                    onChange={this.onChangeForm}
                    width={300}
                  />
                </Column.Value>
              </Column.Content>
            </Column>
            <div className="footer-form">
              <Button
                role="submit"
                size="small"
                loading={addDataLoading || updateDataLoading}
              >
                {isEdit ? messages.button.update : messages.button.add}
              </Button>
              &nbsp;
              <Button
                size="small"
                onClick={() => {
                  history.push(`/maintenance/country?page=${number + 1}`);
                }}
              >
                {messages.button.cancel}
              </Button>
            </div>
          </form>
        </Container>
      </HomeBase>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: countrySelector('data'),
  detailData: countrySelector('detailData'),
  getDetailLoading: countrySelector('getDetailLoading'),
  getDetailSuccess: countrySelector('getDetailSuccess'),
  getDetailFailed: countrySelector('getDetailFailed'),
  addDataLoading: countrySelector('addDataLoading'),
  addDataSuccess: countrySelector('addDataSuccess'),
  addDataSuccessResponse: countrySelector('addDataSuccessResponse'),
  addDataFailed: countrySelector('addDataFailed'),
  updateDataLoading: countrySelector('updateDataLoading'),
  updateDataSuccess: countrySelector('updateDataSuccess'),
  updateDataSuccessResponse: countrySelector('updateDataSuccessResponse'),
  updateDataFailed: countrySelector('updateDataFailed')
});

const mapDispatchToProps = dispatch => ({
  getDetail: data => dispatch(getDetail(data)),
  addData: data => dispatch(addData(data)),
  updateData: data => dispatch(updateData(data))
});

export default compose(
  withReducerCountry,
  withMiddlewareCountry,
  connect(mapStateToProps, mapDispatchToProps)
)(AddEditCountry);
