import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { FISelector } from '@/store/FinancialInstitution/selectors';
import { addData, updateData } from '@/store/FinancialInstitution/actions';
import {
  withReducerFI,
  withMiddlewareFI
} from '@/store/FinancialInstitution/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container,
  Button,
  Column,
  Input,
  Textarea,
  UrlQueryFunc,
  ValidationsFunc,
  ValidationFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class AddFinancialInstitution extends Component {
  constructor(props) {
    super(props);
    const editPath = '/maintenance/financial_institution/update';
    const { location = {} } = props;
    const { pathname = '' } = location;
    this.state = {
      form: {
        code: '',
        biCode: '',
        name: '',
        swiftCode: '',
        description: ''
      },
      errors: {
        code: '',
        biCode: '',
        name: ''
      },
      isEdit: pathname === editPath,
      isDisableUpdate: true
    };
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentWillMount() {
    const { isEdit } = this.state;
    if (isEdit) {
      const { detailData } = this.props;
      if (!detailData) {
        const { state } = this.props.location;
        const id = UrlQueryFunc.getQueryString('id');
        history.push('/maintenance/financial_institution/detail?id=' + id, {
          dataDetail: state ? state.dataDetail : '',
          filter: state ? state.filter : {}
        });
      } else {
        var form = {
          id: detailData.id,
          code: detailData.code,
          name: detailData.name,
          biCode: detailData.biCode,
          swiftCode: detailData.swiftCode,
          description: detailData.description
        };
        this.setState({
          form: form,
          oldData: Object.assign({}, form)
        });
      }
    }
  }

  validationOptions() {
    const validationOptions = {
      code: {
        validator: [ValidationsFunc.Type.required()]
      },
      name: {
        validator: [ValidationsFunc.Type.required()]
      },
      biCode: {
        validator: [ValidationsFunc.Type.required()]
      },
      swiftCode: {
        validator: []
      },
      description: {
        validator: []
      }
    };
    return validationOptions;
  }

  onChangeForm(data) {
    let { form, oldData, isDisableUpdate } = this.state;
    const { name, value } = data;

    form[name] = value;

    const errors = ValidationsFunc.RunValidate(
      name,
      form,
      this.validationOptions()
    );

    if(oldData){
      isDisableUpdate = ValidationFunc.objValidationUpdate(oldData, form);
    }

    this.setState({
      form,
      errors,
      isDisableUpdate
    });
  }

  onSave(e) {
    e.preventDefault();
    const { form, isEdit } = this.state;
    const errors = ValidationsFunc.RunValidateAll(
      form,
      this.validationOptions()
    );
    const errorsKey = Object.keys(errors);

    if (errorsKey.length === 0) {
      if (isEdit) {
        this.props.updateData(form);
      } else {
        this.props.addData(form);
      }
    } else {
      this.setState({ errors });
    }
  }

  render() {
    const { form, errors, isEdit, isDisableUpdate } = this.state;
    const { location, addDataLoading, updateDataLoading } = this.props;
    const { title, state } = location;
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.financialInstitution.add}
        headTitle={title}
      >
        <Container fluid>
          <form onSubmit={this.onSave}>
            <Column>
              <Column.Content>
                <Column.Name text={`${messages.label.code} *`} />
                <Column.Value>
                  <Input.InputCode
                    name="code"
                    value={form.code}
                    error={errors.code}
                    onChange={this.onChangeForm}
                    width={300}
                    maxLength={40}
                    language={messages}
                    disabled={isEdit}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.bankCode + ' *'} />
                <Column.Value>
                  <Input
                    name="biCode"
                    value={form.biCode}
                    error={errors.biCode}
                    onChange={this.onChangeForm}
                    width={300}
                    maxLength={10}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.name + ' *'} />
                <Column.Value>
                  <Input
                    name="name"
                    value={form.name}
                    error={errors.name}
                    onChange={this.onChangeForm}
                    width={300}
                    maxLength={100}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.swiftCode} />
                <Column.Value>
                  <Input
                    name="swiftCode"
                    value={form.swiftCode}
                    onChange={this.onChangeForm}
                    width={300}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.description} />
                <Column.Value>
                  <Textarea
                    name="description"
                    value={form.description}
                    onChange={this.onChangeForm}
                    width={300}
                    maxLength={500}
                  />
                </Column.Value>
              </Column.Content>
            </Column>
            <div className="footer-form">
              <Button
                role="submit"
                size="small"
                loading={addDataLoading || updateDataLoading}
                disabled={isEdit && isDisableUpdate}
              >
                {isEdit ? messages.button.update : messages.button.add}
              </Button>
              &nbsp;
              <Button
                size="small"
                onClick={() =>
                  isEdit
                    ? history.push(
                      `/maintenance/financial_institution/detail?id=${form.id}`,
                      {
                        dataDetail: state ? state.dataDetail : '',
                        filter: state ? state.filter : {}
                      }
                    )
                    : history.push('/maintenance/financial_institution')
                }
              >
                {messages.button.cancel}
              </Button>
            </div>
          </form>
        </Container>
      </HomeBase>
    );
  }
}

AddFinancialInstitution.propTypes = {
  addData: PropTypes.func,
  updateData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  detailData: FISelector('detailData'),
  addDataLoading: FISelector('addDataLoading'),
  updateDataLoading: FISelector('updateDataLoading')
});

const mapDispatchToProps = dispatch => ({
  addData: form => dispatch(addData(form)),
  updateData: form => dispatch(updateData(form))
});

export default compose(
  withReducerFI,
  withMiddlewareFI,
  connect(mapStateToProps, mapDispatchToProps)
)(AddFinancialInstitution);
