import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import {
  withReducerTrxErrorCode,
  withMiddlewareTrxErrorCode,
} from '@/store/TransactionErrorCode/injector';
import { getList, updateData } from '@/store/TransactionErrorCode/actions';
import { tecSelector } from '@/store/TransactionErrorCode/selectors';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container,
  Message,
  Button,
  Pagination,
  Card,
  Input,
  Column,
  UrlQueryFunc,
  TableWithSelect,
  ValidationFunc,
  ValidationsFunc,
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class ListTransactionErrorCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSearching: false,
      isDisableUpdate: true,
      filter: {
        codeFromHost: '',
        mobileMessage: '',
      },
      errors: {},
      isEdit: false,
    };
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  getDataList(options = { page: false }) {
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    let { page } = options;
    let newPage = 1;
    if (!page && pageOnUrl) {
      newPage = parseInt(pageOnUrl);
    } else {
      newPage = page || newPage;
    }
    this.props.getList(Object.assign(options, { page: newPage }));
  }

  componentWillMount() {
    this.getDataList();
  }

  validationOptions() {
    const { updatedData = [] } = this.state;
    const validationOptions = {};

    updatedData.forEach((item) => {
      validationOptions[item.id] = {
        validator: [ValidationsFunc.Type.required()],
      };
    });

    return validationOptions;
  }

  componentWillReceiveProps(props) {
    const isPropChange = (name) =>
      props[name] && props[name] !== this.props[name];
    const { data } = props;

    if (isPropChange('getListSuccess') || isPropChange('getListFailed')) {
      this.setState({ isSearching: false });
      if (data.number > 0) {
        history.push(
          `/maintenance/transaction_error_code?page=${data.number + 1}`
        );
      } else {
        history.push('/maintenance/transaction_error_code');
      }
    }

    if (data !== this.props.data) {
      var updatedData = [],
        updatedDataValues = {};
      data.content.map((item) => {
        updatedData.push({
          id: item.id,
          codeFromHost: item.codeFromHost,
          transactionErrorDescription: item.transactionErrorDescription,
          mobileMessage: item.mobileMessage,
        });
        updatedDataValues[item.id] = item.mobileMessage;
      });
      this.setState({
        updatedData,
        oldUpdatedData: Object.assign({}, updatedData),
        updatedDataValues,
      });
    }
  }

  setPages(page) {
    const { filter } = this.state;
    this.getDataList({ page: page + 1, ...filter });
  }

  onChangeFilter(data) {
    const { filter } = this.state;
    const { value, name } = data;
    filter[name] = value;
    this.setState({
      filter: filter,
    });
  }

  onSubmitFilter(e) {
    e.preventDefault();
    const { filter } = this.state;
    this.setState({ isSearching: true });
    this.getDataList({ ...filter, page: 1 });
  }

  onChangeForm(data) {
    const { updatedData, oldUpdatedData, updatedDataValues } = this.state;
    const { id, value, name } = data;
    updatedData[id][name] = value;
    updatedDataValues[updatedData[id].id] = value;

    const errors = ValidationsFunc.RunValidate(
      updatedData[id].id,
      updatedDataValues,
      this.validationOptions()
    );

    var isDisableUpdate = ValidationFunc.arrayValidationUpdate(
      oldUpdatedData,
      updatedData
    );

    this.setState({
      updatedData,
      isDisableUpdate,
      updatedDataValues,
      errors,
    });
  }

  onUpdate() {
    const { updatedData, updatedDataValues } = this.state;
    const dataTec = {
      update: updatedData,
    };
    const errors = ValidationsFunc.RunValidateAll(
      updatedDataValues,
      this.validationOptions()
    );
    const errorsKey = Object.keys(errors);

    if (errorsKey.length === 0) {
      this.props.updateData(dataTec);
    } else {
      this.setState({ errors });
    }
  }

  render() {
    const {
      isSearching,
      filter,
      updatedData = [],
      isDisableUpdate,
      isEdit,
      errors,
    } = this.state;
    const {
      location,
      data = { content: [] },
      getListLoading,
      updateDataLoading,
    } = this.props;
    const { state, viewOnly = false, title } = location;
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.transactionErrorCode.list}
        headTitle={title}
      >
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <div className="columns">
            <div className={`column ${isEdit ? 'is-12' : 'is-9'}`}>
              <TableWithSelect
                fullwidth
                language={messages}
                headers={[
                  {
                    label: messages.label.errorCode,
                    value: 'codeFromHost',
                  },
                  {
                    label: messages.label.trxErrorDescription,
                    value: 'transactionErrorDescription',
                  },
                  {
                    label: messages.label.custMessage,
                    customValue: (item, index) => {
                      if (isEdit) {
                        return (
                          <Input
                            name="mobileMessage"
                            value={
                              item.mobileMessage == null
                                ? ''
                                : item.mobileMessage
                            }
                            onChange={this.onChangeForm}
                            error={errors[item.id]}
                            index={index}
                            width={400}
                            maxLength={100}
                            language={messages}
                          />
                        );
                      } else {
                        return item.mobileMessage;
                      }
                    },
                  },
                ]}
                data={isEdit ? updatedData : data.content}
                selectable={false}
                loading={getListLoading || isSearching}
              />
              {!isEdit && (
                <Column>
                  <Column.Content isBorder={false}>
                    <Column.Name
                      bold={false}
                      width={120}
                      text={messages.label.updatedBy}
                    />
                    <Column.Name bold={false} width={20} text=":" />
                    <Column.Value>
                      {data.content.length > 0 && data.content[0].updatedBy}
                    </Column.Value>
                  </Column.Content>
                  <Column.Content isBorder={false}>
                    <Column.Name
                      bold={false}
                      width={120}
                      text={messages.label.lastUpdated}
                    />
                    <Column.Name bold={false} width={20} text=":" />
                    <Column.Value>
                      {data.content.length > 0 &&
                        moment(data.content[0].lastUpdated).format(
                          'DD/MM/YYYY hh:mm:ss A'
                        )}
                    </Column.Value>
                  </Column.Content>
                </Column>
              )}
              {data.totalPages > 1 && !getListLoading && !isEdit && (
                <Pagination
                  setPages={(page) => this.setPages(page)}
                  {...data}
                  language={messages}
                />
              )}
              {isEdit && (
                <div className="footer-form">
                  <Button
                    size="small"
                    onClick={this.onUpdate}
                    disabled={isDisableUpdate}
                    loading={updateDataLoading}
                  >
                    {messages.button.update}
                  </Button>
                  &nbsp;
                  <Button
                    size="small"
                    onClick={() => this.setState({ isEdit: false })}
                  >
                    {messages.button.back}
                  </Button>
                </div>
              )}
            </div>
            {!isEdit && (
              <div className="column is-one-quarter is-hidden-mobile">
                <Card>
                  <Card.Header title={messages.label.filter} />
                  <Card.Content>
                    <div className="content">
                      <form onSubmit={this.onSubmitFilter}>
                        <div className="field">
                          <label>
                            <h6>{messages.label.errorCode}</h6>
                          </label>
                          <Input
                            name="codeFromHost"
                            value={filter.codeFromHost}
                            onChange={this.onChangeFilter}
                            size="small"
                            language={messages}
                          />
                        </div>
                        <div className="field">
                          <label>
                            <h6>{messages.label.custMessage}</h6>
                          </label>
                          <Input
                            name="mobileMessage"
                            value={filter.mobileMessage}
                            onChange={this.onChangeFilter}
                            size="small"
                            language={messages}
                          />
                        </div>
                        <div className="columns">
                          <div className="column is-6">
                            <div className="field">
                              <p className="control">
                                <Button
                                  size="small"
                                  role="submit"
                                  loading={isSearching}
                                  block
                                >
                                  {messages.button.search}
                                </Button>
                              </p>
                            </div>
                          </div>
                          {viewOnly == false && (
                            <div className="column is-6">
                              <div className="field">
                                <p className="control">
                                  <Button
                                    size="small"
                                    onClick={() =>
                                      this.setState({ isEdit: true })
                                    }
                                    block
                                  >
                                    {messages.button.edit}
                                  </Button>
                                </p>
                              </div>
                            </div>
                          )}
                        </div>
                      </form>
                    </div>
                  </Card.Content>
                </Card>
              </div>
            )}
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ListTransactionErrorCode.propTypes = {
  getList: PropTypes.func,
  updateData: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  data: tecSelector('data'),
  getListSuccess: tecSelector('getListSuccess'),
  getListFailed: tecSelector('getListFailed'),
  getListLoading: tecSelector('getListLoading'),
  updateDataSuccess: tecSelector('updateDataSuccess'),
  updateDataFailed: tecSelector('updateDataFailed'),
  updateDataLoading: tecSelector('updateDataLoading'),
});

const mapDispatchToProps = (dispatch) => ({
  getList: (optionData) => dispatch(getList(optionData)),
  updateData: (form) => dispatch(updateData(form)),
});

export default compose(
  withReducerTrxErrorCode,
  withMiddlewareTrxErrorCode,
  connect(mapStateToProps, mapDispatchToProps)
)(ListTransactionErrorCode);
