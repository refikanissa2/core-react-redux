import React from 'react';

import language from '@/helpers/messages';
import { TableWithSelect } from 'component-ui-web-teravin';
const messages = language.getLanguage();

export default class DetailColumn extends React.Component {
  render() {
    const { data = {update: []}, loading} = this.props;
    return (
      <TableWithSelect
        fullwidth
        selectable={false}
        data={data.update}
        loading={loading}
        headers={[
          {
            label: messages.label.errorCode,
            value: 'codeFromHost'
          },{
            label: messages.label.trxErrorDescription,
            customValue: item =>
              <p style={{minWidth: '400px'}}>
                {item.transactionErrorDescription}
              </p>
          },{
            label: messages.label.custMessage,
            customValue: item =>
              <p style={{minWidth: '400px'}}>
                {item.mobileMessage}
              </p>
          }
        ]}
      />
    );
  }
}
