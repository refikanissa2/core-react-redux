import React, { Component } from 'react';

import DetailTask from '@/pages/MyTask/ApprovalMaintenance/detail-myTask';
import DetailColumn from './detail-column-tec';
import language from '@/helpers/messages';
import { Title } from 'component-ui-web-teravin';
const messages = language.getLanguage();

class DetailApproval extends Component {
  render() {
    const { location, match } = this.props;
    const taskType = match.params.taskType;
    return (
      <DetailTask
        module='trx_error_code'
        title={messages.menu.transactionErrorCode[taskType]}
        location={location}
        taskType={taskType}
      >
        {props => {
          const {data = {}, oldData = {}, getPendingTaskLoading} = props;
          return(
            <div>
              <div className="columns">
                <div className='column is-12'>
                  <DetailColumn
                    data={taskType === 'edit' ? oldData : data}
                    loading={getPendingTaskLoading}
                  />
                </div>
              </div>
              {taskType === 'edit' && (
                <div className='columns'>
                  <div className='column is-12'>
                    <Title title="Data to change" />
                    <DetailColumn
                      data={data}
                      extraLabel={messages.label.toChange}
                      loading={getPendingTaskLoading}
                    />
                  </div>
                </div>
              )}
            </div>
          );
        }}
      </DetailTask>
    );
  }
}

export default DetailApproval;
