import React, { Component } from 'react';

import history from '@/routes/history';
import language from '@/helpers/messages';
import DetailColumn from './detail-column-tec';
import HomeBase from '@/pages/home';
import {
  Container, Button, Message
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class ApprovalShowTec extends Component {
  render () {
    const {location} = this.props;
    const {state, title} = location;
    const {data} = state;
    return (
      <HomeBase breadcrumb={location} title={messages.menu.transactionErrorCode.detail} headTitle={title}>
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
            autoClose={false}
          />
          <div className="columns">
            <div className="column is-12">
              <DetailColumn data={data} />
              <div className="footer-form">
                <Button
                  size="small"
                  onClick={() => history.push('/maintenance/transaction_error_code')}
                >{messages.button.back}</Button>
              </div>
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

export default ApprovalShowTec;
