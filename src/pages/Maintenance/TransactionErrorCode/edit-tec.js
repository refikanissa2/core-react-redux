import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { tecSelector } from '@/store/TransactionErrorCode/selectors';
import { updateData } from '@/store/TransactionErrorCode/actions';
import { withReducerTrxErrorCode, withMiddlewareTrxErrorCode } from '@/store/TransactionErrorCode/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Table, Button, Input,
  ValidationFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class EditTransactionErrorCode extends Component {
  constructor(){
    super();
    this.state = {
      loadingPage: false,
      isDisableUpdate: true,
      isUpdating: false,
      updateContent: [],
      errors: []
    };
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  componentDidMount(){
    this.setState({loadingPage: true});
    const {data, location} = this.props;
    const {state} = location;
    if(state){
      if(state.filter){
        window.onpopstate = this.onBackBrowser.bind(this, state.filter, data.number+1);
      }
    }
    var updateContent = [];
    var content = [];
    var errors = [];
    data.content.map(item => {
      var newData = {
        id: item.id,
        codeFromHost: item.codeFromHost,
        transactionErrorDescription: item.transactionErrorDescription,
        mobileMessage: item.mobileMessage
      };
      updateContent.push(newData);
      var newContent = Object.assign({}, newData);
      content.push(newContent);
      errors.push('');
    });
    this.setState({content: content, updateContent: updateContent, errors: errors, loadingPage: false});
  }

  onBackBrowser(filter, page){
    history.push('/maintenance/transaction_error_code?page='+page, {filter: filter});
  }

  componentWillReceiveProps(nextProps) {
    const {updateDataSuccess, updateDataFailed} = nextProps;
    if(updateDataSuccess || updateDataFailed){
      this.setState({isUpdating: false});
    }
  }

  onChangeForm(data){
    var {updateContent, content, errors} = this.state;
    const {id, value, name} = data;
    updateContent[id][name] = value;
    errors[id] = '';
    var isDisableUpdate = ValidationFunc.arrayValidationUpdate(content, updateContent);
    this.setState({
      updateContent: updateContent,
      errors: errors,
      isDisableUpdate: isDisableUpdate
    });
  }

  onUpdate(){
    this.setState({isUpdating: true});
    const {updateContent, errors} = this.state;
    var dataTec = {};
    dataTec['update'] = updateContent;
    var errorCount = 0;
    updateContent.map((item, index) => {
      if(item.mobileMessage == ''){
        errorCount += 1;
        errors[index] = messages.text.required;
      }
    });
    if(errorCount == 0){
      this.props.updateData(dataTec);
      return;
    }
    this.setState({errors: errors, isUpdating: false});
  }

  render() {
    const {loadingPage, isDisableUpdate, isUpdating, updateContent, errors} = this.state;
    const {location, data} = this.props;
    const {title, state} = location;
    return (
      <HomeBase loader={loadingPage} breadcrumb={location} title={messages.menu.transactionErrorCode.edit} headTitle={title}>
        <Container fluid>
          <div className="columns">
            <div className="column is-12">
              <Table fullwidth>
                <Table.Head>
                  <Table.Label align="start" label={messages.label.errorCode}/>
                  <Table.Label align="start" label={messages.label.trxErrorDescription}/>
                  <Table.Label align="start" label={messages.label.custMessage+' *'}/>
                </Table.Head>
                <Table.Body>
                  {updateContent.length > 0 ? (
                    updateContent.map((item, index) =>
                      <Table.Values key={index}>
                        <Table.Value align="start" value={item.codeFromHost} />
                        <Table.Value align="start">
                          <p style={{minWidth: '400px'}}>
                            {item.transactionErrorDescription}
                          </p>
                        </Table.Value>
                        <Table.Value align="start">
                          <div style={{minWidth: '400px'}}>
                            <Input
                              name="mobileMessage"
                              value={item.mobileMessage == null ? '' : item.mobileMessage}
                              onChange={this.onChangeForm}
                              error={errors[index]}
                              index={index}
                              width={400}
                              maxLength={100}
                              language={messages}
                            />
                          </div>
                        </Table.Value>
                      </Table.Values>
                    )
                  ):(
                    <Table.Values>
                      <Table.Value
                        value={messages.text.empty}
                        colspan="3"
                        bold
                      />
                    </Table.Values>
                  )}
                </Table.Body>
              </Table>
              <div className="footer-form">
                <Button
                  size="small"
                  onClick={this.onUpdate}
                  disabled={isDisableUpdate}
                  loading={isUpdating}
                >{messages.button.update}</Button>&nbsp;
                <Button
                  size="small"
                  onClick = {() => history.push('/maintenance/transaction_error_code?page='+(data.number+1), {filter: state.filter})}
                >{messages.button.back}</Button>
              </div>
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

EditTransactionErrorCode.propTypes = {
  updateData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  updateDataSuccess: tecSelector('updateDataSuccess'),
  updateDataFailed: tecSelector('updateDataFailed'),
  data: tecSelector('data')
});

const mapDispatchToProps = dispatch => ({
  updateData: (data) => dispatch(updateData(data))
});

export default compose(
  withReducerTrxErrorCode,
  withMiddlewareTrxErrorCode,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(EditTransactionErrorCode);
