import React, { Component } from 'react';

import DetailTask from '@/pages/MyTask/ApprovalMaintenance/detail-myTask';
import DetailColumn from './detail-column-district';
import language from '@/helpers/messages';
import { ContentLoader } from 'component-ui-web-teravin';
const messages = language.getLanguage();

class DetailApproval extends Component {
  render() {
    const { location, match } = this.props;
    const taskType = match.params.taskType;
    return (
      <DetailTask
        module='district'
        title={messages.menu.district[taskType]}
        location={location}
        taskType={taskType}
      >
        {props => {
          const {data = {}, oldData = {}, getPendingTaskLoading} = props;
          return(
            <div className="columns">
              <div className={`column ${taskType == 'edit' ? 'is-half' : ''}`}>
                {getPendingTaskLoading ? (
                  <div className="columns">
                    <div className={`column ${taskType == 'edit' ? 'is-half' : ''}`}>
                      <ContentLoader.Detail />
                    </div>
                  </div>
                ):(
                  <DetailColumn
                    data={taskType === 'edit' ? oldData : data}
                  />
                )}
              </div>
              {taskType === 'edit' && (
                <div className='column is-half'>
                  {getPendingTaskLoading ? (
                    <div className="columns">
                      <div className='column is-half'>
                        <ContentLoader.Detail />
                      </div>
                    </div>
                  ):(
                    <DetailColumn
                      data={data}
                      extraLabel={messages.label.toChange}
                    />
                  )}
                </div>
              )}
            </div>
          );
        }}
      </DetailTask>
    );
  }
}

export default DetailApproval;
