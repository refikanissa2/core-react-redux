import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { districtSelector } from '@/store/District/selectors';
import { addData, updateData } from '@/store/District/actions';
import {
  withReducerDistrict,
  withMiddlewareDistrict
} from '@/store/District/injector';

import { droplistSelector } from '@/store/Droplist/selectors';
import { getAllCity } from '@/store/Droplist/actions';
import { withMiddlewareDroplist, withReducerDroplist } from '@/store/Droplist/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';
import colors from '@/assets/sass/colors.scss';

import HomeBase from '@/pages/home';
import {
  Container,
  Button,
  Column,
  Input,
  Textarea,
  UrlQueryFunc,
  ValidationsFunc,
  ArrayFunc,
  SelectSearch,
  ValidationFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class AddDistrict extends Component {
  constructor(props) {
    super(props);
    const editPath = '/maintenance/district/update';
    const { location = {} } = props;
    const { pathname = '' } = location;
    this.state = {
      form: {
        code: '',
        name: '',
        cityId: '',
        description: '',
        hostSourceMappingDto: []
      },
      errors: {
        code: '',
        name: '',
        cityId: ''
      },
      isEdit: pathname === editPath,
      isDisableUpdate: true
    };
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    this.props.getAllCity();
    const { isEdit } = this.state;
    if (isEdit) {
      const { detailData } = this.props;
      if (!detailData) {
        const { state } = this.props.location;
        const id = UrlQueryFunc.getQueryString('id');
        history.push('/maintenance/district/detail?id=' + id, {
          dataDetail: state ? state.dataDetail : '',
          filter: state ? state.filter : {}
        });
      } else {
        var form = {
          id: detailData.id,
          code: detailData.code,
          name: detailData.name,
          cityId: detailData.cityId,
          description: detailData.description,
          hostSourceMappingDto: detailData.hostSourceMappingDto,
          index: detailData.index
        };
        this.setState({
          form: form,
          oldData: Object.assign({}, form)
        });
      }
    }
  }

  validationOptions() {
    const validationOptions = {
      code: {
        validator: [ValidationsFunc.Type.required()]
      },
      name: {
        validator: [ValidationsFunc.Type.required()]
      },
      countryId: {
        validator: [ValidationsFunc.Type.required()]
      },
      description: {
        validator: []
      }
    };
    return validationOptions;
  }

  onChangeForm(data) {
    let { form, oldData, isDisableUpdate } = this.state;
    const { name, value } = data;

    form[name] = value;

    const errors = ValidationsFunc.RunValidate(
      name,
      form,
      this.validationOptions()
    );

    if(oldData){
      isDisableUpdate = ValidationFunc.objValidationUpdate(oldData, form);
    }

    this.setState({
      form,
      errors,
      isDisableUpdate
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const { form, isEdit } = this.state;
    const errors = ValidationsFunc.RunValidateAll(
      form,
      this.validationOptions()
    );
    const errorsKey = Object.keys(errors);

    if (errorsKey.length === 0) {
      if (isEdit) {
        this.props.updateData(form);
      } else {
        this.props.addData(
          Object.assign({}, form, {
            hostSourceMappingDto: [
              {
                hostCode: 'esb',
                moduleValue: form.code
              }
            ]
          })
        );
      }
    } else {
      this.setState({ errors });
    }
  }

  render() {
    const { form, errors, isEdit, isDisableUpdate } = this.state;
    const {
      location,
      cities = [],
      getAllCityLoading,
      addDataLoading,
      updateDataLoading
    } = this.props;
    const { title, state } = location;
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.district.add}
        headTitle={title}
      >
        <Container fluid>
          <form onSubmit={this.onSubmit}>
            <Column>
              <Column.Content>
                <Column.Name text={messages.label.code + ' *'} />
                <Column.Value>
                  <Input.InputCode
                    name="code"
                    value={form.code}
                    maxLength={5}
                    onChange={this.onChangeForm}
                    error={errors.code}
                    width={300}
                    language={messages}
                    disabled={isEdit}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.name + ' *'} />
                <Column.Value>
                  <Input
                    name="name"
                    value={form.name}
                    onChange={this.onChangeForm}
                    error={errors.name}
                    width={300}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.menu.city.title + ' *'} />
                <Column.Value>
                  <SelectSearch
                    name="cityId"
                    width={getAllCityLoading ? 270 : 300}
                    value={form.cityId}
                    error={errors.cityId}
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(cities, 'id', 'name')}
                    disabled={getAllCityLoading}
                    isClearable={false}
                    isLoading={getAllCityLoading}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.description} />
                <Column.Value>
                  <Textarea
                    name="description"
                    value={form.description}
                    onChange={this.onChangeForm}
                    width={300}
                  />
                </Column.Value>
              </Column.Content>
            </Column>
            <div className="footer-form">
              <Button
                role="submit"
                size="small"
                loading={addDataLoading || updateDataLoading}
                disabled={isEdit && isDisableUpdate}
              >
                {isEdit ? messages.button.update : messages.button.add}
              </Button>
              &nbsp;
              <Button
                size="small"
                onClick={() =>
                  isEdit
                    ? history.push(
                      `/maintenance/district/detail?id=${form.id}`,
                      {
                        dataDetail: state ? state.dataDetail : '',
                        filter: state ? state.filter : {}
                      }
                    )
                    : history.push('/maintenance/district')
                }
              >
                {messages.button.cancel}
              </Button>
            </div>
          </form>
        </Container>
      </HomeBase>
    );
  }
}

AddDistrict.propTypes = {
  addData: PropTypes.func,
  getAllCity: PropTypes.func,
  updateData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  detailData: districtSelector('detailData'),
  addDataLoading: districtSelector('addDataLoading'),
  updateDataLoading: districtSelector('updateDataLoading'),
  getAllCityLoading: droplistSelector('getAllCityLoading'),
  cities: droplistSelector('cityList')
});

const mapDispatchToProps = dispatch => ({
  addData: form => dispatch(addData(form)),
  getAllCity: () => dispatch(getAllCity()),
  updateData: form => dispatch(updateData(form))
});

export default compose(
  withReducerDistrict,
  withMiddlewareDistrict,
  withMiddlewareDroplist,
  withReducerDroplist,
  connect(mapStateToProps, mapDispatchToProps)
)(AddDistrict);
