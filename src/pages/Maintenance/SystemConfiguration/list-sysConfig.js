import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import {
  withReducerSysConfig,
  withMiddlewareSysConfig
} from '@/store/SystemConfiguration/injector';
import { getList } from '@/store/SystemConfiguration/actions';
import { sysConfigSelector } from '@/store/SystemConfiguration/selectors';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container,
  Message,
  Button,
  Pagination,
  Card,
  Input,
  UrlQueryFunc,
  TableWithSelect
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class ListSystemConfiguration extends Component {
  constructor() {
    super();
    this.state = {
      isSearching: false,
      filter: {
        code: ''
      }
    };
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
  }

  getDataList(options = { page: false }) {
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    let { page } = options;
    let newPage = 1;
    if (!page && pageOnUrl) {
      newPage = parseInt(pageOnUrl);
    } else {
      newPage = page || newPage;
    }
    this.props.getList(Object.assign(options, { page: newPage }));
  }

  componentWillMount() {
    let filter = {};
    const { state } = this.props.location;
    if (state) {
      if (state.filter) {
        filter = state.filter;
        this.setState({ filter: state.filter });
      }
    }
    this.getDataList({ ...filter });
  }

  componentWillReceiveProps(props) {
    const isPropChange = name =>
      props[name] && props[name] !== this.props[name];
    const { data } = props;

    if (isPropChange('getListSuccess') || isPropChange('getListFailed')) {
      this.setState({ isSearching: false });
      if (data.number > 0) {
        history.push(
          `/maintenance/system_configuration?page=${data.number + 1}`
        );
      } else {
        history.push('/maintenance/system_configuration');
      }
    }
  }

  onSort(data) {
    var tmpData = data.map(x => x.code).sort();
    var newData = [];
    tmpData.map(dataTmp => {
      data.map(data => {
        if (data.code == dataTmp) {
          newData.push(data);
        }
      });
    });
    return newData;
  }

  setPages(page) {
    const { filter } = this.state;
    this.getDataList({ page: page + 1, ...filter });
  }

  onChangeFilter(data) {
    const { filter } = this.state;
    const { value, name } = data;
    filter[name] = value;
    this.setState({
      filter: filter
    });
  }

  onSubmitFilter(e) {
    e.preventDefault();
    const { filter } = this.state;
    this.setState({ isSearching: true });
    this.getDataList({ ...filter, page: 1 });
  }

  render() {
    const { isSearching, filter } = this.state;
    const { location, data = { content: [] }, getListLoading } = this.props;
    const { state, title } = location;
    var content = [];
    if (data.content) {
      content = this.onSort(data.content);
    }
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.systemConfiguration.list}
        headTitle={title}
      >
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <div className="columns">
            <div className="column is-9">
              <TableWithSelect
                fullwidth
                language={messages}
                headers={[
                  {
                    label: messages.label.name,
                    value: 'code'
                  },
                  {
                    label: messages.label.value,
                    value: 'name'
                  },
                  {
                    label: messages.label.description,
                    value: 'description'
                  }
                ]}
                data={content}
                onSelected={index =>
                  history.push(
                    `/maintenance/system_configuration/detail?id=${data.content[index].id}`,
                    {
                      dataDetail: `${data.content[index].code} - ${data.content[index].name}`,
                      filter: filter
                    }
                  )
                }
                loading={getListLoading || isSearching}
              />
              {data.totalPages > 1 && !getListLoading && (
                <Pagination
                  setPages={page => this.setPages(page)}
                  {...data}
                  language={messages}
                />
              )}
            </div>
            <div className="column is-one-quarter is-hidden-mobile">
              <Card>
                <Card.Header title={messages.label.filter} />
                <Card.Content>
                  <div className="content">
                    <form onSubmit={this.onSubmitFilter}>
                      <div className="field">
                        <label>
                          <h6>{messages.label.name}</h6>
                        </label>
                        <Input
                          name="code"
                          value={filter.code}
                          onChange={this.onChangeFilter}
                          size="small"
                          maxLength={100}
                          language={messages}
                        />
                      </div>
                      <div className="columns">
                        <div className="column is-6">
                          <div className="field">
                            <p className="control">
                              <Button
                                size="small"
                                role="submit"
                                loading={isSearching}
                                block
                              >
                                {messages.button.search}
                              </Button>
                            </p>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </Card.Content>
              </Card>
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ListSystemConfiguration.propTypes = {
  getList: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: sysConfigSelector('data'),
  getListSuccess: sysConfigSelector('getListSuccess'),
  getListFailed: sysConfigSelector('getListFailed'),
  getListLoading: sysConfigSelector('getListLoading')
});

const mapDispatchToProps = dispatch => ({
  getList: optionData => dispatch(getList(optionData))
});

export default compose(
  withReducerSysConfig,
  withMiddlewareSysConfig,
  connect(mapStateToProps, mapDispatchToProps)
)(ListSystemConfiguration);
