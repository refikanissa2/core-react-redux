import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import {
  withReducerSysConfig,
  withMiddlewareSysConfig
} from '@/store/SystemConfiguration/injector';
import { sysConfigSelector } from '@/store/SystemConfiguration/selectors';
import { getDetail } from '@/store/SystemConfiguration/actions';

import history from '@/routes/history';
import language from '@/helpers/messages';
import DetailColumn from './detail-column-sysConfig';
import HomeBase from '@/pages/home';
import {
  Container,
  Button,
  UrlQueryFunc,
  ContentLoader
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class DetailSystemConfiguration extends Component {
  componentDidMount() {
    const { location, dataList } = this.props;
    const { state } = location;
    if (state) {
      if (state.filter) {
        window.onpopstate = this.onBackBrowser.bind(
          this,
          state.filter,
          dataList ? dataList.number + 1 : 1
        );
      }
    }
    const id = UrlQueryFunc.getQueryString('id');
    this.props.getDetail({ id: id, dataDetail: state ? state.dataDetail : '' });
  }

  onBackBrowser(filter, page) {
    history.push('/maintenance/system_configuration?page=' + page, {
      filter: filter
    });
  }

  render() {
    const { location, data = {}, dataList = {}, getDetailLoading } = this.props;
    const { viewOnly, title, state } = location;
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.systemConfiguration.detail}
        headTitle={title}
      >
        <Container fluid>
          {getDetailLoading ? (
            <ContentLoader.Detail />
          ) : (
            <DetailColumn data={data} createdInfo />
          )}
          <div className="footer-form">
            {viewOnly == false && (
              <span>
                <Button
                  size="small"
                  onClick={() =>
                    history.push(
                      `/maintenance/system_configuration/update?id=${data.id}`,
                      {
                        dataDetail: state ? state.dataDetail : '',
                        filter: state ? state.filter : {}
                      }
                    )
                  }
                >
                  {messages.button.edit}
                </Button>
                &nbsp;
              </span>
            )}
            <Button
              size="small"
              onClick={() =>
                history.push(
                  '/maintenance/system_configuration?page=' +
                    (dataList ? dataList.number + 1 : 1),
                  { filter: state ? state.filter : {} }
                )
              }
            >
              {messages.button.back}
            </Button>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

DetailSystemConfiguration.propTypes = {
  getDetail: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: sysConfigSelector('detailData'),
  getDetailLoading: sysConfigSelector('getDetailLoading'),
  dataList: sysConfigSelector('data')
});

const mapDispatchToProps = dispatch => ({
  getDetail: data => dispatch(getDetail(data))
});

export default compose(
  withReducerSysConfig,
  withMiddlewareSysConfig,
  connect(mapStateToProps, mapDispatchToProps)
)(DetailSystemConfiguration);
