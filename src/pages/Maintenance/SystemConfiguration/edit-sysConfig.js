import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { sysConfigSelector } from '@/store/SystemConfiguration/selectors';
import { updateData } from '@/store/SystemConfiguration/actions';
import {
  withReducerSysConfig,
  withMiddlewareSysConfig
} from '@/store/SystemConfiguration/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container,
  Button,
  Column,
  Input,
  Textarea,
  UrlQueryFunc,
  ValidationFunc,
  ValidationsFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class EditSystemConfiguration extends Component {
  constructor() {
    super();
    this.state = {
      form: {
        id: '',
        name: '',
        code: '',
        description: ''
      },
      errors: {
        name: ''
      },
      isDisableUpdate: true
    };
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  componentDidMount() {
    const { detailData } = this.props;
    if (!detailData) {
      const { state } = this.props.location;
      const id = UrlQueryFunc.getQueryString('id');
      history.push('/maintenance/system_configuration/detail?id=' + id, {
        dataDetail: state ? state.dataDetail : '',
        filter: state ? state.filter : {}
      });
    } else {
      var form = {
        id: detailData.id,
        name: detailData.name,
        code: detailData.code,
        description: detailData.description
      };
      this.setState({
        form: form,
        oldData: Object.assign({}, form)
      });
    }
  }

  validationOptions() {
    const validationOptions = {
      code: {
        validator: []
      },
      name: {
        validator: [ValidationsFunc.Type.required()]
      },
      description: {
        validator: []
      }
    };
    return validationOptions;
  }

  onChangeForm(data) {
    const { form, oldData } = this.state;
    const { name, value } = data;
    form[name] = value;
    const isDisableUpdate = ValidationFunc.objValidationUpdate(oldData, form);

    const errors = ValidationsFunc.RunValidate(
      name,
      form,
      this.validationOptions()
    );

    this.setState({
      form,
      errors,
      isDisableUpdate
    });
  }

  onUpdate(e) {
    e.preventDefault();
    const { form } = this.state;
    const errors = ValidationsFunc.RunValidateAll(
      form,
      this.validationOptions()
    );
    const errorsKey = Object.keys(errors);

    if (errorsKey.length === 0) {
      this.props.updateData(form);
    } else {
      this.setState({ errors });
    }
  }

  render() {
    const { isDisableUpdate, form, errors } = this.state;
    const { location, updateDataLoading } = this.props;
    const { title, state } = location;
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.systemConfiguration.edit}
        headTitle={title}
      >
        <Container fluid>
          <form onSubmit={this.onUpdate}>
            <Column>
              <Column.Content>
                <Column.Name text={messages.label.name} />
                <Column.Value>
                  <Input.InputCode
                    name="code"
                    value={form.code}
                    error={errors.code}
                    onChange={this.onChangeForm}
                    width={300}
                    maxLength={40}
                    language={messages}
                    disabled={true}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.value + ' *'} />
                <Column.Value>
                  <Input
                    name="name"
                    value={form.name}
                    error={errors.name}
                    onChange={this.onChangeForm}
                    width={300}
                    maxLength={20}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.description} />
                <Column.Value>
                  <Textarea
                    name="description"
                    value={form.description}
                    onChange={this.onChangeForm}
                    width={300}
                    maxLength={500}
                  />
                </Column.Value>
              </Column.Content>
            </Column>
            <div className="footer-form">
              <Button
                role="submit"
                size="small"
                disabled={isDisableUpdate}
                loading={updateDataLoading}
              >
                {messages.button.update}
              </Button>
              &nbsp;
              <Button
                size="small"
                onClick={() =>
                  history.push(
                    `/maintenance/system_configuration/detail?id=${form.id}`,
                    {
                      dataDetail: state ? state.dataDetail : '',
                      filter: state ? state.filter : {}
                    }
                  )
                }
              >
                {messages.button.cancel}
              </Button>
            </div>
          </form>
        </Container>
      </HomeBase>
    );
  }
}

EditSystemConfiguration.propTypes = {
  updateData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  detailData: sysConfigSelector('detailData'),
  updateDataLoading: sysConfigSelector('updateDataLoading')
});

const mapDispatchToProps = dispatch => ({
  updateData: data => dispatch(updateData(data))
});

export default compose(
  withReducerSysConfig,
  withMiddlewareSysConfig,
  connect(mapStateToProps, mapDispatchToProps)
)(EditSystemConfiguration);
