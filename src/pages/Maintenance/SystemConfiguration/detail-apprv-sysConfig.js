import React, { Component } from 'react';

import DetailTask from '@/pages/MyTask/ApprovalMaintenance/detail-myTask';
import DetailColumn from './detail-column-sysConfig';
import language from '@/helpers/messages';
import { ContentLoader } from 'component-ui-web-teravin';
const messages = language.getLanguage();

class DetailApproval extends Component {
  render() {
    const { location, match } = this.props;
    const taskType = match.params.taskType;
    return (
      <DetailTask
        module='sys_config'
        title={messages.menu.systemConfiguration[taskType]}
        location={location}
        taskType={taskType}
      >
        {props => {
          const {getPendingTaskLoading} = props;
          return(
            <div className="columns">
              <div className={`column ${taskType == 'edit' ? 'is-half' : ''}`}>
                {getPendingTaskLoading ? (
                  <div className="columns">
                    <div className={`column ${taskType == 'edit' ? 'is-half' : ''}`}>
                      <ContentLoader.Detail />
                    </div>
                  </div>
                ):(
                  <DetailColumn
                    data={taskType === 'edit' ? props.oldData : props.data}
                  />
                )}
              </div>
              {taskType === 'edit' && (
                <div className='column is-half'>
                  {getPendingTaskLoading ? (
                    <div className="columns">
                      <div className='column is-half'>
                        <ContentLoader.Detail />
                      </div>
                    </div>
                  ):(
                    <DetailColumn
                      data={props.data}
                      extraLabel={messages.label.toChange}
                    />
                  )}
                </div>
              )}
            </div>
          );
        }}
      </DetailTask>
    );
  }
}

export default DetailApproval;
