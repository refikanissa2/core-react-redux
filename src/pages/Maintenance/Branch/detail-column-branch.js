import React from 'react';
import moment from 'moment';

import language from '@/helpers/messages';
import { Column } from 'component-ui-web-teravin';
const messages = language.getLanguage();

export default class DetailColumn extends React.Component {
  render() {
    const { data = {}, extraLabel = '', createdInfo = false } = this.props;
    return (
      <div>
        <Column>
          <Column.Content>
            <Column.Name text={`${messages.label.code} ${extraLabel}`} />
            <Column.Value>{data.code}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.name} ${extraLabel}`} />
            <Column.Value><p className="wrap-text">{data.name}</p></Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.address+' 1'} ${extraLabel}`} />
            <Column.Value><p className="wrap-text">{data.address1}</p></Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.address+' 2'} ${extraLabel}`} />
            <Column.Value><p className="wrap-text">{data.address2}</p></Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.phoneNumber} ${extraLabel}`} />
            <Column.Value>{data.phoneNo}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.menu.country.title} ${extraLabel}`} />
            <Column.Value>{data.countryName}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.menu.province.title} ${extraLabel}`} />
            <Column.Value>{data.provinceName}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.menu.city.title} ${extraLabel}`} />
            <Column.Value>{data.cityName}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.menu.district.title} ${extraLabel}`} />
            <Column.Value>{data.districtName}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.menu.subDistrict.title} ${extraLabel}`} />
            <Column.Value>{data.subDistrictName}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.lng} ${extraLabel}`} />
            <Column.Value>{data.longt}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.phoneNumber} ${extraLabel}`} />
            <Column.Value>{data.lat}</Column.Value>
          </Column.Content>
        </Column>
        {createdInfo &&
          <Column>
            <Column.Content>
              <Column.Name text={messages.label.createdBy}/>
              <Column.Value>{data.createdBy}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.dateCreated}/>
              <Column.Value>{data.dateCreated == null ? '' : moment(data.dateCreated).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.updatedBy}/>
              <Column.Value>{data.updatedBy}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.lastUpdated}/>
              <Column.Value>{data.lastUpdated == null ? '' : moment(data.lastUpdated).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
            </Column.Content>
          </Column>
        }
      </div>
    );
  }
}
