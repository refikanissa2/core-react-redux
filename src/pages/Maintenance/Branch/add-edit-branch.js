import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { branchSelector } from '@/store/Branch/selectors';
import { addData, updateData } from '@/store/Branch/actions';
import {
  withReducerBranch,
  withMiddlewareBranch,
} from '@/store/Branch/injector';

import { droplistSelector } from '@/store/Droplist/selectors';
import { getAllCountry, getAllProvince, getAllCity, getAllDistrict, getAllSubdistrict } from '@/store/Droplist/actions';
import {
  withReducerDroplist,
  withMiddlewareDroplist,
} from '@/store/Droplist/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';
import colors from '@/assets/sass/colors.scss';

import HomeBase from '@/pages/home';
import {
  Container,
  Button,
  Column,
  Input,
  Textarea,
  ValidationFunc,
  UrlQueryFunc,
  ValidationsFunc,
  ArrayFunc,
  SelectSearch,
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class AddBranch extends Component {
  constructor(props) {
    super(props);
    const editPath = '/maintenance/branch/update';
    const { location = {} } = props;
    const { pathname = '' } = location;
    this.state = {
      form: {
        code: '',
        name: '',
        address1: '',
        address2: '',
        phoneNo: '',
        countryId: '',
        provinceId: '',
        cityId: '',
        districtId: '',
        subDistrictId: '',
        lat: '',
        longt: '',
      },
      errors: {
        code: '',
        name: '',
        address1: '',
        phoneNo: '',
        countryId: '',
        provinceId: '',
        cityId: '',
        districtId: '',
        subDistrictId: '',
      },
      errorLatLng: {
        lat: '',
        longt: '',
      },
      isEdit: pathname === editPath,
      isDisableUpdate: true,
    };
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    this.props.getAllCountry();
    const { isEdit } = this.state;
    if (isEdit) {
      const { detailData } = this.props;
      if (!detailData) {
        const { state } = this.props.location;
        const id = UrlQueryFunc.getQueryString('id');
        history.push('/maintenance/branch/detail?id=' + id, {
          dataDetail: state ? state.dataDetail : '',
          filter: state ? state.filter : {},
        });
      } else {
        var form = {
          id: detailData.id,
          code: detailData.code,
          name: detailData.name,
          address1: detailData.address1,
          address2: detailData.address2,
          phoneNo: detailData.phoneNo,
          countryId: detailData.countryId != null ? detailData.countryId : '',
          provinceId:
            detailData.provinceId != null ? detailData.provinceId : '',
          cityId: detailData.cityId != null ? detailData.cityId : '',
          districtId:
            detailData.districtId != null ? detailData.districtId : '',
          subDistrictId:
            detailData.subDistrictId != null ? detailData.subDistrictId : '',
          lat: detailData.lat,
          longt: detailData.longt,
        };
        this.setState({
          form: form,
          oldData: Object.assign({}, form),
        });
      }
    }
  }

  componentWillReceiveProps(props) {
    const isPropChange = (name) =>
      props[name] && props[name] !== this.props[name];
    const { form } = this.state;

    if (isPropChange('getAllCountrySuccess')) {
      if (form.countryId) {
        this.props.getAllProvince(form.countryId);
      }
    }

    if (isPropChange('getAllProvinceSuccess')) {
      if (form.provinceId) {
        this.props.getAllCity(form.provinceId);
      }
    }

    if (isPropChange('getAllCitySuccess')) {
      if (form.cityId) {
        this.props.getAllDistrict(form.cityId);
      }
    }

    if (isPropChange('getAllDistrictSuccess')) {
      console.log('district success');
      if (form.districtId) {
        this.props.getAllSubdistrict(form.districtId);
      }
    }
  }

  validationOptions() {
    const validationOptions = {
      code: {
        validator: [ValidationsFunc.Type.required()],
      },
      name: {
        validator: [ValidationsFunc.Type.required()],
      },
      address1: {
        validator: [ValidationsFunc.Type.required()],
      },
      phoneNo: {
        validator: [ValidationsFunc.Type.required()],
      },
      countryId: {
        validator: [ValidationsFunc.Type.required()],
      },
      provinceId: {
        validator: [ValidationsFunc.Type.required()],
      },
      cityId: {
        validator: [ValidationsFunc.Type.required()],
      },
      districtId: {
        validator: [ValidationsFunc.Type.required()],
      },
      subDistrictId: {
        validator: [ValidationsFunc.Type.required()],
      },
      lat: {
        validator: [ValidationsFunc.Type.geolocation({ type: 'latitude' })],
      },
      longt: {
        validator: [ValidationsFunc.Type.geolocation({ type: 'longitude' })],
      },
    };
    return validationOptions;
  }

  onChangeForm(data) {
    let { form, oldData, isDisableUpdate } = this.state;
    const { name, value } = data;
    form[name] = value;

    const errors = ValidationsFunc.RunValidate(
      name,
      form,
      this.validationOptions()
    );

    if (oldData) {
      isDisableUpdate = ValidationFunc.objValidationUpdate(oldData, form);
    }

    this.setState({
      form,
      errors,
      isDisableUpdate,
    });
    if (name == 'countryId') {
      form.provinceId = '';
      form.cityId = '';
      form.districtId = '';
      form.subDistrictId = '';
      this.setState({
        form,
      });
      this.props.getAllProvince(value);
      return;
    }
    if (name == 'provinceId') {
      form.cityId = '';
      form.districtId = '';
      form.subDistrictId = '';
      this.setState({
        form,
      });
      this.props.getAllCity(value);
      return;
    }
    if (name == 'cityId') {
      form.districtId = '';
      form.subDistrictId = '';
      this.setState({
        form,
      });
      this.props.getAllDistrict(value);
      return;
    }
    if (name == 'districtId') {
      form.subDistrictId = '';
      this.setState({
        form,
      });
      this.props.getAllSubdistrict(value);
      return;
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const { form, isEdit } = this.state;
    const errors = ValidationsFunc.RunValidateAll(
      form,
      this.validationOptions()
    );
    const errorsKey = Object.keys(errors);

    if (errorsKey.length === 0) {
      if (isEdit) {
        this.props.updateData(form);
      } else {
        this.props.addData(form);
      }
    }
  }

  render() {
    const { form, errors, isEdit, isDisableUpdate } = this.state;
    const {
      location,
      countries = [],
      provinces = [],
      cities = [],
      districts = [],
      subDistricts = [],
      getAllCountryLoading,
      getAllProvinceLoading,
      getAllCityLoading,
      getAllDistrictLoading,
      getAllSubdistrictLoading,
      addDataLoading,
      updateDataLoading,
    } = this.props;
    const { title, state } = location;
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.branch[isEdit ? 'edit' : 'add']}
        headTitle={title}
      >
        <Container fluid>
          <form onSubmit={this.onSubmit}>
            <Column>
              <Column.Content>
                <Column.Name text={`${messages.label.code} *`} />
                <Column.Value>
                  <Input.InputCode
                    name="code"
                    value={form.code}
                    error={errors.code}
                    onChange={this.onChangeForm}
                    width={300}
                    maxLength={5}
                    language={messages}
                    disabled={isEdit}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.name + ' *'} />
                <Column.Value>
                  <Input
                    name="name"
                    value={form.name}
                    error={errors.name}
                    onChange={this.onChangeForm}
                    width={300}
                    maxLength={100}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.address + ' 1 *'} />
                <Column.Value>
                  <Textarea
                    width={300}
                    name="address1"
                    value={form.address1}
                    maxLength={200}
                    onChange={this.onChangeForm}
                    error={errors.address1}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.address + ' 2'} />
                <Column.Value>
                  <Textarea
                    width={300}
                    name="address2"
                    value={form.address2}
                    maxLength={200}
                    onChange={this.onChangeForm}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.label.phoneNumber} *`} />
                <Column.Value>
                  <Input.Number
                    width={300}
                    name="phoneNo"
                    value={form.phoneNo}
                    error={errors.phoneNo}
                    onChange={this.onChangeForm}
                    maxLength={13}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.menu.country.title} *`} />
                <Column.Value>
                  <SelectSearch
                    name="countryId"
                    width={getAllCountryLoading ? 270 : 300}
                    value={form.countryId}
                    error={errors.countryId}
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(countries, 'id', 'name')}
                    disabled={getAllCountryLoading}
                    isClearable={false}
                    isLoading={getAllCountryLoading}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.menu.province.title} *`} />
                <Column.Value>
                  <SelectSearch
                    name="provinceId"
                    width={getAllProvinceLoading ? 270 : 300}
                    value={form.provinceId}
                    error={errors.provinceId}
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(provinces, 'id', 'name')}
                    disabled={getAllProvinceLoading || !form.countryId}
                    isClearable={false}
                    isLoading={getAllProvinceLoading}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.menu.city.title} *`} />
                <Column.Value>
                  <SelectSearch
                    name="cityId"
                    width={getAllCityLoading ? 270 : 300}
                    value={form.cityId}
                    error={errors.cityId}
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(cities, 'id', 'name')}
                    disabled={getAllCityLoading || !form.provinceId}
                    isClearable={false}
                    isLoading={getAllCityLoading}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.menu.district.title} *`} />
                <Column.Value>
                  <SelectSearch
                    name="districtId"
                    width={getAllDistrictLoading ? 270 : 300}
                    value={form.districtId}
                    error={errors.districtId}
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(districts, 'id', 'value')}
                    disabled={getAllDistrictLoading || !form.cityId}
                    isClearable={false}
                    isLoading={getAllDistrictLoading}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={`${messages.menu.subDistrict.title} *`} />
                <Column.Value>
                  <SelectSearch
                    name="subDistrictId"
                    width={getAllSubdistrictLoading ? 270 : 300}
                    value={form.subDistrictId}
                    error={errors.subDistrictId}
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(subDistricts, 'id', 'name')}
                    disabled={getAllSubdistrictLoading || !form.districtId}
                    isClearable={false}
                    isLoading={getAllSubdistrictLoading}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.lat} />
                <Column.Value>
                  <Input.Geolocation
                    width={300}
                    name="lat"
                    value={form.lat}
                    error={errors.lat}
                    onChange={this.onChangeForm}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.lng} />
                <Column.Value>
                  <Input.Geolocation
                    width={300}
                    name="longt"
                    value={form.longt}
                    error={errors.longt}
                    onChange={this.onChangeForm}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
            </Column>
            <div className="footer-form">
              <Button
                role="submit"
                size="small"
                loading={addDataLoading || updateDataLoading}
                disabled={isEdit && isDisableUpdate}
              >
                {isEdit ? messages.button.update : messages.button.add}
              </Button>
              &nbsp;
              <Button
                size="small"
                onClick={() =>
                  isEdit
                    ? history.push(`/maintenance/branch/detail?id=${form.id}`, {
                      dataDetail: state ? state.dataDetail : '',
                      filter: state ? state.filter : {},
                    })
                    : history.push('/maintenance/branch')
                }
              >
                {messages.button.cancel}
              </Button>
            </div>
          </form>
        </Container>
      </HomeBase>
    );
  }
}

AddBranch.propTypes = {
  addData: PropTypes.func,
  updateData: PropTypes.func,
  getAllCountry: PropTypes.func,
  getAllProvince: PropTypes.func,
  getAllCity: PropTypes.func,
  getAllDistrict: PropTypes.func,
  getAllSubdistrict: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  detailData: branchSelector('detailData'),
  addDataLoading: branchSelector('addDataLoading'),
  updateDataLoading: branchSelector('updateDataLoading'),
  getAllCountrySuccess: droplistSelector('getAllCountrySuccess'),
  getAllCountryLoading: droplistSelector('getAllCountryLoading'),
  countries: droplistSelector('countryList'),
  getAllProvinceSuccess: droplistSelector('getAllProvinceSuccess'),
  getAllProvinceLoading: droplistSelector('getAllProvinceLoading'),
  provinces: droplistSelector('provinceList'),
  getAllCitySuccess: droplistSelector('getAllCitySuccess'),
  getAllCityLoading: droplistSelector('getAllCityLoading'),
  cities: droplistSelector('cityList'),
  getAllDistrictSuccess: droplistSelector('getAllDistrictSuccess'),
  getAllDistrictLoading: droplistSelector('getAllDistrictLoading'),
  districts: droplistSelector('districtList'),
  getAllSubdistrictSuccess: droplistSelector('getAllSubdistrictSuccess'),
  getAllSubdistrictLoading: droplistSelector('getAllSubdistrictLoading'),
  subDistricts: droplistSelector('subdistrictList'),
});

const mapDispatchToProps = (dispatch) => ({
  addData: (form) => dispatch(addData(form)),
  updateData: (form) => dispatch(updateData(form)),
  getAllCountry: () => dispatch(getAllCountry()),
  getAllProvince: (countryId) => dispatch(getAllProvince(countryId)),
  getAllCity: (provinceId) => dispatch(getAllCity(provinceId)),
  getAllDistrict: (cityId) => dispatch(getAllDistrict(cityId)),
  getAllSubdistrict: (districtId) => dispatch(getAllSubdistrict(districtId)),
});

export default compose(
  withReducerBranch,
  withMiddlewareBranch,
  withReducerDroplist,
  withMiddlewareDroplist,
  connect(mapStateToProps, mapDispatchToProps)
)(AddBranch);
