import history from '@/routes/history';
import {reportService} from '@/services';
import download from 'downloadjs';
import request from '@/helpers/request';
import language from '@/helpers/messages';
import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED,
  DOWNLOAD_FILE, DOWNLOAD_FILE_SUCCESS, DOWNLOAD_FILE_FAILED,
  EXPORT_DATA, EXPORT_DATA_SUCCESS, EXPORT_DATA_FAILED,
  GET_LIST_FILTER_SUCCESS, GET_LIST_FILTER_FAILED,
} from './constants';
const messages = language.getLanguage();

export default {
  onGetList: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_LIST){
      const payload = action.payload;
      reportService.list({page: payload.page, ...payload.filter}).then(
        resp => {
          dispatch({
            type: payload.status == 'filter' ? GET_LIST_FILTER_SUCCESS : GET_LIST_SUCCESS,
            data: resp
          });
          if(payload.status == 'page'){
            history.push(`/report/report_status?page=${payload.page}`);
          }else if(payload.status == 'filter'){
            history.replace('/report/report_status', payload.state);
          }
        },
        error => {
          dispatch({type: payload.status == 'filter' ? GET_LIST_FILTER_FAILED : GET_LIST_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onDownloadFile: ({dispatch}) => next => action => {
    next(action);
    if(action.type === DOWNLOAD_FILE){
      const data = action.payload;
      reportService.downloadFile(data).then(
        resp => {
          download(resp, data.fileName);
          dispatch({type: DOWNLOAD_FILE_SUCCESS});
        },
        error => {
          dispatch({type: DOWNLOAD_FILE_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onExportData: ({dispatch}) => next => action => {
    next(action);
    if(action.type === EXPORT_DATA){
      reportService.exportData(action.payload).then(
        () => {
          dispatch({type: EXPORT_DATA_SUCCESS});
          history.push('/report/report_status', {
            alert: true,
            message: messages.success.export
          });
        },
        error => {
          dispatch({type: EXPORT_DATA_FAILED});
          request.handleError(error);
        }
      );
    }
  }
};
