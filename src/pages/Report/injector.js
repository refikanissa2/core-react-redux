import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducer = injectReducer({ key: 'report', reducer });
export const withMiddleware = injectMiddleware({ key: 'report', middleware });
