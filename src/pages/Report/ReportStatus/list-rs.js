import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withReducer, withMiddleware } from '../injector';
import { getList, downloadFile } from '../actions';
import { reportSelector } from '../selectors';

import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Button, DatePicker, Card, Message, Table, Pagination,
  UrlQueryFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class ListReportStatus extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: [],
      filter: {
        dateCreated: null,
        userId: this.props.userId
      },
      errors: {
        dateCreated: ''
      },
      pagination: {
        totalPages: 0,
        number: 0,
        totalElements: 0,
        first: false,
        last: false
      },
      loadingPage: false,
      isSearching: false,
      isDownloading: []
    };
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
  }

  componentDidMount() {
    this.setState({loadingPage: true});
    const {filter} = this.state;
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    let page = 1;
    if(pageOnUrl){
      page = parseInt(pageOnUrl);
    }

    var newFilter = {
      dateCreated: filter.dateCreated,
      userId: filter.userId
    };
    if(newFilter.dateCreated == null || newFilter.dateCreated == ''){
      newFilter.dateCreated = '';
    }else{
      newFilter.dateCreated = moment(newFilter.dateCreated).format('DD/MM/YYYY');
    }

    var optionData = {
      page: page,
      status: 'list',
      filter: newFilter
    };
    this.setState({filter: filter});
    this.props.getList(Object.assign(optionData));
  }

  componentWillReceiveProps(nextProps) {
    const {pagination, isDownloading} = this.state;
    const {
      getListSuccess, getListFailed, data,
      getListFilterSuccess, getListFilterFailed,
      downloadFileSuccess, downloadFileFailed
    } = nextProps;
    var content = [];
    var arrDownloading = [];
    if((this.props.getListSuccess !== getListSuccess) && getListSuccess){
      data.content && data.content.map(item => {
        var data = {
          ...item,
          isDownload: true
        };
        content.push(data);
        arrDownloading.push(false);
      });
      this.setState({
        content: content,
        isDownloading: arrDownloading,
        pagination: this.setPagination(data, pagination),
        loadingPage: false
      });
    }
    if(getListFailed){
      this.setState({loadingPage: false});
    }
    if(getListFilterSuccess){
      data.content && data.content.map(item => {
        var data = {
          ...item,
          isDownload: true
        };
        content.push(data);
        arrDownloading.push(false);
      });
      this.setState({
        content: content,
        isDownloading: arrDownloading,
        isSearching: false,
        pagination: this.setPagination(data, pagination)
      });
    }
    if(getListFilterFailed){
      this.setState({isSearching: false});
    }
    if(downloadFileSuccess || downloadFileFailed){
      var index = isDownloading.indexOf(true);
      isDownloading[index] = false;
      this.setState({isDownloading: isDownloading});
    }
  }

  setPagination(data, pagination){
    pagination['totalPages'] = data.totalPages;
    pagination['number'] = data.pageNumber;
    pagination['totalElements'] = data.totalElements;
    pagination['first'] = data.first ? data.first : false;
    pagination['last'] = data.last ? data.last : false;

    return pagination;
  }

  setPages(page){
    const {filter} = this.state;
    var newFilter = {
      dateCreated: filter.dateCreated,
      userId: filter.userId
    };
    if(newFilter.dateCreated == null || newFilter.dateCreated == ''){
      newFilter.dateCreated = '';
    }else{
      newFilter.dateCreated = moment(newFilter.dateCreated).format('DD/MM/YYYY');
    }
    var optionData = {
      page: page+1,
      filter: newFilter,
      status: 'page'
    };
    this.props.getList(Object.assign(optionData));
  }

  onChangeFilter(data){
    const {filter, errors} = this.state;
    const {name, value, isValid, rawValue} = data;
    if(isValid){
      filter[name] = value;
      errors[name] = '';
    }else{
      if(rawValue == ''){
        errors[name] = '';
      }else{
        errors[name] = 'Invalid Date';
      }
    }
    this.setState({
      filter: filter,
      errors: errors
    });
  }

  onSubmitFilter(e){
    e.preventDefault();
    const {state} = this.props.location;
    this.setState({isSearching: true});
    const {filter} = this.state;
    var newFilter = {
      dateCreated: filter.dateCreated,
      userId: filter.userId
    };
    if(newFilter.dateCreated == null || newFilter.dateCreated == ''){
      newFilter.dateCreated = '';
    }else{
      newFilter.dateCreated = moment(newFilter.dateCreated).format('DD/MM/YYYY');
    }
    var optionData = {
      filter: newFilter,
      state: state,
      status: 'filter'
    };
    this.props.getList(Object.assign(optionData));
  }

  onDownload(index, fileName, userId){
    const {isDownloading} = this.state;
    isDownloading[index] = true;
    this.setState({isDownloading: isDownloading});
    var data = {
      fileName: fileName,
      userId: userId
    };
    this.props.downloadFile(data);
  }

  render() {
    const {loadingPage, content, pagination, filter, errors, isDownloading, isSearching} = this.state;
    const {location} = this.props;
    const {state, title} = location;
    return (
      <HomeBase loader={loadingPage} breadcrumb={location} title={messages.menu.reportStatus.title} headTitle={title}>
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <div className="columns">
            <div className="column is-9">
              <Table fullwidth>
                <Table.Head>
                  <Table.Label label={messages.label.date} />
                  <Table.Label label={messages.label.type} />
                  <Table.Label label={messages.label.fileName}/>
                  <Table.Label label={messages.label.status}/>
                  <Table.Label label={messages.label.action}/>
                </Table.Head>
                <Table.Body>
                  {content.length > 0 ? (
                    content.map((data, index) =>
                      <Table.Values key={index}>
                        <Table.Value>
                          <p style={{minWidth: 160}}>{moment(data.dateCreated).format('DD/MM/YYYY, hh:mm:ss A')}</p>
                        </Table.Value>
                        <Table.Value value={data.fileType} />
                        <Table.Value value={data.fileName} />
                        <Table.Value value={data.status} />
                        <Table.Value align="center">
                          {data.status.toUpperCase() == 'FINISHED' &&
                            <div>
                              <Button
                                size="small"
                                loading={isDownloading[index]}
                                onClick={() => this.onDownload(index, data.fileName, data.userId)}
                              ><span>{messages.button.download}</span></Button>
                            </div>
                          }
                        </Table.Value>
                      </Table.Values>
                    )
                  ):(
                    <Table.Values>
                      <Table.Value
                        value={messages.text.empty}
                        colspan="5"
                        bold
                      />
                    </Table.Values>
                  )}
                </Table.Body>
              </Table>
              {pagination.totalPages > 1 &&
                <Pagination
                  setPages = {(page) => this.setPages(page)}
                  {...pagination}
                  language={messages}
                />
              }
            </div>
            <div className="column is-one-quarter is-hidden-mobile">
              <Card>
                <Card.Header title={messages.label.filter} />
                <Card.Content>
                  <div className="content">
                    <form onSubmit={this.onSubmitFilter}>
                      <div className="field">
                        <label><h6>{messages.label.date}</h6></label>
                        <DatePicker
                          customInput={<Button.Datepicker error={errors.dateCreated} width={225} />}
                          name="dateCreated"
                          value={filter.dateCreated}
                          error={errors.dateCreated}
                          onChange={this.onChangeFilter}
                        />
                      </div>
                      <div className="columns">
                        <div className="column is-6">
                          <div className="field">
                            <p className="control">
                              <Button
                                size="small"
                                role="submit"
                                loading={isSearching}
                                block
                              >{messages.button.search}</Button>
                            </p>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </Card.Content>
              </Card>
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ListReportStatus.propTypes = {
  getList: PropTypes.func,
  downloadFile: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: reportSelector('data'),
  userId: reportSelector('userId'),
  getListSuccess: reportSelector('getListSuccess'),
  getListFailed: reportSelector('getListFailed'),
  getListFilterSuccess: reportSelector('getListFilterSuccess'),
  getListFilterFailed: reportSelector('getListFilterFailed'),
  downloadFileSuccess: reportSelector('downloadFileSuccess'),
  downloadFileFailed: reportSelector('downloadFileFailed')
});

const mapDispatchToProps = (dispatch) => ({
  getList: (data) => dispatch(getList(data)),
  downloadFile: (data) => dispatch(downloadFile(data))
});

export default compose(
  withReducer,
  withMiddleware,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ListReportStatus);
