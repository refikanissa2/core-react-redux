import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withReducer, withMiddleware } from '../injector';
import { exportData } from '../actions';
import { reportSelector } from '../selectors';
import moment from 'moment';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Button, Column, DatePicker,
  ValidationFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class FormCustomerRegistered extends Component {
  constructor(props){
    super(props);
    this.state={
      form:{
        code: 'RegisteredCustomer',
        'parameter':{
          FromDate: null,
          ToDate: null
        },
        userId: this.props.userId
      },
      errors:{
        FromDate: '',
        ToDate: '',
      },
      loadingPage: false,
      isExporting: false
    };
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onExport = this.onExport.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {exportDataFailed} = nextProps;
    if(exportDataFailed){
      this.setState({isExporting: false});
      history.replace('/report/registered_customer');
    }
  }

  onChangeForm(data){
    let {form, errors} = this.state;
    const {parameter} = form;
    const {name, value, isValid, rawValue} = data;
    if(isValid){
      if(name == 'FromDate'){
        if(value >= parameter.ToDate){
          parameter.ToDate = null;
        }
      }
      parameter[name] = value;
      errors[name] = '';
    }else{
      if(rawValue == ''){
        errors[name] = '';
      }else{
        errors[name] = 'Invalid Date';
      }
    }
    this.setState({
      form: form,
      errors: errors
    });
  }

  onExport(e){
    e.preventDefault();
    this.setState({isExporting: true});
    const {form, errors} = this.state;
    var newForm = {
      code: form.code,
      'parameter':{
        FromDate: form.parameter.FromDate,
        ToDate: form.parameter.ToDate
      },
      userId: form.userId
    };
    const {parameter} = newForm;
    if(ValidationFunc.form(errors, parameter)){
      parameter.FromDate = moment(parameter.FromDate).format('DD/MM/YYYY');
      parameter.ToDate = moment(parameter.ToDate).format('DD/MM/YYYY');
      this.props.exportData(newForm);
      return;
    }
    this.setState({errors:ValidationFunc.messages(errors, parameter), isExporting: false});
  }

  render() {
    const {loadingPage, isExporting, form, errors} = this.state;
    const {location} = this.props;
    const {title} = location;
    return (
      <HomeBase loader={loadingPage} breadcrumb={location} title={messages.menu.customerRegistered.title} headTitle={title}>
        <Container fluid>
          <form onSubmit={this.onExport}>
            <Column>
              <Column.Content>
                <Column.Name text={messages.label.fromDate+' *'}/>
                <Column.Value>
                  <DatePicker
                    customInput={<Button.Datepicker error={errors.FromDate} width={300} />}
                    name="FromDate"
                    value={form.parameter.FromDate}
                    onChange={this.onChangeForm}
                    error={errors.FromDate}
                    width={300}
                    maxDate={moment()}
                    minDate={moment().subtract(90, 'days')}
                    showDisabledMonthNavigation
                    showMonthDropdown={false}
                    showYearDropdown={false}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.toDate+' *'}/>
                <Column.Value>
                  <DatePicker
                    customInput={<Button.Datepicker error={errors.ToDate} width={300} />}
                    name="ToDate"
                    value={form.parameter.ToDate}
                    onChange={this.onChangeForm}
                    error={errors.ToDate}
                    width={300}
                    minDate={form.parameter.FromDate == null ? moment().subtract(90, 'days') : moment(form.parameter.FromDate)}
                    maxDate={moment()}
                    showDisabledMonthNavigation
                    showMonthDropdown={false}
                    showYearDropdown={false}
                  />
                </Column.Value>
              </Column.Content>
            </Column>
            <div className="footer-form">
              <Button
                role="submit"
                size="small"
                loading={isExporting}
              >{messages.button.export}</Button>
            </div>
          </form>
        </Container>
      </HomeBase>
    );
  }
}

FormCustomerRegistered.propTypes = {
  exportData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  userId: reportSelector('userId'),
  exportDataFailed: reportSelector('exportDataFailed')
});

const mapDispatchToProps = dispatch => ({
  exportData: (data) => dispatch(exportData(data))
});

export default compose(
  withReducer,
  withMiddleware,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(FormCustomerRegistered);
