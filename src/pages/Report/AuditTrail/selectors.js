import { createSelector } from 'reselect';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

import { initialState } from './reducer';

const selectAuditTrail = state => state.get('auditTrail', initialState);

const auditTrailSelector = item =>
  createSelector(
    selectAuditTrail,
    substate =>
      isUndefined(item) || isNull(item) || item === ''
        ? substate.toJS()
        : substate.get(item)
  );

export { selectAuditTrail, auditTrailSelector };
