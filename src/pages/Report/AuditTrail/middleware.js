import history from '@/routes/history';
import {auditTrailService} from '@/services';
import request from '@/helpers/request';
import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED,
  GET_LIST_FILTER_SUCCESS, GET_LIST_FILTER_FAILED,
  GET_DETAIL, GET_DETAIL_SUCCESS, GET_DETAIL_FAILED,
  GET_DOMAIN_LIST, GET_DOMAIN_LIST_SUCCESS, GET_DOMAIN_LIST_FAILED,
} from './constants';

export default {
  onGetList: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_LIST){
      const payload = action.payload;
      auditTrailService.list({page: payload.page, ...payload.filter}).then(
        resp => {
          dispatch({
            type: payload.status == 'filter' ? GET_LIST_FILTER_SUCCESS : GET_LIST_SUCCESS,
            data: resp
          });
          if(payload.status == 'page'){
            history.push(`/report/audit_trail?page=${payload.page}`);
          }else if(payload.status == 'filter'){
            history.replace('/report/audit_trail', payload.state);
          }
        },
        error => {
          dispatch({type: payload.status == 'filter' ? GET_LIST_FILTER_FAILED : GET_LIST_FAILED});
          request.handleError(error);
        }
      );
    }
  },
  onGetDetail: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_DETAIL){
      const data = action.payload;
      auditTrailService.detail(data.paramsDetail).then(
        resp => {
          dispatch({
            type: GET_DETAIL_SUCCESS,
            detailData: resp
          });
        },
        error => {
          dispatch({type: GET_DETAIL_FAILED});
          request.handleError(error, {path: 'report/audit_trail', dataDetail: data.dataDetail});
        }
      );
    }
  },
  onGetDomainList: ({dispatch}) => next => action => {
    next(action);
    if(action.type === GET_DOMAIN_LIST){
      auditTrailService.domainList().then(
        resp => {
          dispatch({
            type: GET_DOMAIN_LIST_SUCCESS,
            allData: resp
          });
        },
        error => {
          dispatch({type: GET_DOMAIN_LIST_FAILED});
          request.handleError(error);
        }
      );
    }
  }
};
