import injectReducer from '@/utils/injectReducer';
import injectMiddleware from '@/utils/injectMiddleware';

import reducer from './reducer';
import middleware from './middleware';

export const withReducer = injectReducer({ key: 'auditTrail', reducer });
export const withMiddleware = injectMiddleware({ key: 'auditTrail', middleware });
