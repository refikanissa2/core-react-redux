import { fromJS } from 'immutable';

import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED, GET_LIST_FILTER_SUCCESS, GET_LIST_FILTER_FAILED,
  GET_DETAIL, GET_DETAIL_SUCCESS, GET_DETAIL_FAILED,
  GET_DOMAIN_LIST, GET_DOMAIN_LIST_SUCCESS, GET_DOMAIN_LIST_FAILED
} from './constants';

export const initialState = fromJS({
  data:{},
  detailData: {},
  domainList: []
});

function reportReducer(state = initialState, action){
  switch(action.type){
  case GET_LIST:
    return state
      .set('getListSuccess', false)
      .set('getListFailed', false)
      .set('getListFilterSuccess', false)
      .set('getListFilterFailed', false);
  case GET_LIST_SUCCESS:
    return state
      .set('getListSuccess', true)
      .set('data', action.data);
  case GET_LIST_FAILED:
    return state
      .set('getListFailed', true);
  case GET_LIST_FILTER_SUCCESS:
    return state
      .set('getListFilterSuccess', true)
      .set('data', action.data);
  case GET_LIST_FILTER_FAILED:
    return state
      .set('getListFilterFailed', true);

  case GET_DETAIL:
    return state
      .set('getDetailSuccess', false)
      .set('getDetailFailed', false);
  case GET_DETAIL_SUCCESS:
    return state
      .set('getDetailSuccess', true)
      .set('detailData', action.detailData);
  case GET_DETAIL_FAILED:
    return state
      .set('getDetailFailed', true);

  case GET_DOMAIN_LIST:
    return state
      .set('getDomainListSuccess', false)
      .set('getDomainListFailed', false);
  case GET_DOMAIN_LIST_SUCCESS:
    return state
      .set('getDomainListSuccess', true)
      .set('domainList', action.allData);
  case GET_DOMAIN_LIST_FAILED:
    return state
      .set('getDomainListFailed', true);

  default:
    return state;
  }
}

export default reportReducer;
