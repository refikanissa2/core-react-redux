import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withReducer, withMiddleware } from '../injector';
import { getList, getDomainList } from '../actions';
import { auditTrailSelector } from '../selectors';
import moment from 'moment';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Message, Table, Button, Pagination, Card, Input, DatePicker, Select, Alert,
  UrlQueryFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class ListAuditTrail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter:{
        objectName: '',
        dateFrom: null,
        dateTo: null,
        value: ''
      },
      pagination: {
        totalPages: 0,
        number: 0,
        totalElements: 0,
        first: false,
        last: false
      },
      errors:{
        dateFrom: '',
        dateTo: '',
        objectName: ''
      },
      loadingPage: false,
      isSearching: false
    };
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
  }

  componentDidMount() {
    this.setState({loadingPage: true});
    this.props.getDomainList();
  }

  componentWillReceiveProps(nextProps) {
    const {
      getDomainListSuccess, getDomainListFailed,
      getListSuccess, getListFailed, data,
      getListFilterSuccess, getListFilterFailed
    } = nextProps;
    let {filter, pagination} = this.state;
    if((this.props.getDomainListSuccess !== getDomainListSuccess) && getDomainListSuccess){
      const {state} = this.props.location;
      if(state){
        if(state.filter){
          const pageOnUrl = UrlQueryFunc.getQueryString('page');
          let page = 1;
          if(pageOnUrl){
            page = parseInt(pageOnUrl);
          }
          var optionData = {
            page: page,
            status: 'list',
            filter: state.filter
          };
          this.props.getList(Object.assign(optionData));
          filter = {
            objectName: state.filter.objectName,
            dateFrom: state.filter.dateFrom == null ? null : moment(state.filter.dateFrom),
            dateTo: state.filter.dateTo == null ? null : moment(state.filter.dateTo),
            value: state.filter.value
          };
          this.setState({ filter: filter });
        }else{
          this.setState({ loadingPage: false});
        }
      }else{
        this.setState({ loadingPage: false });
      }
    }

    if((this.props.getListSuccess !== getListSuccess) && getListSuccess){
      if(data.error){
        if(data.error == true){
          Alert.error({message: data.errorMessage});
          this.setState({
            pagination: {
              totalPages: 0,
              number: 0,
              totalElements: 0,
              first: false,
              last: false
            },
            loadingPage: false
          });
          return;
        }
        return;
      }
      this.setState({
        pagination: this.setPagination(data, pagination),
        loadingPage: false
      });
    }
    if(getListFailed || getDomainListFailed){
      this.setState({ loadingPage: false });
    }
    if((this.props.getListFilterSuccess !== getListFilterSuccess) && getListFilterSuccess){
      if(data.error){
        if(data.error == true){
          Alert.error({message: data.errorMessage});
          this.setState({
            pagination: {
              totalPages: 0,
              number: 0,
              totalElements: 0,
              first: false,
              last: false
            },
            isSearching: false
          });
          return;
        }
        return;
      }
      this.setState({
        pagination: this.setPagination(data, pagination),
        isSearching: false
      });
    }
    if(getListFilterFailed){
      this.setState({ isSearching: false });
    }
  }

  setPagination(data, pagination){
    pagination['totalPages'] = data.totalPages;
    pagination['number'] = data.pageNumber-1;
    pagination['totalElements'] = data.totalElements;
    pagination['first'] = data.firstPage ? data.firstPage : false;
    pagination['last'] = data.lastPage ? data.lastPage : false;

    return pagination;
  }

  setPages(page) {
    const {filter} = this.state;
    var newFilter = {
      objectName: filter.objectName,
      dateFrom: filter.dateFrom == null || '' ? '' : moment(filter.dateFrom).format('YYYY-MM-DD HH:mm:ss'),
      dateTo: filter.dateTo == null || '' ? '' : moment(filter.dateTo).format('YYYY-MM-DD HH:mm:ss'),
      value: filter.value
    };
    var optionData = {
      page: page+1,
      filter: newFilter,
      status: 'page'
    };
    this.props.getList(Object.assign(optionData));
  }

  onChangeFilter(data){
    const {filter, errors} = this.state;
    const {value, name, isValid, rawValue} = data;
    if(name == 'dateFrom' || name == 'dateTo'){
      if(isValid){
        if(name == 'dateFrom'){
          if(value >= filter.dateTo){
            filter.dateTo = null;
            errors.dateTo = '';
          }
        }
        filter[name] = value;
        errors[name] = '';
      }else{
        if(rawValue == ''){
          errors[name] = '';
        }else{
          errors[name] = 'Invalid Date';
        }
      }
    }else{
      filter[name] = value;
      if(errors[name]){
        errors[name] = '';
      }
    }
    this.setState({
      filter: filter,
      errors: errors
    });
  }

  onSubmitFilter(e){
    e.preventDefault();
    this.setState({isSearching: true});
    const {filter, errors} = this.state;
    const {state} = this.props.location;
    var newFilter = {
      objectName: filter.objectName,
      dateFrom: filter.dateFrom == null || '' ? '' : moment(filter.dateFrom).format('YYYY-MM-DD HH:mm:ss'),
      dateTo: filter.dateTo == null || '' ? '' : moment(filter.dateTo).format('YYYY-MM-DD HH:mm:ss'),
      value: filter.value
    };

    if(newFilter.dateFrom == '' && newFilter.dateTo != ''){
      errors.dateFrom = messages.text.required;
      if(newFilter.objectName == ''){
        errors.objectName = messages.text.required;
      }
      this.setState({errors: errors, isSearching: false});
      return;
    }
    if(newFilter.dateTo == '' && newFilter.dateFrom != ''){
      errors.dateTo = messages.text.required;
      if(newFilter.objectName == ''){
        errors.objectName = messages.text.required;
      }
      this.setState({errors: errors, isSearching: false});
      return;
    }
    if(newFilter.objectName != ''){
      var optionData = {
        filter: newFilter,
        state: state,
        status: 'filter'
      };
      this.props.getList(Object.assign(optionData));
    }
  }

  render() {
    const {loadingPage, isSearching, filter, errors, pagination} = this.state;
    const {location, data, domainList} = this.props;
    const {state, title} = location;
    var content = [];
    if(data.content) content = data.content;
    return (
      <HomeBase loader={loadingPage} breadcrumb={location} title={messages.menu.auditTrail.list} headTitle={title}>
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <div className="columns">
            <div className="column is-9">
              <Table fullwidth>
                <Table.Head>
                  <Table.Label label={messages.label.object}/>
                  <Table.Label label={messages.label.value}/>
                  <Table.Label label={messages.label.type}/>
                  <Table.Label label={messages.label.modifiedBy}/>
                  <Table.Label label={messages.label.date}/>
                </Table.Head>
                <Table.Body>
                  {content.length > 0 ? (
                    content.map((item, index) =>
                      <Table.Values key={index}
                        onClick={() => history.push('/report/audit_trail/detail', {
                          paramsDetail: {
                            objectName: item.canonicalName,
                            revNumber: item.revisionNumber,
                            primaryKeyEntity: item.primaryKeyValue,
                            type: item.revisionType
                          },
                          filter: {
                            objectName: filter.objectName,
                            dateFrom: filter.dateFrom == null ? null : moment(filter.dateFrom).format('YYYY-MM-DD HH:mm:ss'),
                            dateTo: filter.dateTo == null ? null : moment(filter.dateTo).format('YYYY-MM-DD HH:mm:ss'),
                            value: filter.value
                          },
                          page: pagination.number+1,
                          dataDetail: `${item.objectName} - ${item.value}`
                        })}
                      >
                        <Table.Value align="center" value={item.objectName} />
                        <Table.Value align="center" value={item.value} />
                        <Table.Value align="center" value={item.revisionType} />
                        <Table.Value align="center" value={item.modifiedBy} />
                        <Table.Value align="center" value={item.dateModified} />
                      </Table.Values>
                    )
                  ):(
                    <Table.Values>
                      <Table.Value
                        value={messages.text.empty}
                        colspan="5"
                        bold
                      />
                    </Table.Values>
                  )}
                </Table.Body>
              </Table>
              {pagination.totalPages > 1 &&
                <Pagination
                  setPages = {(page) => this.setPages(page)}
                  {...pagination}
                  language={messages}
                />
              }
            </div>
            <div className="column is-one-quarter is-hidden-mobile">
              <Card>
                <Card.Header title={messages.label.filter} />
                <Card.Content>
                  <div className="content">
                    <form onSubmit={this.onSubmitFilter}>
                      <div className="field">
                        <label><h6>{messages.label.object}</h6></label>
                        <Select
                          name="objectName"
                          value={filter.objectName}
                          onChange={this.onChangeFilter}
                          error={errors.objectName}
                          width={220}
                          language={messages}
                        >
                          {domainList.map((item, index) =>
                            <Select.Option key={index} value={item.code}>{item.name}</Select.Option>
                          )}
                        </Select>
                      </div>
                      <div className="field">
                        <label><h6>{messages.label.fromDate}</h6></label>
                        <DatePicker
                          customInput={<Button.Datepicker error={errors.dateFrom} width={220} />}
                          name="dateFrom"
                          value={filter.dateFrom}
                          onChange={this.onChangeFilter}
                          error={errors.dateFrom}
                          dateFormat="DD/MM/YYYY HH:mm"
                          showTimeSelect
                          maxDate={moment()}
                          minDate={moment().subtract(90, 'days')}
                          showDisabledMonthNavigation
                          showMonthDropdown={false}
                          showYearDropdown={false}
                        />
                      </div>
                      <div className="field">
                        <label><h6>{messages.label.toDate}</h6></label>
                        <DatePicker
                          customInput={<Button.Datepicker error={errors.dateTo} width={220} />}
                          name="dateTo"
                          value={filter.dateTo}
                          onChange={this.onChangeFilter}
                          error={errors.dateTo}
                          minDate={filter.dateFrom ? moment(filter.dateFrom) : null}
                          dateFormat="DD/MM/YYYY HH:mm"
                          showTimeSelect
                          minDate={filter.dateFrom == null ? moment().subtract(90, 'days') : moment(filter.dateFrom)}
                          maxDate={moment()}
                          showDisabledMonthNavigation
                          showMonthDropdown={false}
                          showYearDropdown={false}
                        />
                      </div>
                      <div className="field">
                        <label><h6>{messages.label.value}</h6></label>
                        <Input
                          name="value"
                          value={filter.value}
                          onChange={this.onChangeFilter}
                          size="small"
                          language={messages}
                        />
                      </div>
                      <div className="columns">
                        <div className="column is-6">
                          <div className="field">
                            <p className="control">
                              <Button
                                size="small"
                                role="submit"
                                loading={isSearching}
                                block
                              >{messages.button.search}</Button>
                            </p>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </Card.Content>
              </Card>
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ListAuditTrail.propTypes = {
  getList: PropTypes.func,
  getDomainList: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: auditTrailSelector('data'),
  getListSuccess: auditTrailSelector('getListSuccess'),
  getListFailed: auditTrailSelector('getListFailed'),
  getListFilterSuccess: auditTrailSelector('getListFilterSuccess'),
  getListFilterFailed: auditTrailSelector('getListFilterFailed'),
  getDomainListSuccess: auditTrailSelector('getDomainListSuccess'),
  getDomainListFailed: auditTrailSelector('getDomainListFailed'),
  domainList: auditTrailSelector('domainList')
});

const mapDispatchToProps = dispatch => ({
  getList: (data) => dispatch(getList(data)),
  getDomainList: () => dispatch(getDomainList())
});

export default compose(
  withReducer,
  withMiddleware,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ListAuditTrail);
