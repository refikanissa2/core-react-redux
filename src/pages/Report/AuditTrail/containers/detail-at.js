import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withReducer, withMiddleware } from '../injector';
import { getDetail } from '../actions';
import { auditTrailSelector } from '../selectors';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Button, Column, Table, Alert
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class DetailAuditTrail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingPage: false,
    };
  }

  componentDidMount() {
    this.setState({loadingPage: true});
    const {location} = this.props;
    const {state} = location;
    if(state){
      const {paramsDetail, filter, page, dataDetail} = state;
      window.onpopstate = this.onBackButtonBrowser.bind(this, filter, page);
      this.props.getDetail({paramsDetail: paramsDetail, dataDetail: dataDetail});
    }else{
      Alert.error({message: 'Error page detail, please try again'});
      history.push('/report/audit_trail');
    }
  }

  componentWillReceiveProps(nextProps) {
    const {getDetailSuccess, getDetailFailed} = nextProps;
    if(getDetailSuccess || getDetailFailed){
      this.setState({ loadingPage: false });
    }
  }

  onBackButtonBrowser(filter, page){
    history.push('/report/audit_trail?page='+page, {filter: filter});
  }

  render() {
    const {loadingPage} = this.state;
    const {location, data} = this.props;
    const {title, state} = location;
    return (
      <HomeBase loader={loadingPage} breadcrumb={location} title={messages.menu.auditTrail.detail} headTitle={title}>
        <Container fluid>
          <Column>
            <Column.Content>
              <Column.Name text={messages.label.object}/>
              <Column.Value>{data.objectName ? data.objectName : ''}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.value}/>
              <Column.Value>{data.value ? data.value : ''}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.transactionType}/>
              <Column.Value>{data.transactionType ? data.transactionType : ''}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.modifiedBy}/>
              <Column.Value>{data.modifiedBy ? data.modifiedBy : ''}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.date}/>
              <Column.Value>{data.modifiedDate ? data.modifiedDate : ''}</Column.Value>
            </Column.Content>
          </Column>
          <div style={{border: '1px solid rgb(239, 239, 239)', margin: '20px 0', padding: '20px 20px'}}>
            <div style={{maxHeight: 325}}>
              <Table fullwidth>
                <Table.Head style={{display: 'inline-block', width: '100%'}} styleTr={{display: 'inline-table', width: '100%'}}>
                  <Table.Label label={messages.label.propertyName} width='40%' />
                  <Table.Label label={messages.label.oldValue} width='30%' />
                  <Table.Label label={messages.label.newValue} width='30%' />
                </Table.Head>
                <Table.Body style={{display: 'inline-block', maxHeight: 325, overflowY: 'auto', width: '100%'}}>
                  {data.diffMapList && data.diffMapList.map((item, index) =>
                    <Table.Values key={index} style={{width: '100%', display: 'inline-table', cursor: 'pointer'}}>
                      <Table.Value width='40%' value={item.propertyName} />
                      <Table.Value width='30%' value={item.oldPropertyValue == null ? '' : item.oldPropertyValue.toString()} />
                      <Table.Value width='30%' value={item.newPropertyValue == null ? '' : item.newPropertyValue.toString()} />
                    </Table.Values>
                  )}
                </Table.Body>
              </Table>
            </div>
          </div>
          <div className="footer-form">
            <Button
              size="small"
              onClick={() => history.push('/report/audit_trail?page='+state.page, {filter: state.filter})}
            >{messages.button.back}</Button>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

DetailAuditTrail.propTypes = {
  getDetail: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: auditTrailSelector('detailData'),
  getDetailSuccess: auditTrailSelector('getDetailSuccess'),
  getDetailFailed: auditTrailSelector('getDetailFailed'),
});

const mapDispatchToProps = dispatch => ({
  getDetail: (data) => dispatch(getDetail(data))
});

export default compose(
  withReducer,
  withMiddleware,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(DetailAuditTrail);
