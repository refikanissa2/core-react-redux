export const GET_LIST = 'auditTrail/GET_LIST';
export const GET_LIST_SUCCESS = 'auditTrail/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'auditTrail/GET_LIST_FAILED';
export const GET_LIST_FILTER_SUCCESS = 'auditTrail/GET_LIST_FILTER_SUCCESS';
export const GET_LIST_FILTER_FAILED = 'auditTrail/GET_LIST_FILTER_FAILED';

export const GET_DETAIL = 'auditTrail/GET_DETAIL';
export const GET_DETAIL_SUCCESS = 'auditTrail/GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = 'auditTrail/GET_DETAIL_FAILED';

export const GET_DOMAIN_LIST = 'auditTrail/GET_DOMAIN_LIST';
export const GET_DOMAIN_LIST_SUCCESS = 'auditTrail/GET_DOMAIN_LIST_SUCCESS';
export const GET_DOMAIN_LIST_FAILED = 'auditTrail/GET_DOMAIN_LIST_FAILED';
