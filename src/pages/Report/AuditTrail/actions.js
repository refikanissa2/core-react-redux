import { GET_LIST, GET_DETAIL, GET_DOMAIN_LIST} from './constants';

export const getList = (data) => ({
  type: GET_LIST,
  payload: data
});

export const getDetail = (data) => ({
  type: GET_DETAIL,
  payload: data
});

export const getDomainList = () => ({
  type: GET_DOMAIN_LIST
});
