import { fromJS } from 'immutable';
import {AuthenticationFunc} from 'component-ui-web-teravin';

import {
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILED, GET_LIST_FILTER_SUCCESS, GET_LIST_FILTER_FAILED,
  DOWNLOAD_FILE, DOWNLOAD_FILE_SUCCESS, DOWNLOAD_FILE_FAILED,
  EXPORT_DATA, EXPORT_DATA_SUCCESS, EXPORT_DATA_FAILED
} from './constants';

var dataDecrypt = AuthenticationFunc.decryptData('teravin');
var decoded = AuthenticationFunc.getDecodedJwttoken(dataDecrypt.token);

export const initialState = fromJS({
  data:{},
  userId: decoded.user_name
});

function reportReducer(state = initialState, action){
  switch(action.type){
  case GET_LIST:
    return state
      .set('getListSuccess', false)
      .set('getListFailed', false)
      .set('getListFilterSuccess', false)
      .set('getListFilterFailed', false);
  case GET_LIST_SUCCESS:
    return state
      .set('getListSuccess', true)
      .set('data', action.data);
  case GET_LIST_FAILED:
    return state
      .set('getListFailed', true);
  case GET_LIST_FILTER_SUCCESS:
    return state
      .set('getListFilterSuccess', true)
      .set('data', action.data);
  case GET_LIST_FILTER_FAILED:
    return state
      .set('getListFilterFailed', true);

  case DOWNLOAD_FILE:
    return state
      .set('downloadFileSuccess', false)
      .set('downloadFileFailed', false);
  case DOWNLOAD_FILE_SUCCESS:
    return state
      .set('downloadFileSuccess', true);
  case DOWNLOAD_FILE_FAILED:
    return state
      .set('downloadFileFailed', true);

  case EXPORT_DATA:
    return state
      .set('exportDataSuccess', false)
      .set('exportDataFailed', false);
  case EXPORT_DATA_SUCCESS:
    return state
      .set('exportDataSuccess', true);
  case EXPORT_DATA_FAILED:
    return state
      .set('exportDataFailed', true);

  default:
    return state;
  }
}

export default reportReducer;
