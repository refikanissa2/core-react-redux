export const GET_LIST = 'report/GET_LIST';
export const GET_LIST_SUCCESS = 'report/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'report/GET_LIST_FAILED';
export const GET_LIST_FILTER_SUCCESS = 'report/GET_LIST_FILTER_SUCCESS';
export const GET_LIST_FILTER_FAILED = 'report/GET_LIST_FILTER_FAILED';

export const DOWNLOAD_FILE = 'report/DOWNLOAD_FILE';
export const DOWNLOAD_FILE_SUCCESS = 'report/DOWNLOAD_FILE_SUCCESS';
export const DOWNLOAD_FILE_FAILED = 'report/DOWNLOAD_FILE_FAILED';

export const EXPORT_DATA = 'report/EXPORT_DATA';
export const EXPORT_DATA_SUCCESS = 'report/EXPORT_DATA_SUCCESS';
export const EXPORT_DATA_FAILED = 'report/EXPORT_DATA_FAILED';
