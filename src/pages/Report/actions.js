import { GET_LIST, DOWNLOAD_FILE, EXPORT_DATA} from './constants';

export const getList = (data) => ({
  type: GET_LIST,
  payload: data
});

export const downloadFile = (data) => ({
  type: DOWNLOAD_FILE,
  payload: data
});

export const exportData = (data) => ({
  type: EXPORT_DATA,
  payload: data
});
