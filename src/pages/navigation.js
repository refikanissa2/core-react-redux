import React, { Component } from 'react';
import moment from 'moment';
import {Navbar, FlagIcon, AuthenticationFunc, ConvertFunc} from 'component-ui-web-teravin';
import history from '@/routes/history';

class Navigation extends Component {

  constructor(){
    super();
    this.state = {
      date : '',
      name:'Administrator',
      menu: []
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
    var name = '';
    var menu = [];
    var menus = [];
    if(AuthenticationFunc.cekDataStorages('teravin')){
      var dataDecrypt = AuthenticationFunc.decryptData('teravin');
      var decoded = AuthenticationFunc.getDecodedJwttoken(dataDecrypt.token);
      name = decoded.name;
    }
    else{
      name = 'Administrator';
    }
    if(AuthenticationFunc.cekDataStorages('mn')){
      menu = AuthenticationFunc.getSessionStorages('mn');
      menu.map((item) => {
        if(item.level == 0){
          var parentType = 'item';
          var children = [];
          menu.map((childItem) => {
            if(childItem.level == 1 && childItem.parentCode == item.code){
              var newChild = {
                name : childItem.name,
                url : `${ConvertFunc.spasiToSnakeCase(item.name)}/${ConvertFunc.spasiToSnakeCase(childItem.name)}`,
                type: 'item'
              };
              children.push(newChild);
              parentType= 'dropdown';
            }
          });
          children.sort((a,b) => {
            return (a.name > b.name) ? 1 : (
              (b.name > a.name) ? -1 : 0
            );
          });
          var parent = {
            name : item.name,
            url: ConvertFunc.spasiToSnakeCase(item.name),
            type : parentType,
            child : children
          };
          menus.push(parent);
        }
      });
    }

    this.setState({
      name:name,
      menu: menus
    });
  }

  componentWillUnmount(){
    clearInterval(this.interval);
  }

  tick(){
    const date = new Date();
    const today = this.formatDate(date);
    this.setState({
      date:today
    });
  }

  formatDate(date) {
    const dateTime = moment(date).format('MMMM DD, YYYY HH:mm');
    return dateTime ;
  }

  onChangeLang(lang, e){
    e.preventDefault();
    localStorage.setItem('_locale', lang);
    window.location.reload();
  }

  render() {
    const {menu, name} = this.state;
    const lang = localStorage.getItem('_locale');
    return (
      <div>
        <nav className="container is-fluid" style={{height:38}}>
          <Navbar.Account
            styleItem={{height: 38}}
            name={name}
            onLogout={() => {
              sessionStorage.removeItem('teravin');
              sessionStorage.removeItem('mn');
              history.push('/login');
            }}
          >
            <ul className="lang-switcher lang-switcher--pill">
              <li><a href="#" onClick={this.onChangeLang.bind(this,'en')} className={lang === 'en' ? 'is-active' : ''}>
                <FlagIcon code="gb"/>
              </a></li>
              <li><a href="#" onClick={this.onChangeLang.bind(this,'id')} className={lang === 'id' ? 'is-active' : ''}>
                <FlagIcon code="id"/>
              </a></li>
            </ul>
          </Navbar.Account>
        </nav>
        <Navbar container="fluid" logo={require('@/assets/img/logo.png')} showDat>
          <Navbar.Start>
            {menu.map((item, index) => {
              if(item.type == 'dropdown'){
                return(
                  <Navbar.Dropdown isBorder title={item.name} key={index}>
                    {item.child.map((childItem, childIndex) =>
                      <Navbar.Item href={`/${childItem.url}`} key={childIndex}>{childItem.name}</Navbar.Item>
                    )}
                  </Navbar.Dropdown>
                );
              }else if(item.type == 'item'){
                return(
                  <Navbar.Item isBorder href={`/${item.url}`} key={index}>{item.name}</Navbar.Item>
                );
              }
            })}
          </Navbar.Start>
        </Navbar>
      </div>

    );
  }
}

export default Navigation;
