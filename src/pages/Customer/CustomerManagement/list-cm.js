import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import {
  withReducerCustManagement,
  withMiddlewareCustManagement,
} from '@/store/CustomerManagement/injector';
import { getList } from '@/store/CustomerManagement/actions';
import { custManagementSelector } from '@/store/CustomerManagement/selectors';

import {
  withReducerDroplist,
  withMiddlewareDroplist,
} from '@/store/Droplist/injector';
import { getAllBranch } from '@/store/Droplist/actions';
import { droplistSelector } from '@/store/Droplist/selectors';

import history from '@/routes/history';
import language from '@/helpers/messages';
import colors from '@/assets/sass/colors.scss';

import HomeBase from '@/pages/home';
import {
  Container,
  Message,
  Pagination,
  Card,
  Input,
  Button,
  UrlQueryFunc,
  ArrayFunc,
  SelectSearch,
  TableWithSelect,
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class ListCustomerManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSearching: false,
      filter: {
        mobileNo: '',
        identityNo: '',
        name: '',
        cifKey: '',
        branchCode: '',
      },
    };
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
  }

  getDataList(options = { page: false }) {
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    const { page } = options;
    let newPage = 1;
    if (!page && pageOnUrl) {
      newPage = parseInt(pageOnUrl);
    } else {
      newPage = page || newPage;
    }
    this.props.getList(Object.assign(options, { page: newPage }));
  }

  componentWillMount() {
    let filter = {};
    const { state } = this.props.location;
    if (state) {
      if (state.filter) {
        filter = state.filter;
        this.setState({ filter: state.filter });
      }
    }
    this.getDataList({ ...filter });
    this.props.getAllBranch();
  }

  componentWillReceiveProps(props) {
    const isPropChange = (name) =>
      props[name] && props[name] !== this.props[name];
    const { data } = props;

    if (isPropChange('getListSuccess')) {
      this.setState({ isSearching: false });
      if (data.number > 0) {
        history.push(`/customer/customer_management?page=${data.number + 1}`);
      } else {
        history.push('/customer/customer_management');
      }
    }

    if(isPropChange('getListFailed')){
      this.setState({isSearching: false});
    }
  }

  setPages(page) {
    const { filter } = this.state;
    this.getDataList({ page: page + 1, ...filter });
  }

  onChangeFilter(data) {
    const { filter } = this.state;
    const { value, name } = data;
    filter[name] = value;
    this.setState({
      filter: filter,
    });
  }

  onSubmitFilter(e) {
    e.preventDefault();
    const { filter } = this.state;
    this.setState({ isSearching: true });
    this.getDataList({ ...filter, page: 1 });
  }

  render() {
    const { isSearching, filter } = this.state;
    const {
      location,
      data = { content: [] },
      mmsList = [],
      getListLoading,
      getAllBranchLoading,
    } = this.props;
    const { state, title } = location;
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.customerManagement.list}
        headTitle={title}
      >
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <div className="columns">
            <div className="column is-9">
              <TableWithSelect
                fullwidth
                language={messages}
                headers={[
                  {
                    label: messages.label.customerId,
                    value: 'cifKey',
                  },
                  {
                    label: messages.label.noWowCust,
                    value: 'mobileNo',
                  },
                  {
                    label: messages.label.customerName,
                    value: 'name',
                  },
                  {
                    label: messages.label.idNo,
                    value: 'identityNo',
                  },
                  {
                    label: messages.label.mms,
                    customValue: (item) =>
                      item.branchCode
                        ? item.branchCode +
                          (item.branchName ? ' - ' + item.branchName : '')
                        : '',
                  },
                ]}
                data={data.content}
                onSelected={(index) =>
                  history.push(
                    `/customer/customer_management/detail?id=${data.content[index].id}`,
                    {
                      dataDetail: `${data.content[index].code} - ${data.content[index].name}`,
                      filter: filter,
                    }
                  )
                }
                loading={getListLoading || isSearching}
              />
              {data.totalPages > 1 && !getListLoading && (
                <Pagination
                  setPages={(page) => this.setPages(page)}
                  {...data}
                  language={messages}
                />
              )}
            </div>
            <div className="column is-one-quarter is-hidden-mobile">
              <Card>
                <Card.Header title={messages.label.filter} />
                <Card.Content>
                  <div className="content">
                    <form onSubmit={this.onSubmitFilter}>
                      <div className="field">
                        <label>
                          <h6>{messages.label.customerId}</h6>
                        </label>
                        <Input
                          name="cifKey"
                          value={filter.cifKey}
                          onChange={this.onChangeFilter}
                          size="small"
                          language={messages}
                        />
                      </div>
                      <div className="field">
                        <label>
                          <h6>{messages.label.noWowCust}</h6>
                        </label>
                        <Input
                          name="mobileNo"
                          value={filter.mobileNo}
                          onChange={this.onChangeFilter}
                          size="small"
                          maxLength={17}
                          language={messages}
                        />
                      </div>
                      <div className="field">
                        <label>
                          <h6>{messages.label.idNo}</h6>
                        </label>
                        <Input
                          name="identityNo"
                          value={filter.identityNo}
                          onChange={this.onChangeFilter}
                          size="small"
                          maxLength={20}
                          language={messages}
                        />
                      </div>
                      <div className="field">
                        <label>
                          <h6>{messages.label.customerName}</h6>
                        </label>
                        <Input
                          name="name"
                          value={filter.name}
                          onChange={this.onChangeFilter}
                          size="small"
                          maxLength={100}
                          language={messages}
                        />
                      </div>
                      <div className="field">
                        <label>
                          <h6>{messages.label.mms}</h6>
                        </label>
                        <SelectSearch
                          name="branchCode"
                          width={getAllBranchLoading ? 270 : 300}
                          value={filter.branchCode}
                          onChange={this.onChangeFilter}
                          options={ArrayFunc.onArrangeDataSelect(mmsList, 'code', 'value')}
                          disabled={getAllBranchLoading}
                          isLoading={getAllBranchLoading}
                        />
                      </div>
                      <div className="columns">
                        <div className="column is-6">
                          <div className="field">
                            <p className="control">
                              <Button
                                size="small"
                                role="submit"
                                loading={isSearching}
                                block
                              >
                                {messages.button.search}
                              </Button>
                            </p>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </Card.Content>
              </Card>
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ListCustomerManagement.propTypes = {
  getList: PropTypes.func,
  getAllBranch: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  data: custManagementSelector('data'),
  getListSuccess: custManagementSelector('getListSuccess'),
  getListFailed: custManagementSelector('getListFailed'),
  getListLoading: custManagementSelector('getListLoading'),
  getAllBranchLoading: droplistSelector('getAllBranchLoading'),
  mmsList: droplistSelector('branchList'),
});

const mapDispatchToProps = (dispatch) => ({
  getList: (data) => dispatch(getList(data)),
  getAllBranch: () => dispatch(getAllBranch()),
});

export default compose(
  withReducerCustManagement,
  withMiddlewareCustManagement,
  withReducerDroplist,
  withMiddlewareDroplist,
  connect(mapStateToProps, mapDispatchToProps)
)(ListCustomerManagement);
