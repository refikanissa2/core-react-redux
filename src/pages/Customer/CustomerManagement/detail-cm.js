import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import moment from 'moment';

import {
  withReducerCustManagement,
  withMiddlewareCustManagement,
} from '@/store/CustomerManagement/injector';
import { getDetail } from '@/store/CustomerManagement/actions';
import { custManagementSelector } from '@/store/CustomerManagement/selectors';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container,
  Column,
  Image,
  Button,
  UrlQueryFunc,
  ContentLoader,
  LoadIndicator,
  TextCurrency,
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

var BASE_URL = '';
const { DESC_ENV } = process.env;

if (DESC_ENV === 'productionRelative') {
  BASE_URL = window.location.origin;
} else {
  BASE_URL = process.env.API_ENDPOINT;
}

export class DetailCustomerManagement extends Component {
  componentDidMount() {
    const { location, dataList } = this.props;
    const { state } = location;
    if (state) {
      if (state.filter) {
        window.onpopstate = this.onBackButtonBrowser.bind(
          this,
          state.filter,
          dataList ? dataList.number + 1 : 1
        );
      }
    }
    const id = UrlQueryFunc.getQueryString('id');
    this.props.getDetail({ id: id, dataDetail: state ? state.dataDetail : '' });
  }

  onBackButtonBrowser(filter, page) {
    history.push('/customer/customer_management?page=' + page, {
      filter: filter,
    });
  }

  render() {
    const { location, data = {}, dataList = {}, getDetailLoading } = this.props;
    const { title, state } = location;
    return (
      <HomeBase
        breadcrumb={location}
        title={messages.menu.customerManagement.detail}
        headTitle={title}
      >
        <Container fluid>
          {getDetailLoading ? (
            <ContentLoader.Detail />
          ) : (
            <Column>
              <Column.Content>
                <Column.Name text={messages.label.customerId} />
                <Column.Value>{data.cifKey}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.noWowCust} />
                <Column.Value>{data.mobileNo}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.idNo} />
                <Column.Value>{data.identityNo}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.customerName} />
                <Column.Value>{data.name}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.coId} />
                <Column.Value>{data.createdBy}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.coName} />
                <Column.Value>{data.customerField1}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.mms} />
                <Column.Value>
                  {data.branchCode
                    ? data.branchCode +
                      (data.branchName ? ' - ' + data.branchName : '')
                    : ''}
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.createdBy} />
                <Column.Value>{data.createdBy}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.dateCreated} />
                <Column.Value>
                  {data.dateCreated == null
                    ? ''
                    : moment(data.dateCreated).format('DD/MM/YYYY hh:mm:ss A')}
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.updatedBy} />
                <Column.Value>{data.updatedBy}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.lastUpdated} />
                <Column.Value>
                  {data.lastUpdated == null
                    ? ''
                    : moment(data.lastUpdated).format('DD/MM/YYYY hh:mm:ss A')}
                </Column.Value>
              </Column.Content>
            </Column>
          )}
          <br />
          <br />
          <div className="columns">
            <div className="column is-5">
              <div>
                <div style={{ padding: 8, backgroundColor: '#F0802A' }}>
                  <span style={{ fontSize: 14, color: '#ffffff' }}>
                    {messages.label.customerPhoto}
                  </span>
                </div>
                <div
                  style={{
                    padding: 10,
                    display: 'flex',
                    justifyCenter: 'center',
                  }}
                >
                  {getDetailLoading ? (
                    <ContentLoader width={300} height={200}>
                      <rect
                        x="2"
                        y="9"
                        rx="0"
                        ry="0"
                        width="300"
                        height="200"
                      />
                    </ContentLoader>
                  ) : (
                    <Image.Magnify
                      width="70%"
                      mgWidth={200}
                      mgHeight={200}
                      urlImg={
                        data.urlImageSlf
                          ? BASE_URL + '/storage/' + data.urlImageSlf
                          : require('@/assets/img/default-image.jpg')
                      }
                    />
                  )}
                </div>
              </div>
            </div>
            <div
              className="column is-2"
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <div style={{ justifyContent: 'center' }}>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                  }}
                >
                  Liveness Score
                </div>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#2E7D32',
                    padding: 20,
                    color: 'white',
                    fontSize: 40,
                    minHeight: 140,
                    width: 175,
                  }}
                >
                  <span
                    style={{
                      maxWidth: 177,
                      wordWrap: 'break-word',
                      textAlign: 'center',
                    }}
                  >
                    {getDetailLoading ? (
                      <LoadIndicator type="digital" />
                    ) : (
                      <TextCurrency
                        value={data.scoreLiveness ? data.scoreLiveness : '0.0'}
                        decimalScale={3}
                      />
                    )}
                  </span>
                </div>
              </div>
            </div>
            <div className="column is-5">
              <div>
                <div style={{ padding: 8, backgroundColor: '#F0802A' }}>
                  <span style={{ fontSize: 14, color: '#ffffff' }}>
                    {messages.label.coNCustPhoto}
                  </span>
                </div>
                <div
                  style={{
                    padding: 10,
                    display: 'flex',
                    justifyCenter: 'center',
                  }}
                >
                  {getDetailLoading ? (
                    <ContentLoader width={300} height={200}>
                      <rect
                        x="2"
                        y="9"
                        rx="0"
                        ry="0"
                        width="300"
                        height="200"
                      />
                    </ContentLoader>
                  ) : (
                    <Image.Magnify
                      width="70%"
                      mgWidth={200}
                      mgHeight={200}
                      urlImg={
                        data.urlImageCslf
                          ? BASE_URL + '/storage/' + data.urlImageCslf
                          : require('@/assets/img/default-image.jpg')
                      }
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="footer-form">
            <Button
              size="small"
              onClick={() =>
                history.push(
                  '/customer/customer_management?page=' + (dataList.number + 1),
                  { filter: state.filter }
                )
              }
            >
              {messages.button.back}
            </Button>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

DetailCustomerManagement.propTypes = {
  getDetail: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  data: custManagementSelector('detailData'),
  getDetailLoading: custManagementSelector('getDetailLoading'),
  dataList: custManagementSelector('data'),
});

const mapDispatchToProps = (dispatch) => ({
  getDetail: (data) => dispatch(getDetail(data)),
});

export default compose(
  withReducerCustManagement,
  withMiddlewareCustManagement,
  connect(mapStateToProps, mapDispatchToProps)
)(DetailCustomerManagement);
