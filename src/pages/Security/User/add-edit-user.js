import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { userSelector } from '@/store/User/selectors';
import { addData, updateData} from '@/store/User/actions';
import { withReducerUser, withMiddlewareUser } from '@/store/User/injector';

import { droplistSelector } from '@/store/Droplist/selectors';
import { getAllApplication, getRoleByApp, getChannelByApp, getAllBranch } from '@/store/Droplist/actions';
import {
  withMiddlewareDroplist,
  withReducerDroplist
} from '@/store/Droplist/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Button, Column, Input, SelectSearch,
  ValidationFunc,
  UrlQueryFunc,
  ValidationsFunc,
  ArrayFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class AddUser extends Component {
  constructor(props) {
    super(props);
    const editPath = '/security/user/update';
    const { location = {} } = props;
    const { pathname = '' } = location;
    this.state = {
      form: {
        id: '',
        name: '',
        email: '',
        application: 'BO',
        applicationChannelList: '',
        roleList: '',
        branchId: ''
      },
      errors: {},
      isEdit: pathname === editPath,
      isDisableUpdate: true
    };
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    let { form, isEdit } = this.state;

    this.props.getAllApplication();
    this.props.getRoleByApp(form.application);
    this.props.getChannelByApp(form.application);
    this.props.getAllBranch();

    if(isEdit){
      const { detailData, location } = this.props;
      if(!detailData){
        const { state } = location;
        const id = UrlQueryFunc.getQueryString('id');
        history.push('/security/user/detail?id='+id, {
          dataDetail: state ? state.dataDetail : '',
          filter: state ? state.filter : {}
        });
      }else{
        form = {
          id: detailData.id,
          name: detailData.name,
          email: detailData.email,
          application: form.application,
          applicationChannelList: detailData.applicationChannelList[0].applicationCode === 'BO' ? detailData.applicationChannelList.map(x => x.channelCode).join(', ') : '',
          roleList: detailData.applicationChannelList[0].applicationCode === 'BO' ? detailData.roleList.map(x => x.code).join(', ') : '',
          branchId: detailData.branchId ? detailData.branchId : ''
        };

        this.setState({
          form,
          oldData: JSON.parse(JSON.stringify(form))
        });
      }
    }
  }

  validationOptions() {
    const validationOptions = {
      id: {
        validator: [ValidationsFunc.Type.required()]
      },
      name: {
        validator: [ValidationsFunc.Type.required()]
      },
      email: {
        validator: [ValidationsFunc.Type.required()]
      },
      application: {
        validator: [ValidationsFunc.Type.required()]
      },  
      applicationChannelList: {
        validator: [ValidationsFunc.Type.required()]
      },
      roleList: {
        validator: [ValidationsFunc.Type.required()]
      },
      branchId: {
        validator: [ValidationsFunc.Type.required()]
      },
    };
    return validationOptions;
  }

  onChangeForm(data){
    let {form, oldData, isDisableUpdate} = this.state;
    const {name, value} = data;

    form[name] = value;
    const errors = ValidationsFunc.RunValidate(
      name,
      form,
      this.validationOptions()
    );

    if(oldData){
      isDisableUpdate = ValidationFunc.objValidationUpdate(oldData, form);
    }

    this.setState({
      form,
      errors,
      isDisableUpdate
    });
  }

  onSubmit(e){
    e.preventDefault();
    const {form, isEdit} = this.state;
    const errors = ValidationsFunc.RunValidateAll(
      form,
      this.validationOptions()
    );
    const errorsKey = Object.keys(errors);
    const {mmsList, roles, channels} = this.props;

    if(errorsKey.length === 0){
      let arrayRole = form.roleList.split(',');
      form.roleList = [];
      arrayRole.map(item => {
        var index = roles.map(x => x.value).indexOf(item);
        var newData = {
          code: roles[index]['value'],
          name: roles[index]['label']
        };
        form.roleList.push(newData);
      });

      let arrayChannel = form.applicationChannelList.split(',');
      form.applicationChannelList = [];
      arrayChannel.map(item => {
        var index = channels.map(x => x.value).indexOf(item);
        var newData = {
          applicationCode: form.application,
          channelCode: channels[index]['value']
        };
        form.applicationChannelList.push(newData);
      });
    
      form.branchCode = mmsList[mmsList.map(e => e.id).indexOf(form.branchId)].code;
      form.branchName = mmsList[mmsList.map(e => e.id).indexOf(form.branchId)].name;

      if(isEdit){
        this.props.updateData(form);
      }else{
        this.props.addData(form);
      }
      
      return;
    }

    this.setState({errors});
  }

  render() {
    const {form, errors, isEdit, isDisableUpdate} = this.state;
    const {location, updateDataLoading, addDataLoading, roles = [], channels = [], mmsList = [], applicationList = [], getAllApplicationLoading, getRoleByAppLoading, getAllBranchLoading, getChannelByAppLoading} = this.props;
    const {title, state} = location;
    return (
      <HomeBase breadcrumb={location} title={messages.menu.user.add} headTitle={title}>
        <Container fluid>
          <form onSubmit={this.onSubmit}>
            <Column>
              <Column.Content>
                <Column.Name text={messages.label.userId+' *'}/>
                <Column.Value>
                  <Input.InputCode
                    name="id"
                    value={form.id}
                    error={errors.id}
                    onChange={this.onChangeForm}
                    width={300}
                    language={messages}
                    disabled={isEdit}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.name+' *'}/>
                <Column.Value>
                  <Input
                    name="name"
                    value={form.name}
                    error={errors.name}
                    onChange={this.onChangeForm}
                    width={300}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.email+' *'}/>
                <Column.Value>
                  <Input.Email
                    name="email"
                    value={form.email}
                    error={errors.email}
                    width={300}
                    onChange={this.onChangeForm}
                    language={messages}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.application+' *'}/>
                <Column.Value>
                  <SelectSearch
                    name="application"
                    width={getAllApplicationLoading ? 270 : 300}
                    value={form.application}
                    error={errors.application}
                    placeholder="Select Application"
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(applicationList, 'id', 'value')}
                    disabled={true}
                    isClearable={false}
                    isLoading={getAllApplicationLoading}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.channel+' *'}/>
                <Column.Value>
                  <SelectSearch
                    isMulti
                    name="applicationChannelList"
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(channels, 'channelCode', 'channelName')}
                    placeholder="Select Channel"
                    isRemoveSelected={true}
                    isLoading={getChannelByAppLoading}
                    value={form.applicationChannelList}
                    error={errors.applicationChannelList}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.role+' *'}/>
                <Column.Value>
                  <SelectSearch
                    isMulti
                    name="roleList"
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(roles, 'id', 'value')}
                    placeholder="Select Role"
                    isRemoveSelected={true}
                    isLoading={getRoleByAppLoading}
                    value={form.roleList}
                    error={errors.roleList}
                  />
                </Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.mms+' *'}/>
                <Column.Value>
                  <SelectSearch
                    name="branchId"
                    width={getAllBranchLoading ? 270 : 300}
                    value={form.branchId}
                    error={errors.branchId}
                    placeholder="Select MMS"
                    onChange={this.onChangeForm}
                    options={ArrayFunc.onArrangeDataSelect(mmsList, 'id', 'value')}
                    disabled={getAllBranchLoading}
                    isClearable={false}
                    isLoading={getAllBranchLoading}
                  />
                </Column.Value>
              </Column.Content>
            </Column>
            <div className="footer-form">
              <Button
                size="small"
                role="submit"
                loading={addDataLoading || updateDataLoading}
                disabled={isEdit && isDisableUpdate}
              >{isEdit ? messages.button.update : messages.button.add}</Button>&nbsp;
              <Button
                size="small"
                onClick={() => 
                  isEdit 
                    ? history.push('/security/user/detail?id='+form.id, {
                      dataDetail: state ? this.state.dataDetail : '',
                      filter: state ? this.state.filter : {}
                    }) 
                    : history.push('/security/user')
                }
              >{messages.button.back}</Button>
            </div>
          </form>
        </Container>
      </HomeBase>
    );
  }
}

AddUser.propTypes = {
  updateData: PropTypes.func,
  addData: PropTypes.func,
  getAllApplication: PropTypes.func,
  getRoleByApp: PropTypes.func,
  getChannelByApp: PropTypes.func,
  getAllBranch: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  detailData: userSelector('detailData'),
  updateDataLoading: userSelector('updateDataLoading'),
  addDataLoading: userSelector('addDataLoading'),
  getAllApplicationLoading: droplistSelector('getAllApplicationLoading'),
  applicationList: droplistSelector('applicationList'),
  getRoleByAppLoading: droplistSelector('getRoleByAppLoading'),
  roles: droplistSelector('roleByAppList'),
  getChannelByAppLoading: droplistSelector('getChannelByAppLoading'),
  channels: droplistSelector('channelByAppList'),
  getAllBranchLoading: droplistSelector('getAllBranchLoading'),
  mmsList: droplistSelector('branchList')
});

const mapDispatchToProps = dispatch => ({
  addData: (data) => dispatch(addData(data)),
  updateData: (data) => dispatch(updateData(data)),
  getAllApplication: () => dispatch(getAllApplication()),
  getRoleByApp: (appId) => dispatch(getRoleByApp(appId)),
  getChannelByApp: (appId) => dispatch(getChannelByApp(appId)),
  getAllBranch: () => dispatch(getAllBranch())
});

export default compose(
  withReducerUser,
  withMiddlewareUser,
  withReducerDroplist,
  withMiddlewareDroplist,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(AddUser);
