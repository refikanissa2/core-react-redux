import React, { Component } from 'react';

import history from '@/routes/history';
import language from '@/helpers/messages';
import DetailColumn from './detail-column-user';
import HomeBase from '@/pages/home';
import {
  Container, Button, Message
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class ApprovalShowUser extends Component {
  render () {
    const {location} = this.props;
    const {state, title} = location;
    const {data} = state;
    return (
      <HomeBase breadcrumb={location} title={messages.menu.user.detail} headTitle={title}>
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
            autoClose={false}
          />
          <DetailColumn data={data} createdInfo />
          <div className="footer-form">
            <Button
              size="small"
              onClick={() => history.push('/security/user')}
            >{messages.button.back}</Button>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

export default ApprovalShowUser;
