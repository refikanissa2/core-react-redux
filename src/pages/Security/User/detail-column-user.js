import React from 'react';
import moment from 'moment';

import language from '@/helpers/messages';
import { Column } from 'component-ui-web-teravin';
const messages = language.getLanguage();

export default class DetailColumn extends React.Component {
  render() {
    const { data = {}, extraLabel = '', createdInfo = false } = this.props;
    return (
      <div>
        <Column>
          <Column.Content>
            <Column.Name text={`${messages.label.userId} ${extraLabel}`} />
            <Column.Value>{data.id}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.name} ${extraLabel}`} />
            <Column.Value>{data.name}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.email} ${extraLabel}`} />
            <Column.Value>{data.email}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.application} ${extraLabel}`} />
            <Column.Value>{data.applicationChannelList && data.applicationChannelList.length > 0 ? data.applicationChannelList[0].applicationCode : ''}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.channel} ${extraLabel}`} />
            <Column.Value>
              <p className="wrap-text">
                {data.applicationChannelList && data.applicationChannelList.length > 0 ? data.applicationChannelList.map(x => x.channelCode).join(', ') : ''}
              </p></Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.role} ${extraLabel}`} />
            <Column.Value>
              <p className="wrap-text">
                {data.roleList && data.roleList.length > 0 ? data.roleList.map(x => x.name).join(', ') : ''}
              </p>
            </Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.label.status} ${extraLabel}`} />
            <Column.Value>{data.userStatus}</Column.Value>
          </Column.Content>
          <Column.Content>
            <Column.Name text={`${messages.menu.branch.title} ${extraLabel}`} />
            <Column.Value>{data.branchName}</Column.Value>
          </Column.Content>
        </Column>
        {createdInfo &&
          <Column>
            <Column.Content>
              <Column.Name text={messages.label.createdBy}/>
              <Column.Value>{data.createdBy}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.dateCreated}/>
              <Column.Value>{data.dateCreated == null ? '' : moment(data.dateCreated).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.updatedBy}/>
              <Column.Value>{data.updatedBy}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={messages.label.lastUpdated}/>
              <Column.Value>{data.lastUpdated == null ? '' : moment(data.lastUpdated).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
            </Column.Content>
          </Column>
        }
      </div>
    );
  }
}
