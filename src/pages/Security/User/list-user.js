import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { withReducerUser, withMiddlewareUser } from '@/store/User/injector';
import { getList } from '@/store/User/actions';
import { userSelector } from '@/store/User/selectors';

import { withReducerDroplist, withMiddlewareDroplist } from '@/store/Droplist/injector';
import { getAllRole } from '@/store/Droplist/actions';
import { droplistSelector } from '@/store/Droplist/selectors';

import { withReducerUploadFile, withMiddlewareUploadFile } from '@/store/UploadFile/injector';
import { uploadFile } from '@/store/UploadFile/actions';
import { uploadFileSelector } from '@/store/UploadFile/selectors';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Message, Card, Input, Button, Pagination,
  UrlQueryFunc,
  ArrayFunc,
  SelectSearch,
  TableWithSelect
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class ListUserManagement extends Component{
  constructor(){
    super();
    this.state={
      filter: {
        id: '',
        name: '',
        roleCode: '',
        userStatus: ''
      },
      isSearching: false,
      importFile: 'No file selected',
      valProgressBarUpload: 0,
      form: {
        file: null,
        type: 'user_management'
      },
      errors: {
        file: ''
      },
      isDisableUpload: true
    };
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
    this.onChangeImportFile = this.onChangeImportFile.bind(this);
    this.onUpload = this.onUpload.bind(this);
  }

  getDataList(options = { page: false }) {
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    const { page } = options;
    let newPage = 1;
    if (!page && pageOnUrl) {
      newPage = parseInt(pageOnUrl);
    } else {
      newPage = page || newPage;
    }
    this.props.getList(Object.assign(options, { page: newPage }));
  }

  componentDidMount(){
    let filter = {};
    const { state } = this.props.location;
    if (state) {
      if (state.filter) {
        filter = state.filter;
        this.setState({ filter: state.filter });
      }
    }
    this.getDataList({...filter});
    this.props.getAllRole();
  }

  componentWillReceiveProps(props) {
    const isPropChange = name =>
      props[name] && props[name] !== this.props[name];
    const {data} = props;
    const { form } = this.state;

    if (isPropChange('getListSuccess') || isPropChange('getListFailed')) {
      this.setState({ isSearching: false });
      if(data.number > 0){
        history.push(`/security/user?page=${data.number+1}`);
      }else{
        history.push('/security/user');
      }
    }

    if(isPropChange('uploadFileSuccess')){
      this.setState({
        valProgressBarUpload: 0
      });
      clearInterval(this.interval);
    }

    if(isPropChange('uploadFileFailed')){
      form.file = null;
      document.getElementById('importFile').value = null;
      this.setState({
        valProgressBarUpload: 0,
        form: form,
        importFile: 'No file selected'
      });
      clearInterval(this.interval);
    }
  }

  setPages(page) {
    const { filter } = this.state;
    this.getDataList({ page: page + 1, ...filter });
  }

  onChangeFilter(data){
    const {filter} = this.state;
    const {value, name} = data;
    filter[name] = value;
    this.setState({
      filter: filter
    });
  }

  onSubmitFilter(e){
    e.preventDefault();
    const {filter} = this.state;
    this.setState({isSearching: true});
    this.getDataList({ ...filter, page: 1 });
  }

  onChangeImportFile(data, files){
    const {form, errors} = this.state;
    const {id} = data;
    if(files){
      const value = files;
      const name = value.name;
      const extension = name.substring(name.lastIndexOf('.')+1);
      if(extension == 'xls' || extension == 'xlsx'){
        form.file = value;
        errors.file = '';
        this.setState({
          importFile: name,
          form: form,
          errors: errors,
          isDisableUpload: false
        });
      }else{
        errors.file = 'This type of file not allow';
        form.file = null;
        document.getElementById(id).value = null;
        this.setState({
          form: form,
          errors: errors,
          importFile: 'No file selected',
          isDisableUpload: true
        });
      }
    }
  }

  onUpload(e){
    e.preventDefault();
    const {form} = this.state;
    this.interval = setInterval(() => this.frame(), 10);
    this.props.uploadFile(form);
  }

  frame(){
    this.setState((prevState) => ({
      valProgressBarUpload: prevState.valProgressBarUpload + 1
    }));
  }

  render(){
    const {
      isSearching,
      filter,
      importFile, valProgressBarUpload, errors, isDisableUpload
    } = this.state;
    const {location, roles = [], data = { content: []}, getAllRoleLoading, getListLoading, uploadFileLoading} = this.props;
    const {state, viewOnly = false, title} = location;
    return(
      <HomeBase breadcrumb={location} title={messages.menu.user.list} headTitle={title}>
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <Card>
            <Card.Header title={messages.label.filter}/>
            <Card.Content>
              <div className="content">
                <form onSubmit={this.onSubmitFilter}>
                  <div className="columns is-desktop">
                    <div className="column">
                      <h6>{messages.label.userId}</h6>
                      <Input
                        name="id"
                        value={filter.id}
                        onChange={this.onChangeFilter}
                        language={messages}
                      />
                    </div>
                    <div className="column">
                      <h6>{messages.label.name}</h6>
                      <Input
                        name="name"
                        value={filter.name}
                        onChange={this.onChangeFilter}
                        language={messages}
                      />
                    </div>
                    <div className="column">
                      <h6>{messages.label.role}</h6>
                      <SelectSearch
                        name="roleCode"
                        width={getAllRoleLoading ? 270 : 300}
                        value={filter.roleCode}
                        placeholder="Select Role"
                        onChange={this.onChangeFilter}
                        options={ArrayFunc.onArrangeDataSelect(roles, 'id', 'value')}
                        disabled={getAllRoleLoading}
                        isLoading={getAllRoleLoading}
                      />
                    </div>
                    <div className="column">
                      <h6>{messages.label.status}</h6>
                      <SelectSearch
                        name="userStatus"
                        width={300}
                        value={filter.userStatus}
                        placeholder="Select User Status"
                        onChange={this.onChangeFilter}
                        options={[
                          {
                            label: 'Active',
                            value: 'ACTIVE'
                          },{
                            label: 'Inactive',
                            value: 'DELETED'
                          }
                        ]}
                      />
                    </div>
                    <div className="column">
                      <h6>&nbsp;</h6>
                      <p className="control" style={{display: 'flex'}}>
                        <Button
                          size="small"
                          role="submit"
                          loading={isSearching}
                        >{messages.button.search}</Button>&nbsp;
                        {viewOnly == false &&
                          <Button
                            size="small"
                            onClick={() => history.push('/security/user/add')}
                          >{messages.button.add}</Button>
                        }
                      </p>
                    </div>
                  </div>
                </form>
              </div>
            </Card.Content>
          </Card>
          <div>&nbsp;</div>
          {viewOnly == false &&
            <Card>
              <Card.Header title={messages.label.import} />
              <Card.Content>
                <div className="content">
                  <form onSubmit={this.onUpload} encType="multipart/form-data">
                    <div className="columns">
                      <div className="column is-one-third" style={{display: 'flex', 'alignItems': 'center'}}>
                        <Input.File
                          name="importFile"
                          id="importFile"
                          acceptTypeFile="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                          onChange={this.onChangeImportFile}
                          filename={importFile}
                          error={errors.file}
                          language={messages}
                        />
                      </div>
                      <div className="column is-one-third">
                        <p className="control">
                          <Button
                            size="small"
                            role="submit"
                            loading={uploadFileLoading}
                            disabled={isDisableUpload}
                          >
                            <span className="icon">
                              <i className="fa fa-upload"></i>
                            </span>
                            <span>{messages.button.upload}</span>
                          </Button>
                        </p>
                      </div>
                    </div>
                    {uploadFileLoading &&
                      <div className="columns">
                        <div className="column">
                          <progress className="progress is-primary" value={valProgressBarUpload} max="100"/>
                        </div>
                      </div>
                    }
                    <div className="columns">
                      <div className="column">
                        <a onClick={() => history.push('/service/status_upload_bulk')}>{messages.text.linkToStatusUpload}</a>
                      </div>
                    </div>
                  </form>
                </div>
              </Card.Content>
            </Card>
          }
          <div>&nbsp;</div>
          <TableWithSelect
            fullwidth
            language={messages}
            loading={getListLoading || isSearching}
            headers={[
              {
                label: messages.label.userId,
                value: 'id'
              },
              {
                label: messages.label.name,
                value: 'name'
              },
              {
                label: messages.label.status,
                customValue: item => {
                  var status = item.userStatus;
                  if(status == 'DELETED'){
                    status = 'INACTIVE';
                  }
                  return <span>{status}</span>;
                }
              }
            ]}
            data={data.content}
            onSelected={index =>
              history.push('/security/user/detail?id='+data.content[index].id, {
                dataDetail: `${data.content[index].id} - ${data.content[index].name}`,
                filter: filter
              })
            }
          />
          {data.totalPages > 1 &&
            <Pagination
              setPages = {(page) => this.setPages(page)}
              {...data}
              language={messages}
            />
          }
        </Container>
      </HomeBase>
    );
  }
}

ListUserManagement.propTypes = {
  getList: PropTypes.func,
  getAllRole: PropTypes.func,
  uploadFile: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: userSelector('data'),
  getListSuccess: userSelector('getListSuccess'),
  getListFailed: userSelector('getListFailed'),
  getAllRoleLoading: droplistSelector('getAllRoleLoading'),
  roles: droplistSelector('roleList'),
  uploadFileSuccess: uploadFileSelector('uploadFileSuccess'),
  uploadFileFailed: uploadFileSelector('uploadFileFailed'),
  uploadFileLoading: uploadFileSelector('uploadFileLoading')
});

const mapDispatchToProps = dispatch => ({
  getList: (optionData) => dispatch(getList(optionData)),
  getAllRole: () => dispatch(getAllRole()),
  uploadFile: (data) => dispatch(uploadFile(data))
});

export default compose(
  withReducerUser,
  withMiddlewareUser,
  withReducerDroplist,
  withMiddlewareDroplist,
  withReducerUploadFile,
  withMiddlewareUploadFile,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ListUserManagement);
