import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { withReducerUser, withMiddlewareUser } from '@/store/User/injector';
import { userSelector } from '@/store/User/selectors';
import { getDetail, deleteData } from '@/store/User/actions';

import history from '@/routes/history';
import language from '@/helpers/messages';
import DetailColumn from './detail-column-user';
import HomeBase from '@/pages/home';
import {
  Container, Button, ModalConfirmation,ContentLoader,
  UrlQueryFunc
} from 'component-ui-web-teravin';
import colors from '@/assets/sass/colors.scss';
const messages = language.getLanguage();

export class DetailUser extends Component {
  constructor() {
    super();
    this.state = {
      deleteModal: false
    };
    this.onDeleteData = this.onDeleteData.bind(this);
  }

  componentDidMount() {
    const {location, dataList} = this.props;
    const {state} = location;
    if(state){
      if(state.filter){
        window.onpopstate = this.onBackBrowser.bind(this, state.filter, dataList.number+1);
      }
    }
    const id = UrlQueryFunc.getQueryString('id');
    this.props.getDetail({id: id, dataDetail: state ? state.dataDetail : ''});
  }

  onBackBrowser(filter, page){
    history.push('/security/user?page='+page, {filter: filter});
  }

  componentWillReceiveProps(props) {
    const isPropChange = name =>
      props[name] && props[name] !== this.props[name];

    if (isPropChange('deleteDataFailed') || isPropChange('deleteDataSuccess')) {
      this.setState({ deleteModal: false });
    }
  }

  onDeleteData(){
    const {data} = this.props;
    this.props.deleteData(data.id);
  }

  render() {
    const {deleteModal} = this.state;
    const {location, data = {}, dataList = {}, getDetailLoading, deleteDataLoading} = this.props;
    const {viewOnly, title, state} = location;
    return (
      <HomeBase breadcrumb={location} title={messages.menu.user.detail} headTitle={title}>
        <Container fluid>
          {getDetailLoading ? (
            <ContentLoader.Detail />
          ):(
            <DetailColumn data={data} createdInfo />
          )}
          <div className="footer-form">
            {viewOnly == false &&
              <span>
                <Button
                  size="small"
                  onClick={() => history.push(`/security/user/update?id=${data.id}`, {
                    dataDetail: state ? state.dataDetail : '', filter: state ? state.filter : {}
                  })}
                >{messages.button.edit}</Button>&nbsp;
                <Button
                  size="small"
                  type="danger"
                  onClick={() => this.setState({deleteModal: true})}
                >{messages.button.delete}</Button>&nbsp;
              </span>
            }
            <Button
              size="small"
              onClick={() => history.push('/security/user?page='+(dataList ? dataList.number+1 : 1), {filter: state.filter})}
            >{messages.button.back}</Button>
          </div>
        </Container>
        <ModalConfirmation
          message={messages.text.confirmDelete}
          active={deleteModal}
          loading={deleteDataLoading}
          headerColor={colors.primary}
          onConfirm={this.onDeleteData}
          onClose={() => this.setState({deleteModal: false})}
        />
      </HomeBase>
    );
  }
}

DetailUser.propTypes = {
  getDetail: PropTypes.func,
  deleteData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: userSelector('detailData'),
  getDetailLoading: userSelector('getDetailLoading'),
  deleteDataSuccess: userSelector('deleteDataSuccess'),
  deleteDataFailed: userSelector('deleteDataFailed'),
  deleteDataLoading: userSelector('deleteDataLoading'),
  dataList: userSelector('data')
});

const mapDispatchToProps = dispatch => ({
  getDetail: (id)  => dispatch(getDetail(id)),
  deleteData: (id) => dispatch(deleteData(id))
});

export default compose(
  withReducerUser,
  withMiddlewareUser,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(DetailUser);
