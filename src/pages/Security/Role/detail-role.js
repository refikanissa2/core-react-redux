import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withReducerRole, withMiddlewareRole } from '@/store/Role/injector';
import { roleSelector } from '@/store/Role/selectors';
import { getDetail, deleteData, getMenuTree } from '@/store/Role/actions';

import history from '@/routes/history';
import language from '@/helpers/messages';
import DetailColumn from './detail-column-role';
import HomeBase from '@/pages/home';
import {
  Container, Button, ModalConfirmation,
  UrlQueryFunc,
  ContentLoader
} from 'component-ui-web-teravin';
import colors from '@/assets/sass/colors.scss';
import LoaderTree from './loader-tree';
const messages = language.getLanguage();

export class DetailRole extends Component {
  constructor(props) {
    super(props);

    this.state = {
      deleteModal: false,
      checkedList: []
    };
    this.onDeleteData = this.onDeleteData.bind(this);
  }

  componentDidMount() {
    const {location, dataList} = this.props;
    const {state} = location;
    if(state && dataList){
      if(state.filter){
        window.onpopstate = this.onBackBrowser.bind(this, state.filter, dataList.number+1);
      }
    }
    const id = UrlQueryFunc.getQueryString('id');
    this.props.getDetail({id: id, dataDetail: state ? state.dataDetail : ''});
  }

  onBackBrowser(filter, page){
    history.push('/security/role?page='+page, {filter: filter});
  }

  componentWillReceiveProps(props) {
    const isPropChange = name =>
      props[name] && props[name] !== this.props[name];
    const { data } = props;

    if(isPropChange('getDetailSuccess')){
      this.props.getMenuTree(data.applicationCode);
      
    }
    if(isPropChange('getMenuTreeSuccess')){
      var checkedList = data.menuList;
      this.setState({
        checkedList: checkedList
      });
    }

    if(isPropChange('deleteDataFailed') || isPropChange('deleteDataSuccess')){
      this.setState({deleteModal: false});
    }
  }

  onDeleteData(){
    const {data} = this.props;
    this.props.deleteData(data.code);
  }

  render() {
    const {deleteModal, checkedList} = this.state;
    var {location, data = {}, menuTree = [], dataList = {}, getDetailLoading, deleteDataLoading} = this.props;
    const {viewOnly, title, state} = location;
    return (
      <HomeBase breadcrumb={location} title={messages.menu.role.detail} headTitle={title}>
        <Container fluid>
          {getDetailLoading ? (
            <div className="columns">
              <div className="column is-7">
                <ContentLoader.Detail />
              </div>
              <div className="column is-5">
                <LoaderTree />
              </div>
            </div>
            
          ):(
            <DetailColumn 
              data={data} 
              menuTree={menuTree} 
              checkedList={checkedList} 
              createdInfo
            />
          )}
          
          <div className="footer-form">
            {viewOnly == false &&
              <span>
                <Button
                  size="small"
                  onClick={() => history.push('/security/role/update?id='+data.code, {
                    dataDetail: state.dataDetail, filter: state.filter
                  })}
                >{messages.button.edit}</Button>&nbsp;
                <Button
                  size="small"
                  type="danger"
                  onClick={() => this.setState({deleteModal: true})}
                >{messages.button.delete}</Button>&nbsp;
              </span>
            }
            <Button
              size="small"
              onClick={() => history.push('/security/role?page='+(dataList.number+1), {filter: state.filter})}
            >{messages.button.back}</Button>
          </div>
        </Container>
        <ModalConfirmation
          message={messages.text.confirmDelete}
          active={deleteModal}
          loading={deleteDataLoading}
          headerColor={colors.primary}
          onConfirm={this.onDeleteData}
          onClose={() => this.setState({deleteModal: false})}
        />
      </HomeBase>
    );
  }
}

DetailRole.propTypes = {
  getDetail: PropTypes.func,
  deleteData: PropTypes.func,
  getMenuTree: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: roleSelector('detailData'),
  getDetailSuccess: roleSelector('getDetailSuccess'),
  getDetailLoading: roleSelector('getDetailLoading'),
  deleteDataSuccess: roleSelector('deleteDataSuccess'),
  deleteDataFailed: roleSelector('deleteDataFailed'),
  deleteDataLoading: roleSelector('deleteDataLoading'),
  getMenuTreeSuccess: roleSelector('getMenuTreeSuccess'),
  menuTree: roleSelector('menuTree'),
  dataList: roleSelector('data')
});

const mapDispatchToProps = dispatch => ({
  getDetail: (id) => dispatch(getDetail(id)),
  deleteData: (id) => dispatch(deleteData(id)),
  getMenuTree: (data) => dispatch(getMenuTree(data))
});

export default compose(
  withMiddlewareRole,
  withReducerRole,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(DetailRole);
