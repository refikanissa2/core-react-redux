import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { roleSelector } from '@/store/Role/selectors';
import { addData, updateData, getMenuTree} from '@/store/Role/actions';
import { withReducerRole, withMiddlewareRole } from '@/store/Role/injector';

import { droplistSelector } from '@/store/Droplist/selectors';
import { getAllApplication } from '@/store/Droplist/actions';
import {
  withMiddlewareDroplist,
  withReducerDroplist
} from '@/store/Droplist/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Input, Button, Tree, Column, Textarea, Alert, Select,
  ValidationFunc,
  UrlQueryFunc,
  ValidationsFunc,
  ContentLoader,
  ArrayFunc
} from 'component-ui-web-teravin';
import LoaderTree from './loader-tree';
import SelectSearch from 'component-ui-web-teravin/build/components/SelectSearch';
const messages = language.getLanguage();

export class AddRole extends Component {
  constructor(props) {
    super(props);
    const editPath = '/security/role/update';
    const { location = {} } = props;
    const { pathname = '' } = location;
    this.state = {
      form:{
        code: '',
        name: '',
        applicationCode: '',
        menuList: [],
        description:''
      },
      errors:{},
      isEdit: pathname === editPath,
      isDisableUpdate: true      
    };
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onCheck = this.onCheck.bind(this);
  }

  componentDidMount() {
    let { isEdit } = this.state;

    this.props.getAllApplication();

    if(isEdit){
      const { detailData, location } = this.props;
      console.log('detailData :>> ', detailData);
      if(!detailData){
        const { state } = location;
        const id = UrlQueryFunc.getQueryString('id');
        history.push('/security/role/detail?id='+id, {
          dataDetail: state ? state.dataDetail : '',
          filter: state ? state.filter : {}
        });
      }else{
        this.props.getMenuTree(detailData.applicationCode);
      }
    }
  }

  componentWillReceiveProps(props) {
    const isPropChange = name =>
      props[name] && props[name] !== this.props[name];
    const { detailData, menuTree } = this.props;
    let { form, isEdit } = this.state;

    if(isPropChange('getMenuTreeSuccess')){
      if(isEdit){
        form = {
          code: detailData.code,
          name: detailData.name,
          applicationCode: detailData.applicationCode,
          description:detailData.description,
          menuList: detailData.menuList
        };
  
        var checkedList = form.menuList;
        checkedList.map(item => {
          var indexOnMenu = menuTree.map(x => x.code).indexOf(item);
          if(menuTree[indexOnMenu].parentCode){
            var parentCode = menuTree[indexOnMenu].parentCode;
            if(checkedList.includes(parentCode)){
              var indexParent = checkedList.indexOf(parentCode);
              checkedList.splice(indexParent, 1);
            }
          }
        });
  
        form.menuList = checkedList;
  
        this.setState({
          form,
          oldData: Object.assign({}, form)
        });
      }
    }
  }

  validationOptions() {
    const validationOptions = {
      code: {
        validator: [ValidationsFunc.Type.required()]
      },
      name: {
        validator: [ValidationsFunc.Type.required()]
      },
      applicationCode: {
        validator: [ValidationsFunc.Type.required()]
      },
      menuList: {
        validator: [ValidationsFunc.Type.required()]
      },  
      description: {
        validator: []
      }
    };
    return validationOptions;
  }

  onChangeForm(data){
    let {form, oldData, isDisableUpdate} = this.state;
    const {name, value} = data;

    form[name] = value;
    const errors = ValidationsFunc.RunValidate(
      name,
      form,
      this.validationOptions()
    );

    if(name == 'applicationCode'){
      form.menuList = [];
      this.props.getMenuTree(value);
    }
    
    if(oldData){
      isDisableUpdate = ValidationFunc.objValidationUpdate(oldData, form);
    }

    this.setState({
      form,
      errors,
      isDisableUpdate
    });
  }

  onCheck(checkedKeys) {
    const {form} = this.state;

    form.menuList = checkedKeys;

    this.setState({
      form
    });
  }

  onSave(e){
    e.preventDefault();
    const {form, isEdit} = this.state;
    const {menuTree} = this.props;
    const errors = ValidationsFunc.RunValidateAll(
      form,
      this.validationOptions()
    );
    const errorsKey = Object.keys(errors);
    const checkedList = form.menuList;
    
    if(errorsKey.length === 0){
      if(checkedList.length > 0){
        checkedList.map(item => {
          var indexOnMenu = menuTree.map(x => x.code).indexOf(item);
          if(menuTree[indexOnMenu].parentCode){
            var parentCode = menuTree[indexOnMenu].parentCode;
            if(!checkedList.includes(parentCode)){
              checkedList.push(parentCode);
            }
          }
        });
        form.menuList = checkedList;

        if(isEdit){
          this.props.updateData(form);
        }else{
          this.props.addData(form);
        }
      }else{
        Alert.error({message: messages.error.requiredMenuTree});
      }
      return;
    }

    this.setState({errors});
  }

  render() {
    const {form, errors, isEdit, isDisableUpdate} = this.state;
    const {location, updateDataLoading, addDataLoading, getMenuTreeLoading, getAllApplicationLoading, menuTree = [], applicationList = []} = this.props;
    const {title, state} = location;
    return (
      <HomeBase breadcrumb={location} title={messages.menu.role.add} headTitle={title}>
        <Container fluid>
          <form onSubmit={this.onSave}>
            <div className="columns">
              <div className="column is-7">
                <Column>
                  <Column.Content>
                    <Column.Name text={`${messages.label.code} *`}/>
                    <Column.Value>
                      <Input.InputCode
                        name="code"
                        value={form.code}
                        onChange={this.onChangeForm}
                        error={errors.code}
                        width={300}
                        maxLength={40}
                        language={messages}
                        disabled={isEdit}
                      />
                    </Column.Value>
                  </Column.Content>
                  <Column.Content>
                    <Column.Name text={`${messages.label.name} *`}/>
                    <Column.Value>
                      <Input
                        name="name"
                        value={form.name}
                        onChange={this.onChangeForm}
                        error={errors.name}
                        width={300}
                        language={messages}
                      />
                    </Column.Value>
                  </Column.Content>
                  <Column.Content>
                    <Column.Name text={`${messages.label.application} *`}/>
                    <Column.Value>
                      <SelectSearch
                        name="applicationCode"
                        width={getAllApplicationLoading ? 270 : 300}
                        value={form.applicationCode}
                        error={errors.applicationCode}
                        onChange={this.onChangeForm}
                        options={ArrayFunc.onArrangeDataSelect(applicationList, 'id', 'value')}
                        disabled={getAllApplicationLoading}
                        isClearable={false}
                        isLoading={getAllApplicationLoading}
                      />
                    </Column.Value>
                  </Column.Content>
                  <Column.Content>
                    <Column.Name text={`${messages.label.description}`}/>
                    <Column.Value>
                      <Textarea
                        name="description"
                        value={form.description}
                        onChange={this.onChangeForm}
                        width={300}
                      />
                    </Column.Value>
                  </Column.Content>
                </Column>
              </div>
              <div className="column is-5">
                {getMenuTreeLoading ? (
                  <LoaderTree />
                ):(
                  <Tree
                    data={menuTree}
                    checkable={true}
                    onCheck={this.onCheck}
                    checkedKeys={form.menuList}
                  />
                )}
              </div>
            </div>
            <div className="footer-form">
              <Button
                size="small"
                role="submit"
                loading={addDataLoading || updateDataLoading}
                disabled={isEdit && isDisableUpdate}
              >{isEdit ? messages.button.update : messages.button.add}</Button>&nbsp;
              <Button
                size="small"
                onClick={() => 
                  isEdit 
                    ? history.push('/security/role/detail?id='+form.id, {
                      dataDetail: state ? this.state.dataDetail : '',
                      filter: state ? this.state.filter : {}
                    }) 
                    : history.push('/security/role')
                }
              >{messages.button.back}</Button>
            </div>
          </form>
        </Container>
      </HomeBase>
    );
  }
}

AddRole.propTypes = {
  addData: PropTypes.func,
  updateData: PropTypes.func,
  getAllApplication: PropTypes.func,
  getMenuTree: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  detailData: roleSelector('detailData'),
  addDataLoading: roleSelector('addDataLoading'),
  updateDataLoading: roleSelector('updateDataLoading'),
  getAllApplicationLoading: droplistSelector('getAllApplicationLoading'),
  applicationList: droplistSelector('applicationList'),
  getMenuTreeSuccess: roleSelector('getMenuTreeSuccess'),
  getMenuTreeLoading: roleSelector('getMenuTreeLoading'),
  menuTree: roleSelector('menuTree')
});

const mapDispatchToProps = dispatch => ({
  addData: (data) => dispatch(addData(data)),
  updateData: (data) => dispatch(updateData(data)),
  getAllApplication: () => dispatch(getAllApplication()),
  getMenuTree: (data) => dispatch(getMenuTree(data))
});

export default compose(
  withReducerRole,
  withMiddlewareRole,
  withReducerDroplist,
  withMiddlewareDroplist,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(AddRole);
