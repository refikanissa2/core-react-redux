import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { roleSelector } from '@/store/Role/selectors';
import { getMenuTree } from '@/store/Role/actions';
import { withReducerRole, withMiddlewareRole } from '@/store/Role/injector';

import history from '@/routes/history';
import language from '@/helpers/messages';
import DetailColumn from './detail-column-role';
import HomeBase from '@/pages/home';
import {
  Container, Button, Message
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

class ApprovalShowRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedList: []
    };
  }

  componentDidMount() {
    const {state} = this.props.location;
    const {data} = state;
    this.props.getMenuTree(data.applicationCode);
  }

  componentWillReceiveProps(props) {
    const isPropChange = name =>
      props[name] && props[name] !== this.props[name];
    const { menuTree, location } = this.props;
    const {state} = location;
    const {data} = state;

    if(isPropChange('getMenuTreeSuccess')){
      var checkedList = data.menuList;
      this.setState({
        checkedList: checkedList
      });
    }
  }

  render () {
    const {location, menuTree = [], getMenuTreeLoading} = this.props;
    const {state, title} = location;
    const {data} = state;
    const {checkedList} = this.state;
    return (
      <HomeBase breadcrumb={location} title={messages.menu.role.detail} headTitle={title}>
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
            autoClose={false}
          />
          <DetailColumn data={data} menuTree={menuTree.length ? menuTree : []} checkedList={checkedList} createdInfo />
          <div className="footer-form">
            <Button
              size="small"
              onClick={() => history.push('/security/role')}
            >{messages.button.back}</Button>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ApprovalShowRole.propTypes = {
  getMenuTree: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  getMenuTreeSuccess: roleSelector('getMenuTreeSuccess'),
  getMenuTreeLoading: roleSelector('getMenuTreeLoading'),
  menuTree: roleSelector('menuTree')
});

const mapDispatchToProps = dispatch => ({
  getMenuTree: (data) => dispatch(getMenuTree(data))
});

export default compose(
  withMiddlewareRole,
  withReducerRole,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ApprovalShowRole);
