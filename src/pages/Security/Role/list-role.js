import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withReducerRole, withMiddlewareRole } from '@/store/Role/injector';
import { getList} from '@/store/Role/actions';
import { roleSelector } from '@/store/Role/selectors';

import { withReducerDroplist, withMiddlewareDroplist } from '@/store/Droplist/injector';
import { getAllApplication } from '@/store/Droplist/actions';
import { droplistSelector } from '@/store/Droplist/selectors';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Message, Input, Button, Pagination, Card,
  UrlQueryFunc,
  ValidationFunc,
  TableWithSelect,
  ArrayFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class ListRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: {
        code: '',
        name: '',
        applicationCode:''
      },
      errors: {
        applicationCode: '',
      },
      isSearching: false,
    };
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
  }

  getDataList(options = { page: false }) {
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    const { page } = options;
    let newPage = 1;
    if (!page && pageOnUrl) {
      newPage = parseInt(pageOnUrl);
    } else {
      newPage = page || newPage;
    }
    this.props.getList(Object.assign(options, { page: newPage }));
  }

  componentDidMount() {
    let filter = {};
    const { state } = this.props.location;
    if (state) {
      if (state.filter) {
        filter = state.filter;
        this.setState({ filter: state.filter });
      }
    }
    this.getDataList({...filter});
    this.props.getAllApplication();
  }

  componentWillReceiveProps(props) {
    const isPropChange = name =>
      props[name] && props[name] !== this.props[name];
    const {data} = props;

    if (isPropChange('getListSuccess') || isPropChange('getListFailed')) {
      this.setState({ isSearching: false });
      if(data.number > 0){
        history.push(`/security/role?page=${data.number+1}`);
      }else{
        history.push('/security/role');
      }
    }
  }

  setPages(page) {
    const { filter } = this.state;
    this.getDataList({ page: page + 1, ...filter });
  }

  onChangeFilter(data){
    const {filter, errors} = this.state;
    const {value, name} = data;
    filter[name] = value;
    if(errors[name]){
      errors[name] = '';
    }
    this.setState({
      filter: filter,
      errors: errors
    });
  }

  onSubmitFilter(e){
    e.preventDefault();
    const {filter, errors} = this.state;
    this.setState({isSearching: true});
    if(ValidationFunc.form(errors, filter)){
      this.getDataList({ ...filter, page: 1 });
      return;
    }
    this.setState({errors:ValidationFunc.messages(errors, filter), isSearching: false});
  }

  render() {
    const {filter, errors, isSearching} = this.state;
    const {location, getAllApplicationLoading, getListLoading, applicationList = [], data = { content: []}} = this.props;
    const {state, viewOnly, title} = location;
    return (
      <HomeBase breadcrumb={location} title={messages.menu.role.list} headTitle={title}>
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <div className="columns">
            <div className="column is-9">
              <TableWithSelect
                fullwidth
                language={messages}
                loading={getListLoading || isSearching}
                headers={[
                  {
                    label: messages.label.code,
                    value: 'code'
                  },
                  {
                    label: messages.label.role,
                    value: 'name'
                  }
                ]}
                data={data.content}
                onSelected={index =>
                  history.push('/security/role/detail?id='+data.content[index].code, {
                    dataDetail: `${data.content[index].code} - ${data.content[index].name}`,
                    filter: filter
                  })
                }
              />
              {data.totalPages > 1 &&
                <Pagination
                  setPages = {(page) => this.setPages(page)}
                  {...data}
                  language={messages}
                />
              }
            </div>
            <div className="column is-one-quarter is-hidden-mobile">
              <Card>
                <Card.Header title={messages.label.filter}/>
                <Card.Content>
                  <div className="content">
                    <form onSubmit={this.onSubmitFilter}>
                      <div className="field">
                        <label><h6>{messages.label.code}</h6></label>
                        <Input
                          name="code"
                          value={filter.code}
                          onChange={this.onChangeFilter}
                          maxLength={40}
                          language={messages}
                        />
                      </div>
                      <div className="field">
                        <label><h6>{messages.label.role}</h6></label>
                        <Input
                          name="name"
                          value={filter.name}
                          onChange={this.onChangeFilter}
                          size="small"
                          language={messages}
                        />
                      </div>
                      <div className="field">
                        <label><h6>{messages.label.application}</h6></label>
                        <SelectSearch
                          name="applicationCode"
                          width={getAllApplicationLoading ? 270 : 300}
                          value={filter.applicationCode}
                          error={errors.applicationCode}
                          onChange={this.onChangeFilter}
                          options={ArrayFunc.onArrangeDataSelect(applicationList, 'id', 'value')}
                          disabled={getAllApplicationLoading}
                          isLoading={getAllApplicationLoading}
                        />
                      </div>
                      <div className="columns">
                        <div className="column is-6">
                          <div className="field">
                            <p className="control">
                              <Button
                                size="small"
                                role="submit"
                                loading={isSearching}
                                block
                              >{messages.button.search}</Button>
                            </p>
                          </div>
                        </div>
                        {viewOnly == false &&
                          <div className="column is-6">
                            <div className="field">
                              <p className="control">
                                <Button
                                  size="small"
                                  onClick={() => history.push('/security/role/add')}
                                  block
                                >{messages.button.add}</Button>
                              </p>
                            </div>
                          </div>
                        }
                      </div>
                    </form>
                  </div>
                </Card.Content>
              </Card>
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ListRole.propTypes = {
  getList: PropTypes.func,
  getAllApplication: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: roleSelector('data'),
  getListSuccess: roleSelector('getListSuccess'),
  getListFailed: roleSelector('getListFailed'),
  getListLoading: roleSelector('getListLoading'),
  getAllApplicationLoading: droplistSelector('getAllApplicationLoading'),
  applicationList: droplistSelector('applicationList')
});

const mapDispatchToProps = dispatch => ({
  getList: (data) => dispatch(getList(data)),
  getAllApplication: () => dispatch(getAllApplication())
});

export default compose(
  withReducerRole,
  withMiddlewareRole,
  withReducerDroplist,
  withMiddlewareDroplist,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ListRole);
