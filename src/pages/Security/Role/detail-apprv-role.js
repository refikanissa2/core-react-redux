import React, { Component } from 'react';

import DetailTask from '@/pages/MyTask/ApprovalMaintenance/detail-myTask';
import DetailColumn from './detail-column-role';
import language from '@/helpers/messages';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { roleSelector } from '@/store/Role/selectors';
import { getMenuTree } from '@/store/Role/actions';
import { withReducerRole, withMiddlewareRole } from '@/store/Role/injector';

import { approvalMaintenanceSelector } from '@/store/ApprovalMaintenance/selectors';
import { withReducerApprovalMaintenance, withMiddlewareApprovalMaintenance } from '@/store/ApprovalMaintenance/injector';
import { ContentLoader } from 'component-ui-web-teravin';
import LoaderTree from './loader-tree';

const messages = language.getLanguage();

class DetailApproval extends Component {
  componentWillReceiveProps(nextProps) {
    const isPropChange = name =>
      nextProps[name] && nextProps[name] !== this.props[name];
    const { data } = nextProps;
    
    if(isPropChange('getPendingTaskSuccess')){
      this.props.getMenuTree(data.applicationCode);
    }
  }

  render() {
    let { location, match, menuTree = [], getPendingTaskLoading } = this.props;
    const taskType = match.params.taskType;
    return (
      <DetailTask
        module='role'
        title={messages.menu.role[taskType]}
        location={location}
        taskType={taskType}
      >
        {props => {
          const {data = {}, oldData = {}} = props;
          
          if(menuTree.length > 0){
            var checkedList = Object.keys(data).length > 0 && data.menuList ? data.menuList : [];
            if(taskType === 'edit'){
              var oldCheckedList = Object.keys(oldData).length > 0 && oldData.menuList ? oldData.menuList : [];
            }
          }
          return(
            <div className="columns">
              <div className={`column ${taskType == 'edit' ? 'is-half' : ''}`}>
                {getPendingTaskLoading ? (
                  <div className="columns">
                    <div className={`column ${taskType == 'edit' ? 'is-half' : ''}`}>
                      <ContentLoader.Detail />
                    </div>
                    {taskType != 'edit' &&
                      <div className="column is-5">
                        <LoaderTree />
                      </div>
                    }
                  </div>
                ):(
                  <DetailColumn
                    data={taskType === 'edit' ? oldData : data}
                    menuTree={menuTree}
                    checkedList={taskType === 'edit' ? oldCheckedList : checkedList}
                    isUpdate={taskType === 'edit'}
                  />
                )}
              </div>
              {taskType === 'edit' && (
                <div className='column is-half'>
                  {getPendingTaskLoading ? (
                    <div className="columns">
                      <div className="column is-half">
                        <ContentLoader.Detail />
                      </div>
                    </div>
                  ):(
                    <DetailColumn
                      data={data}
                      menuTree={menuTree}
                      checkedList={checkedList}
                      extraLabel={messages.label.toChange}
                      isUpdate={taskType === 'edit'}
                    />
                  )}
                </div>
              )}
            </div>
          );
        }}
      </DetailTask>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  menuTree: roleSelector('menuTree'),
  getPendingTaskLoading: approvalMaintenanceSelector('getPendingTaskLoading'),
  getPendingTaskSuccess: approvalMaintenanceSelector('getPendingTaskSuccess'),
  data: approvalMaintenanceSelector('detailPendingTask'),

});

const mapDispatchToProps = dispatch => ({
  getMenuTree: (data, options) => dispatch(getMenuTree(data, options))
});

export default compose(
  withReducerRole,
  withMiddlewareRole,
  withReducerApprovalMaintenance,
  withMiddlewareApprovalMaintenance,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(DetailApproval);
