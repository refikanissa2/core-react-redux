import React from 'react';
import moment from 'moment';

import language from '@/helpers/messages';
import { Column, Tree } from 'component-ui-web-teravin';
import colors from '@/assets/sass/colors.scss';
const messages = language.getLanguage();

export default class DetailColumn extends React.Component {
  render() {
    const {
      data = {},
      extraLabel = '',
      createdInfo = false,
      menuTree = [],
      checkedList = [],
      isUpdate = false,
    } = this.props;
    return (
      <div className="columns">
        <div className={`column ${isUpdate ? '' : 'is-7'}`}>
          <Column>
            <Column.Content>
              <Column.Name text={`${messages.label.code} ${extraLabel}`} />
              <Column.Value>{data.code}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={`${messages.label.name} ${extraLabel}`} />
              <Column.Value>
                <p className="wrap-text">
                  {data.name}
                </p>
              </Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={`${messages.label.application} ${extraLabel}`} />
              <Column.Value>{data.applicationCode}</Column.Value>
            </Column.Content>
            <Column.Content>
              <Column.Name text={`${messages.label.description} ${extraLabel}`} />
              <Column.Value>
                <p className="wrap-text">
                  {data.description}
                </p>
              </Column.Value>
            </Column.Content>
          </Column>
          {createdInfo &&
            <Column>
              <Column.Content>
                <Column.Name text={messages.label.createdBy}/>
                <Column.Value>{data.createdBy}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.dateCreated}/>
                <Column.Value>{data.dateCreated == null ? '' : moment(data.dateCreated).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.updatedBy}/>
                <Column.Value>{data.updatedBy}</Column.Value>
              </Column.Content>
              <Column.Content>
                <Column.Name text={messages.label.lastUpdated}/>
                <Column.Value>{data.lastUpdated == null ? '' : moment(data.lastUpdated).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
              </Column.Content>
            </Column>
          }
          {isUpdate &&
            <div>
              <div style={{padding: 8, backgroundColor: colors.primary}}>
                <span style={{fontSize: 14, color: colors.white, fontWeight: 'bold'}}>{messages.label.menuTree}</span>
              </div>
              <div style={{paddingLeft: '14px'}}>
                <Tree
                  data={menuTree}
                  checkable={true}
                  checkedKeys={checkedList}
                  disabled={true}
                />
              </div>
            </div>
          }
        </div>
        {!isUpdate &&
          <div className="column is-5">
            <Tree
              data={menuTree}
              checkable={true}
              checkedKeys={checkedList}
              disabled={true}
            />
          </div>
        }
      </div>
    );
  }
}
