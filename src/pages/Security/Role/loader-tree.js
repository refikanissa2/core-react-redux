import { ContentLoader } from 'component-ui-web-teravin';
import React, { Component } from 'react';

export default class LoaderTree extends Component {
  render() {
    return (
      <ContentLoader width={600} height={300}>
        <rect x="0" y="0" rx="0" ry="0" width="150" height="15" /> 
        <rect x="20" y="20" rx="0" ry="0" width="180" height="15" /> 
        <rect x="20" y="40" rx="0" ry="0" width="180" height="15" /> 
        <rect x="0" y="60" rx="0" ry="0" width="150" height="15" /> 
        <rect x="20" y="80" rx="0" ry="0" width="180" height="15" /> 
        <rect x="20" y="100" rx="0" ry="0" width="180" height="15" /> 
        <rect x="20" y="120" rx="0" ry="0" width="180" height="15" /> 
        <rect x="0" y="140" rx="0" ry="0" width="150" height="15" /> 
        <rect x="20" y="160" rx="0" ry="0" width="180" height="15" /> 
        <rect x="20" y="180" rx="0" ry="0" width="180" height="15" /> 
        <rect x="20" y="200" rx="0" ry="0" width="180" height="15" /> 
        <rect x="20" y="220" rx="0" ry="0" width="180" height="15" /> 
        <rect x="0" y="240" rx="0" ry="0" width="150" height="15" /> 
        <rect x="20" y="260" rx="0" ry="0" width="180" height="15" /> 
        <rect x="20" y="280" rx="0" ry="0" width="180" height="15" /> 
        <rect x="20" y="300" rx="0" ry="0" width="180" height="15" />
      </ContentLoader>
    );
  }
}
