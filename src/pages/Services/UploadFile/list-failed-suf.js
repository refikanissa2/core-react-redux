import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withReducerUploadFile, withMiddlewareUploadFile } from '@/store/UploadFile/injector';
import { getFailedFile, exportData } from '@/store/UploadFile/actions';
import { uploadFileSelector } from '@/store/UploadFile/selectors';

import moment from 'moment';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Button, Table, Column, Pagination, Alert,
  UrlQueryFunc,
  ValidationFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class ListFailedUploadFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form:{
        code: 'UploadFileFailedDetail',
        'parameter':{
          UploadFileId: ''
        },
        userId: this.props.userId
      },
      errors: {
        UploadFileId: ''
      }
    };
    this.onExport = this.onExport.bind(this);
  }

  getDataList(options = { page: false }) {
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    const { page } = options;
    let newPage = 1;
    if (!page && pageOnUrl) {
      newPage = parseInt(pageOnUrl);
    } else {
      newPage = page || newPage;
    }
    this.props.getFailedFile(Object.assign(options, { page: newPage }));
  }

  componentDidMount() {
    const {location, dataList} = this.props;
    const {state} = location;
    let filter = {};
    if(state){
      if(state.filter){
        filter = state.filter;
        this.setState({ filter: state.filter });
      }
    }

    const id = UrlQueryFunc.getQueryString('id');
    const {form} = this.state;
    form.parameter.UploadFileId = id;
    this.setState({ form: form });

    this.getDataList({...filter, id: id});
  }

  componentWillReceiveProps(nextProps) {
    const isPropChange = name =>
      nextProps[name] && nextProps[name] !== this.props[name];
    const {form} = this.state;
    const id = form.parameter.UploadFileId;
    const {
      exportDataSuccess, exportDataFailed, data, location
    } = nextProps;
    const {state} = location;
    
    if (isPropChange('getListSuccess') || isPropChange('getListFailed')) {
      this.setState({ isSearching: false });
      if(data.number > 0){
        history.push(`/service/status_upload_bulk/failed?id=${id}&page=${data.number+1}`, Object.assign(state));
      }else{
        history.push(`/service/status_upload_bulk/failed?id=${id}`);
      }
    }

    if(exportDataSuccess){
      this.setState({ isExporting: false });
    }
    if(exportDataFailed){
      this.setState({ isExporting: false });
      history.replace(`/service/status_upload_bulk/failed?id=${id}`);
    }
  }

  setPages(page){
    const { filter, form } = this.state;
    this.getDataList({ page: page + 1, ...filter, id: form.parameter.UploadFileId });
  }

  onExport(){
    const {form, errors} = this.state;
    const {parameter} = form;
    if(ValidationFunc.form(errors, parameter)){
      this.props.exportData(form);
      return;
    }
    this.setState({ errors: {UploadFileId: 'Upload file id not found'}});
    Alert.error({message: errors.UploadFileId});
  }

  render() {
    const {location, data = { content : []}, dataList = {}, getFailedFileLoading, exportDataLoading} = this.props;
    const {state, title} = location;
    let dataUserEdit = {};
    if(state) dataUserEdit = state.data;
    return (
      <HomeBase loader={getFailedFileLoading} breadcrumb={location} title={messages.menu.uploadedFile.failed} headTitle={title}>
        <Container fluid>
          <div className="columns">
            <div className="column is-12">
              <div style={{float: 'right'}}>
                <Button
                  size="small"
                  loading={exportDataLoading}
                  onClick={this.onExport}
                >
                  <span>{messages.button.export}</span>
                </Button>&nbsp;
                <Button
                  size="small"
                  onClick={() => history.push('/service/status_upload_bulk?page='+(dataList.number+1), {filter: state.filter})}
                >{messages.button.back}</Button>
              </div>
            </div>
          </div>
          <div className="columns">
            <div className="column is-12">
              <Table fullwidth>
                <Table.Head>
                  <Table.Label label={messages.label.row.toUpperCase()} />
                  <Table.Label label={messages.label.noWow} />
                  <Table.Label label={messages.label.note.toUpperCase()} />
                </Table.Head>
                <Table.Body>
                  {data.content && data.content.length > 0 &&
                  data.content.map((item, index) =>
                    <Table.Values key={index}>
                      <Table.Value value={item.rows} />
                      <Table.Value value={item.userId} />
                      <Table.Value value={item.note} />
                    </Table.Values>
                  )}
                </Table.Body>
              </Table>
            </div>
          </div>
          <div className="columns">
            <div className="column is-12">
              <Column>
                <Column.Content isBorder={false}>
                  <Column.Name bold={false} width={120} text={messages.label.uploadedBy}/>
                  <Column.Name bold={false} width={20} text=":"/>
                  <Column.Value>{dataUserEdit.uploadedBy}</Column.Value>
                </Column.Content>
                <Column.Content isBorder={false}>
                  <Column.Name bold={false} width={120} text={messages.label.dateUploaded}/>
                  <Column.Name bold={false} width={20} text=":"/>
                  <Column.Value>{dataUserEdit.dateUploaded == null ? '' : moment(dataUserEdit.dateUploaded).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
                </Column.Content>
                <Column.Content isBorder={false}>
                  <Column.Name bold={false} width={120} text={messages.label.approvedBy}/>
                  <Column.Name bold={false} width={20} text=":"/>
                  <Column.Value>{dataUserEdit.approvedBy}</Column.Value>
                </Column.Content>
                <Column.Content isBorder={false}>
                  <Column.Name bold={false} width={120} text={messages.label.lastApproved}/>
                  <Column.Name bold={false} width={20} text=":"/>
                  <Column.Value>{dataUserEdit.approvedDate == null ? '' : moment(dataUserEdit.approvedDate).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
                </Column.Content>
              </Column>
              {data.totalPages > 1 &&
                <Pagination
                  setPages = {(page) => this.setPages(page)}
                  {...data}
                  language={messages}
                />
              }
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ListFailedUploadFile.propTypes = {
  getFailedFile: PropTypes.func,
  exportData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  userId: uploadFileSelector('userId'),
  data: uploadFileSelector('failedData'),
  getFailedFileSuccess: uploadFileSelector('getFailedFileSuccess'),
  getFailedFileFailed: uploadFileSelector('getFailedFileFailed'),
  getFailedFileLoading: uploadFileSelector('getFailedFileLoading'),
  exportDataSuccess: uploadFileSelector('exportDataSuccess'),
  exportDataFailed: uploadFileSelector('exportDataFailed'),
  exportDataLoading: uploadFileSelector('exportDataLoading'),
  dataList: uploadFileSelector('data')
});

const mapDispatchToProps = dispatch => ({
  getFailedFile: (data) => dispatch(getFailedFile(data)),
  exportData: (data) => dispatch(exportData(data))
});

export default compose(
  withReducerUploadFile,
  withMiddlewareUploadFile,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ListFailedUploadFile);
