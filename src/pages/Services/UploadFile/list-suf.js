import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withReducerUploadFile, withMiddlewareUploadFile } from '@/store/UploadFile/injector';
import { getList } from '@/store/UploadFile/actions';
import { uploadFileSelector } from '@/store/UploadFile/selectors';
import moment from 'moment';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Message, Card, DatePicker, Input, Button, Table, Pagination,
  UrlQueryFunc,
  ValidationFunc,
  ConvertFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class ListStatusUploadFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSearching: false,
      filter: {
        fromDate: null,
        toDate: null,
        fileName: ''
      },
      errors: {
        fromDate: '',
        toDate: ''
      }
    };
    this.onChangeFilter = this.onChangeFilter.bind(this);
    this.onSubmitFilter = this.onSubmitFilter.bind(this);
  }

  getDataList(options = { page: false }) {
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    const { page } = options;
    let newPage = 1;
    if (!page && pageOnUrl) {
      newPage = parseInt(pageOnUrl);
    } else {
      newPage = page || newPage;
    }
    this.props.getList(Object.assign(options, { page: newPage }));
  }

  componentDidMount() {
    let filter = {};
    const { state } = this.props.location;
    if (state) {
      if (state.filter) {
        filter = state.filter;
        this.setState({ filter: state.filter });
      }
    }
    this.getDataList({...filter});
  }

  componentWillReceiveProps(nextProps) {
    const isPropChange = name =>
      nextProps[name] && nextProps[name] !== this.props[name];
    const {data} = nextProps;

    if (isPropChange('getListSuccess') || isPropChange('getListFailed')) {
      this.setState({ isSearching: false });
      if(data.number > 0){
        history.push(`/service/status_upload_bulk?page=${data.number+1}`);
      }else{
        history.push('/service/status_upload_bulk');
      }
    }
  }

  setPages(page){
    const { filter } = this.state;
    this.getDataList({ page: page + 1, ...filter });
  }

  onChangeFilter(data){
    const {filter, errors} = this.state;
    const {name, value, isValid, rawValue} = data;
    if(name == 'fromDate' || name == 'toDate'){
      if(isValid){
        if(name == 'fromDate'){
          if(value >= filter.toDate){
            filter.toDate = null;
            errors.toDate = '';
          }
        }
        filter[name] = value;
        errors[name] = '';
      }else{
        if(rawValue == ''){
          errors[name] = '';
        }else{
          errors[name] = 'Invalid Date';
        }
      }
    }else{
      filter[name] = value;
    }
    this.setState({
      filter: filter,
      errors: errors
    });
  }

  onSubmitFilter(e){
    e.preventDefault();
    this.setState({ isSearching: true });
    const {filter, errors} = this.state;
    const {state} = this.props.location;
    var newFilter = {
      fromDate: filter.fromDate == null ? '' : moment(filter.fromDate).format('DD/MM/YYYY'),
      toDate: filter.toDate == null ? '' : moment(filter.toDate).format('DD/MM/YYYY'),
      fileName: filter.fileName
    };
    
    if(newFilter.fromDate !== '' || newFilter.toDate !== ''){
      if(ValidationFunc.form(errors, newFilter)){
        this.getDataList({ ...newFilter, page: 1 });
        return;
      }
      this.setState({ errors: ValidationFunc.messages(errors, newFilter), isSearching: false });
    }else{
      errors.fromDate = '';
      errors.toDate = '';
      this.getDataList({ ...newFilter, page: 1 });
      this.setState({ errors: errors });
    }
  }

  render() {
    const {isSearching, filter, errors} = this.state;
    const {location, data = {content: []}, getListLoading} = this.props;
    const {state, title} = location;
    return (
      <HomeBase loader={getListLoading} breadcrumb={location} title={messages.menu.uploadedFile.list} headTitle={title}>
        <Container fluid>
          <Message
            show={state != null ? state.alert : false}
            message={state != null ? state.message : ''}
            type={state != null ? state.type : 'success'}
          />
          <div className="columns">
            <div className="column is-12">
              <Card>
                <Card.Header title={messages.label.filter} />
                <Card.Content>
                  <div className="content">
                    <form onSubmit={this.onSubmitFilter}>
                      <div className="columns is-desktop">
                        <div className="column">
                          <h6>{messages.label.fromDate}</h6>
                          <DatePicker
                            customInput={<Button.Datepicker error={errors.fromDate} width={245} />}
                            name="fromDate"
                            error={errors.fromDate}
                            value={filter.fromDate}
                            onChange={this.onChangeFilter}
                            width={245}
                            showDisabledMonthNavigation
                            showMonthDropdown={false}
                            showYearDropdown={false}
                          />
                        </div>
                        <div className="column">
                          <h6>{messages.label.toDate}</h6>
                          <DatePicker
                            customInput={<Button.Datepicker error={errors.toDate} width={245} />}
                            name="toDate"
                            error={errors.toDate}
                            value={filter.toDate}
                            onChange={this.onChangeFilter}
                            minDate={moment(filter.fromDate)}
                            width={245}
                            showDisabledMonthNavigation
                            showMonthDropdown={false}
                            showYearDropdown={false}
                          />
                        </div>
                        <div className="column">
                          <h6>{messages.label.fileName}</h6>
                          <Input
                            name="fileName"
                            value={filter.fileName}
                            size="small"
                            onChange={this.onChangeFilter}
                            language={messages}
                          />
                        </div>
                        <div className="column">
                          <h6>&nbsp;</h6>
                          <p className="control">
                            <Button
                              size="small"
                              role="submit"
                              loading={isSearching}
                            >{messages.button.search}</Button>
                          </p>
                        </div>
                      </div>
                    </form>
                  </div>
                </Card.Content>
              </Card>
            </div>
          </div>
          <div className="columns">
            <div className="column is-12">
              <Table fullwidth>
                <Table.Head>
                  <Table.Label label={messages.label.date.toUpperCase()} />
                  <Table.Label label={messages.label.type.toUpperCase()} />
                  <Table.Label label={messages.label.fileName.toUpperCase()} />
                  <Table.Label label={messages.label.status.toUpperCase()} />
                  <Table.Label label={messages.label.failed.toUpperCase()} />
                  <Table.Label label={messages.label.success.toUpperCase()} />
                  <Table.Label label={messages.label.total.toUpperCase()} />
                  <Table.Label label={messages.label.action.toUpperCase()} />
                </Table.Head>
                <Table.Body>
                  {data.content && data.content.length > 0 ? (
                    data.content.map((item, index) => {
                      return(
                        <Table.Values key={index}>
                          <Table.Value>
                            <p style={{minWidth: 150}}>
                              {moment(item.dateCreated).format('DD/MM/YYYY hh:mm:ss A')}
                            </p>
                          </Table.Value>
                          <Table.Value value={ConvertFunc.snakeToSpasiUpperCase(item.documentType)} />
                          <Table.Value value={item.fileName} />
                          <Table.Value value={item.status} />
                          <Table.Value align="center">
                            {parseInt(item.recordFail) > 0 ? (
                              <a onClick={() => history.push('/service/status_upload_bulk/failed/?id='+item.id, {
                                data: {
                                  uploadedBy: item.createdBy,
                                  dateUploaded: item.dateCreated,
                                  approvedBy: item.updatedBy,
                                  approvedDate: item.lastUpdated
                                },
                                filter: {
                                  fromDate: filter.fromDate == null ? null : moment(filter.fromDate).format('DD/MM/YYYY'),
                                  toDate: filter.toDate == null ? null : moment(filter.toDate).format('DD/MM/YYYY'),
                                  fileName: filter.fileName
                                }
                              })}>
                                {item.recordFail}
                              </a>
                            ):(
                              <p>{item.recordFail}</p>
                            )}
                          </Table.Value>
                          <Table.Value align="center" value={item.recordSuccess} />
                          <Table.Value align="center" value={item.totalRecord} />
                          <Table.Value align="center">
                            {item.status == 'COMPLETED' &&
                              <a onClick={() => history.push(`/service/status_upload_bulk/${item.documentType.toLowerCase()}/detail/?id=${item.id}`, {
                                data: {
                                  uploadedBy: item.createdBy,
                                  dateUploaded: item.dateCreated,
                                  approvedBy: item.updatedBy,
                                  approvedDate: item.lastUpdated
                                },
                                filter: {
                                  fromDate: filter.fromDate == null ? null : moment(filter.fromDate).format('DD/MM/YYYY'),
                                  toDate: filter.toDate == null ? null : moment(filter.toDate).format('DD/MM/YYYY'),
                                  fileName: filter.fileName
                                }
                              })}>
                                View Details
                              </a>
                            }
                          </Table.Value>
                        </Table.Values>
                      );
                    })
                  ):(
                    <Table.Values>
                      <Table.Value
                        value={messages.text.empty}
                        colspan="8"
                        bold
                      />
                    </Table.Values>
                  )}
                </Table.Body>
              </Table>
              {data.totalPages > 1 &&
                <Pagination
                  setPages = {(page) => this.setPages(page)}
                  {...data}
                  language={messages}
                />
              }
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

ListStatusUploadFile.propTypes = {
  getList: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  data: uploadFileSelector('data'),
  getListSuccess: uploadFileSelector('getListSuccess'),
  getListFailed: uploadFileSelector('getListFailed'),
  getListLoading: uploadFileSelector('getListLoading')
});

const mapDispatchToProps = dispatch => ({
  getList: (data) => dispatch(getList(data))
});

export default compose(
  withReducerUploadFile,
  withMiddlewareUploadFile,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ListStatusUploadFile);
