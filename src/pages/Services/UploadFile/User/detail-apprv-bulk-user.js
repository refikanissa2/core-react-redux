import React, { Component } from 'react';
import moment from 'moment';

import DetailTask from '@/pages/MyTask/ApprovalMaintenance/detail-myTask';
import language from '@/helpers/messages';
import { Button, Column, ContentLoader } from 'component-ui-web-teravin';
const messages = language.getLanguage();

class DetailApproval extends Component {
  render() {
    const { location, match } = this.props;
    const taskType = match.params.taskType;
    return (
      <DetailTask
        module='user_management'
        title={messages.menu.user[taskType]}
        location={location}
        taskType={taskType}
        isDownload
      >
        {props => {
          const {data = {}, getPendingTaskLoading} = props;
          if(getPendingTaskLoading){
            return(
              <div className="columns">
                <div className='column'>
                  <ContentLoader.Detail />
                </div>
              </div>
            );
          }else{
            return(
              <Column>
                <Column.Content>
                  <Column.Name text={messages.label.fileName}/>
                  <Column.Value>
                    <div style={{display: 'flex', alignItems: 'center'}}>
                      <span style={{marginRight: 15}}>{data.fileName}</span>
                      {data.id &&
                        <Button
                          size="small"
                          loading={props.downloadFileLoading}
                          onClick={() => props.onDownload(data)}
                        ><span>{messages.button.download}</span></Button>
                      }
                    </div>
                  </Column.Value>
                </Column.Content>
                <Column.Content>
                  <Column.Name text={messages.label.modifiedBy}/>
                  <Column.Value>{data.createdBy}</Column.Value>
                </Column.Content>
                <Column.Content>
                  <Column.Name text={messages.label.modifiedDate}/>
                  <Column.Value>{data.dateCreated == null ? '' : moment(data.dateCreated).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
                </Column.Content>
              </Column>
            );
          }
          
        }}
      </DetailTask>
    );
  }
}

export default DetailApproval;
