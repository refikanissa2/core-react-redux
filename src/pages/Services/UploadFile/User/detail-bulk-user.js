import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withReducerUploadFile, withMiddlewareUploadFile } from '@/store/UploadFile/injector';
import { uploadFileSelector } from '@/store/UploadFile/selectors';
import { getDetailFile, exportData } from '@/store/UploadFile/actions';
import moment from 'moment';

import history from '@/routes/history';
import language from '@/helpers/messages';

import HomeBase from '@/pages/home';
import {
  Container, Button, Table, Pagination, Column, Alert,
  UrlQueryFunc,
  ValidationFunc
} from 'component-ui-web-teravin';
const messages = language.getLanguage();

export class DetailBulkUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingPage: false,
      isExporting: false,
      form:{
        code: 'User-FileDetail',
        'parameter':{
          UploadFileId: ''
        },
        userId: this.props.userId
      },
      errors: {
        UploadFileId: ''
      },
      module: 'user_management'
    };
    this.onExport = this.onExport.bind(this);
  }

  componentDidMount() {
    this.setState({ loadingPage: true });
    const {location, dataList} = this.props;
    const {state} = location;
    if(state){
      if(state.filter){
        window.onpopstate = this.onBackButtonBrowser.bind(this, state.filter, dataList.number+1);
      }
    }
    const id = UrlQueryFunc.getQueryString('id');
    const pageOnUrl = UrlQueryFunc.getQueryString('page');
    const {form, module} = this.state;
    form.parameter.UploadFileId = id;
    this.setState({ form: form });
    let page = 0;
    if(pageOnUrl){
      page = parseInt(pageOnUrl);
    }
    var optionData = {
      status: 'list',
      id: id,
      module: module,
      page: page
    };
    this.props.getDetailFile(Object.assign(optionData));
  }

  componentWillReceiveProps(nextProps) {
    const {form} = this.state;
    const {
      getDetailFileSuccess, getDetailFileFailed,
      exportDataSuccess, exportDataFailed
    } = nextProps;
    if(getDetailFileFailed || getDetailFileSuccess){
      this.setState({ loadingPage: false });
    }
    if(exportDataSuccess){
      this.setState({ isExporting: false });
    }
    if(exportDataFailed){
      this.setState({ isExporting: false });
      history.replace(`/service/status_upload_bulk/user/detail?id=${form.parameter.UploadFileId}`);
    }
  }

  onBackButtonBrowser(filter, page){
    history.push('/service/status_upload_bulk?page='+page, {filter: filter});
  }

  setPages(page) {
    const {state} = this.props.location;
    const {form, module} = this.state;
    var optionData = {
      status: 'page',
      page: page+1,
      id: form.parameter.UploadFileId,
      module: module,
      state: state
    };
    this.props.getDetailFile(Object.assign(optionData));
  }

  onExport(){
    this.setState({ isExporting: true });
    const {form, errors} = this.state;
    const {parameter} = form;
    if(ValidationFunc.form(errors, parameter)){
      this.props.exportData(form);
      return;
    }
    this.setState({ errors: {uploadFileId: 'Upload file id not found'}, isExporting: false });
    Alert.error({message: errors.uploadFileId});
  }

  render() {
    const {loadingPage, isExporting} = this.state;
    const {location, data, dataList} = this.props;
    const {state, title} = location;
    let dataUserEdit = {};
    if(state) dataUserEdit = state.data;
    return (
      <HomeBase loader={loadingPage} breadcrumb={location} title={messages.menu.agentManagement.detailFile} headTitle={title}>
        <Container fluid>
          <div className="columns">
            <div className="column is-12">
              <div style={{float: 'right'}}>
                <Button
                  size="small"
                  loading={isExporting}
                  onClick={this.onExport}
                >
                  <span>{messages.button.export}</span>
                </Button>&nbsp;
                <Button
                  size="small"
                  onClick={() => history.push('/service/status_upload_bulk?page='+(dataList.number+1), {filter: state.filter})}
                >{messages.button.back}</Button>
              </div>
            </div>
          </div>
          <div className="columns">
            <div className="column is-12">
              <Table fullwidth>
                <Table.Head>
                  <Table.Label label={messages.label.userId}/>
                  <Table.Label label={messages.label.name}/>
                  <Table.Label label={messages.label.role}/>
                  <Table.Label label={messages.label.mms}/>
                  <Table.Label label={messages.label.status}/>
                </Table.Head>
                <Table.Body>
                  {data.content && data.content.length > 0 &&
                  data.content.map((item, index) => {
                    return(
                      <Table.Values key={index}>
                        <Table.Value align="center" value={item.id}/>
                        <Table.Value align="center" value={item.name}/>
                        <Table.Value align="center" value={item.roleList ? item.roleList.map(x => x.name).join(', ') : ''}/>
                        <Table.Value align="center" value={item.branchCode ? item.branchCode+(item.branchName ? ' - '+item.branchName : '') : ''}/>
                        <Table.Value align="center" value={item.userStatus}/>
                      </Table.Values>
                    );
                  }
                  )}
                </Table.Body>
              </Table>
            </div>
          </div>
          <div className="columns">
            <div className="column is-12">
              <Column>
                <Column.Content isBorder={false}>
                  <Column.Name bold={false} width={120} text={messages.label.uploadedBy}/>
                  <Column.Name bold={false} width={20} text=":"/>
                  <Column.Value>{dataUserEdit.uploadedBy}</Column.Value>
                </Column.Content>
                <Column.Content isBorder={false}>
                  <Column.Name bold={false} width={120} text={messages.label.dateUploaded}/>
                  <Column.Name bold={false} width={20} text=":"/>
                  <Column.Value>{dataUserEdit.dateUploaded == null ? '' : moment(data.dateUploaded).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
                </Column.Content>
                <Column.Content isBorder={false}>
                  <Column.Name bold={false} width={120} text={messages.label.approvedBy}/>
                  <Column.Name bold={false} width={20} text=":"/>
                  <Column.Value>{dataUserEdit.approvedBy}</Column.Value>
                </Column.Content>
                <Column.Content isBorder={false}>
                  <Column.Name bold={false} width={120} text={messages.label.lastApproved}/>
                  <Column.Name bold={false} width={20} text=":"/>
                  <Column.Value>{dataUserEdit.approvedDate == null ? '' : moment(data.approvedDate).format('DD/MM/YYYY hh:mm:ss A')}</Column.Value>
                </Column.Content>
              </Column>
              {data.totalPages > 1 &&
                <Pagination
                  setPages = {(page) => this.setPages(page)}
                  {...data}
                  language={messages}
                />
              }
            </div>
          </div>
        </Container>
      </HomeBase>
    );
  }
}

DetailBulkUser.propTypes = {
  getDetailFile: PropTypes.func,
  exportData: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  userId: uploadFileSelector('userId'),
  getDetailFileSuccess: uploadFileSelector('getDetailFileSuccess'),
  getDetailFileFailed: uploadFileSelector('getDetailFileFailed'),
  data: uploadFileSelector('detailData'),
  exportDataSuccess: uploadFileSelector('exportDataSuccess'),
  exportDataFailed: uploadFileSelector('exportDataFailed'),
  dataList: uploadFileSelector('data')
});

const mapDispatchToProps = dispatch => ({
  getDetailFile: (data) => dispatch(getDetailFile(data)),
  exportData: (data) => dispatch(exportData(data))
});

export default compose(
  withReducerUploadFile,
  withMiddlewareUploadFile,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(DetailBulkUser);
