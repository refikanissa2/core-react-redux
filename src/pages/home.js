import React, { Component } from 'react';
import PropTypes from 'prop-types';

// import Navigation from '@/pages/navigation';
import SideNavigation from '@/pages/sidenav';
import {Layout, Header, Loader, Breadcrumb, Footer} from 'component-ui-web-teravin';

import language from '@/helpers/messages';
import colors from '@/assets/sass/colors.scss';
const messages = language.getLanguage();

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      widthNav: 250,
      marginLeft: 250,
      display: 'none',
      lineWidth: '80%'
    };
    this.openNav = this.openNav.bind(this);
    this.closeNav = this.closeNav.bind(this);
  }

  componentDidMount() {
    var title = messages.project;
    if(this.props.headTitle) title += ' | '+this.props.headTitle;
    document.title = title;
  }

  openNav(){
    this.setState({
      widthNav: 250,
      marginLeft: 250,
      display: 'none',
      lineWidth: '80%'
    });
  }

  closeNav(){
    this.setState({
      widthNav: 0,
      marginLeft: 0,
      display: 'flex',
      lineWidth: '100%'
    });
  }

  render() {
    const {title, breadcrumb, loader} = this.props;
    const {widthNav, marginLeft, display, lineWidth} = this.state;
    return (
      <Layout>
        {/* <Navigation/> */}
        <SideNavigation widthNav={widthNav} closeNav={this.closeNav} />
        <div id="main" style={{marginLeft: marginLeft}}>
          <div style={{paddingLeft: '2.5rem', display: display, alignItems: 'center', height: 60}}>
            <span style={{fontSize:'30px', cursor:'pointer'}} onClick={this.openNav}>&#9776;</span>
            <img src={require('@/assets/img/logo.png')} alt="Logo Teravin" style={{height: 35, paddingLeft: 10}} />
          </div>
          {loader && <Loader show={loader} logo={require('@/assets/img/logo_siera.png')} logoPlaceholder={require('@/assets/img/logo_siera.png')} />}
          {breadcrumb && <Breadcrumb location={breadcrumb} />}
          {title && <Header title={title}/>}
          {this.props.children}
          <Footer lineColor={colors.primary} lineWidth={lineWidth} />
        </div>
      </Layout>
    );
  }
}

Home.propTypes = {
  title: PropTypes.string,
  breadcrumb: PropTypes.object,
  loader: PropTypes.bool,
  headTitle: PropTypes.string
};

export default Home;
