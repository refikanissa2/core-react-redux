import React from 'react';
import Route from 'react-router-dom/Route';
import Redirect from 'react-router-dom/Redirect';
import moment from 'moment';

import {AuthenticationFunc, ConvertFunc, } from 'component-ui-web-teravin';
import Bundle from '@/helpers/Bundle';

const NotFound = (props) => (
  <Bundle load={require('bundle-loader?lazy!../../pages/Errors/404')}>
    {(Comp) => <Comp {...props}/>}
  </Bundle>
);

const PrivateRoute = ({ component: Component ,...rest }) => {
  var status=false;
  var exp = 0;
  var menu = [];
  var timeToken = null, timeNow = null;
  if(AuthenticationFunc.cekDataStorages('teravin')){
    var dataDecrypt = AuthenticationFunc.decryptData('teravin');
    var decoded = AuthenticationFunc.getDecodedJwttoken(dataDecrypt.token);
    status = dataDecrypt.status;
    exp = decoded.exp;
    timeToken= moment(dataDecrypt.expTime).format('DD MMM YYYY HH:mm');
    timeNow= moment().format('DD MMM YYYY HH:mm');
    if(timeToken < timeNow){
      sessionStorage.removeItem('teravin');
      sessionStorage.removeItem('mn');
    }
  }
  else{
    status = false;
    exp = 0;
  }
  if(AuthenticationFunc.cekDataStorages('mn')){
    menu = AuthenticationFunc.getSessionStorages('mn');
    var defaultMenu = {
      code: 'MNU_000',
      inactiveFlag: false,
      index: 0,
      level: 0,
      name: '',
      parentCode: '',
      parentName: ''
    };
    menu.push(defaultMenu);
    var errorMenu = {
      code: 'ERR_000',
      inactiveFlag: false,
      index: 0,
      level: 0,
      name: '500',
      parentCode: '',
      parentName: ''
    };
    menu.push(errorMenu);
  }
  var lang = 'en';
  if(!localStorage.getItem('_locale')){
    localStorage.setItem('_locale', lang);
  }

  if(AuthenticationFunc.cekDataStorages('teravin') && status === true && exp > 0){
    var res = rest.location.pathname.split('/');
    res.splice(0, 1);
    var str_pathname;
    if(res.length > 1){
      str_pathname = res[1];
    }else{
      str_pathname = res[0];
    }
    var last_pathname;
    last_pathname = res[res.length-1];
    if(AuthenticationFunc.cekDataStorages('mn') && menu.map(x => ConvertFunc.spasiToSnakeCase(x.name)).includes(str_pathname)){
      var indexMenu = menu.map(x => ConvertFunc.spasiToSnakeCase(x.name)).indexOf(str_pathname);
      if(menu[indexMenu].viewOnly == true){
        if(last_pathname != 'update' && last_pathname != 'add'){
          return(
            <Route {...rest} render={props => {
              props.location = {viewOnly: true, ...props.location, title: rest.title};
              return(
                <Component {...props} />
              );
            }} />
          );
        }else{
          return(
            <Route path="*" component={NotFound}/>
          );
        }
      }else{
        return(
          <Route {...rest} render={props => {
            props.location = {viewOnly: false, ...props.location, title: rest.title};
            return(
              <Component {...props} />
            );
          }} />
        );
      }
    }else{
      return(
        <Route path="*" component={NotFound}/>
      );
    }
  }else{
    return(
      <Route {...rest} render={props => {
        props.location = {...props.location, title: rest.title};
        return(
          <Redirect to={{
            pathname: '/login',
            search:`?redirect=${props.location.pathname}${props.location.search}`,
            state: { from: props.location }
          }}/>
        );
      }}/>
    );
  }
};

export default PrivateRoute;
