import React from 'react';
import {Loader} from 'component-ui-web-teravin';

class Bundle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mod: null
    };
  }

  componentWillMount() {
    this.load(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.load !== this.props.load) {
      this.load(nextProps);
    }
  }

  load(props) {
    this.setState({
      mod: null
    });
    props.load((mod) => {
      this.setState({
        // handle both es imports and cjs
        mod: mod.default ? mod.default : mod
      });
    });
  }

  render() {
    return this.state.mod ? (
      this.props.children(this.state.mod)
    ) : (
      <Loader show={true}  logo={require('@/assets/img/logo_siera.png')} logoPlaceholder={require('@/assets/img/logo_siera.png')}  />
    );
  }
}

export default Bundle;
