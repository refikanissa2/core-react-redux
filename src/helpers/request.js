import history from '@/routes/history';
import {Alert, AuthenticationFunc} from 'component-ui-web-teravin';
import language from '@/helpers/messages';
const messages = language.getLanguage();

const VALIDATION_ERROR = 'BAS-00001';
const RECORD_NOT_FOUND = 'BAS-00002';
const RECORD_ALREADY_EXIST = 'BAS-00003';
const WF_NOT_FOUND = 'WFE-00001';
const NOT_SUPPORT_TASKTYPE = 'WFE-00002';
const EXIST_OBJECT = 'WFE-00003';
const KEY_NOT_FOUND = 'WFE-00004';
const EXIST_OBJECT_KEY = 'WFE-00005';
const PENDING_TASK_ID_NOT_FOUND = 'WFE-00006';
const PENDING_TASK_PROC_NOT_FOUND = 'WFE-00007';
const PENDING_TASK_PROC_NOT_FOUND_2 = 'WFE-00008';
const USER_ROLE_NOT_FOR_TASK = 'WFE-00009';
const SAME_USER = 'WFE-00010';
const NO_ASSIGNEE = 'WFE-00011';
const USER_NOT_ASSIGNEE = 'WFE-00012';
const FAILED_OBJECT_TO_JSON = 'WFE-90001';
const FAILED_JSONOBJECT_TO_JSONSTRING = 'WFE-90002';
const OBJECT_CLASS_NOT_FOUND = 'WFE-90003';
const FAILED_CONSTRUCT_OBJECT = 'WFE-90004';
const CIF_NOT_FOUND = 'BTS-00001';
const CUST_NOT_FOUND = 'BTS-00002';
const CARD_NOT_FOUND = 'BTS-00003';
const CIF_ACC_NOT_FOUND = 'BTS-00005';
const ACC_TYPE_NOT_FOUN = 'BTS-00006';
const CURR_NOT_FOUND = 'BTS-00007';
const AGENT_ALREADY_EXIST = 'BTS-00008';
const AGENT_NOT_FOUND = 'BTS-00009';
const AGENT_IS_INACTIVE = 'BTS-00010';
const CIF_ID_NOT_FOUND = 'BTS-00011';
const TRX_CODE_NOT_VALID = 'BTS-00012';
const CUST_LIMIT_NOT_FOUND = 'BTS-00013';
const EXIST_AGENT_ID = 'BTS-00016';
const USED_MERCHANT = 'BTS-00017';
const SUB_MERCHANT_NOT_FOUND = 'BTS-00018';
const WRONG_DATE_FORMAT = 'BTS-00019';
const ERROR_PARSING_DATE = 'BTS-00020';
const UPLOAD_MAX_ROW = 'UPL-00001';
const EMPTY_PARAMETER = 'RPT-00001';
const NOT_MATCH_PARAMETER = 'RPT-00002';
const WRONG_PARAMETER = 'RPT-00003';
const REQUEST_NOT_ALLOWED = 'RPT-00004';

const request = {
  handleResponse,
  handleError,
  headers,
  headersWithContentType,
  handleResponseDelete,
  handleResponseStream
};

function handleResponse(response) {
  let json = response.json();
  if (response.status >= 200 && response.status < 300) {
    return json;
  }else if(response.status == 502){
    Alert.error({message: messages.error.expiredToken});
    sessionStorage.removeItem('teravin');
    sessionStorage.removeItem('mn');
    history.push('/login');
    return;
  }else if(response.status == 504){
    history.push('/error/500');
    return json.then(Promise.reject.bind(Promise));
  }else if(response.status == 500){
    history.push('/error/500');
    return json.then(Promise.reject.bind(Promise));
  }else {
    return json.then(Promise.reject.bind(Promise));
  }
}

function handleResponseStream(response) {
  if (response.status >= 200 && response.status < 300) {
    return response.url;
  } else if(response.status == 502){
    Alert.error({message: messages.error.expiredToken});
    sessionStorage.removeItem('teravin');
    sessionStorage.removeItem('mn');
    history.push('/login');
  }else if(response.status == 504){
    history.push('/error/500');
    let json = response.json();
    return json.then(Promise.reject.bind(Promise));
  }else if(response.status == 500){
    history.push('/error/500');
    let json = response.json();
    return json.then(Promise.reject.bind(Promise));
  }else {
    let json = response.json();
    return json.then(Promise.reject.bind(Promise));
  }
}

function handleResponseDelete(response) {
  if (response.status >= 200 && response.status < 300) {
    return {};
  } else if(response.status == 502){
    Alert.error({message: messages.error.expiredToken});
    sessionStorage.removeItem('teravin');
    sessionStorage.removeItem('mn');
    history.push('/login');
  } else {
    return response.json.then(Promise.reject.bind(Promise));
  }
}

function handleError(error,option) {
  let path;
  let code = '';
  if(option){
    path = `/${option.path}`;
    if(option.dataDetail){
      code = option.dataDetail;
    }
  }
  if(error.code === RECORD_NOT_FOUND){
    Alert.error({message: `${messages.error.notFound} ${code}`});
    history.push(path);
  }
  else if(error.code === VALIDATION_ERROR){
    Alert.error({message: messages.error.validationError});
  }
  else if(error.code === RECORD_ALREADY_EXIST){
    Alert.error({message: messages.error.exist});
  }
  else if(error.code === WF_NOT_FOUND){
    Alert.error({message: messages.errorWF.failedStart});
  }
  else if(error.code === NOT_SUPPORT_TASKTYPE){
    Alert.error({message: messages.errorWF.failedStart});
  }
  else if(error.code === EXIST_OBJECT){
    Alert.error({message: messages.errorWF.existObject});
  }
  else if(error.code === KEY_NOT_FOUND){
    Alert.error({message: messages.errorWF.failedStart});
  }
  else if(error.code === EXIST_OBJECT_KEY){
    Alert.error({message: messages.errorWF.existObject});
  }
  else if(error.code === PENDING_TASK_ID_NOT_FOUND){
    Alert.error({message: messages.errorWF.taskNotFound});
  }
  else if(error.code === PENDING_TASK_PROC_NOT_FOUND){
    Alert.error({message: messages.errorWF.taskNotFound});
  }
  else if(error.code === PENDING_TASK_PROC_NOT_FOUND_2){
    Alert.error({message: messages.errorWF.taskNotFound});
  }
  else if(error.code === USER_ROLE_NOT_FOR_TASK){
    Alert.error({message: messages.errorWF.taskNotAvailable});
  }
  else if(error.code === SAME_USER){
    Alert.error({message: messages.errorWF.taskNotAvailable});
  }
  else if(error.code === NO_ASSIGNEE){
    Alert.error({message: messages.errorWF.taskNotAvailable});
  }
  else if(error.code === USER_NOT_ASSIGNEE){
    Alert.error({message: messages.errorWF.taskNotAvailable});
  }
  else if(error.code === FAILED_OBJECT_TO_JSON){
    Alert.error({message: messages.errorWF.errorSystem});
  }
  else if(error.code === FAILED_JSONOBJECT_TO_JSONSTRING){
    Alert.error({message: messages.errorWF.errorSystem});
  }
  else if(error.code === OBJECT_CLASS_NOT_FOUND){
    Alert.error({message: messages.errorWF.errorSystem});
  }
  else if(error.code === FAILED_CONSTRUCT_OBJECT){
    Alert.error({message: messages.errorWF.errorSystem});
    history.goBack();
  }
  else if(error.code === CIF_NOT_FOUND){
    Alert.error({message: messages.error.cifNotFound});
  }
  else if(error.code === CUST_NOT_FOUND){
    Alert.error({message: messages.error.custNotFound});
  }
  else if(error.code === CARD_NOT_FOUND){
    Alert.error({message: messages.error.cardNotFound});
  }
  else if(error.code === CIF_ACC_NOT_FOUND){
    Alert.error({message: messages.error.cifAccNotFound});
  }
  else if(error.code === ACC_TYPE_NOT_FOUN){
    Alert.error({message: messages.error.accTypeNotFound});
  }
  else if(error.code === CURR_NOT_FOUND){
    Alert.error({message: messages.error.currNotFound});
  }
  else if(error.code === AGENT_ALREADY_EXIST){
    Alert.error({message: messages.error.agentExist});
  }
  else if(error.code === AGENT_NOT_FOUND){
    Alert.error({message: messages.error.agentNotFound});
  }
  else if(error.code === AGENT_IS_INACTIVE){
    Alert.error({message: messages.error.agentInactive});
  }
  else if(error.code === CIF_ID_NOT_FOUND){
    Alert.error({message: messages.error.cifIdNotFound});
  }
  else if(error.code === TRX_CODE_NOT_VALID){
    Alert.error({message: messages.error.trxCodeNotFound});
  }
  else if(error.code === CUST_LIMIT_NOT_FOUND){
    Alert.error({message: messages.error.custLimitNotFound});
  }
  else if(error.code === EXIST_AGENT_ID){
    Alert.error({message: messages.error.existAgentId});
  }
  else if(error.code === USED_MERCHANT){
    Alert.error({message: messages.error.usedMerchant});
  }
  else if(error.code === SUB_MERCHANT_NOT_FOUND){
    Alert.error({message: messages.error.subMerchantNotFound});
  }
  else if(error.code === WRONG_DATE_FORMAT){
    Alert.error({message: messages.error.wrongDateFormat});
  }
  else if(error.code === ERROR_PARSING_DATE){
    Alert.error({message: messages.error.parsingDate});
  }
  else if(error.code === UPLOAD_MAX_ROW){
    Alert.error({message: messages.error.uploadMaxRow});
  }
  else if(error.code === EMPTY_PARAMETER){
    Alert.error({message: messages.error.emptyParameter});
  }
  else if(error.code === NOT_MATCH_PARAMETER){
    Alert.error({message: messages.error.notMatchParameter});
  }
  else if(error.code === WRONG_PARAMETER){
    Alert.error({message: messages.error.wrongParameter});
  }
  else if(error.code === REQUEST_NOT_ALLOWED){
    Alert.error({message: messages.error.requestNotAllowed});
  }
  else if(error.status == 500){
    history.push('/error/500');
  }
  else if(error.status == 404){
    Alert.error({message: messages.error.noService});
  }
  else if(error.error == 'invalid_token'){
    Alert.error({message: messages.error.expiredToken});
    sessionStorage.removeItem('teravin');
    sessionStorage.removeItem('mn');
    history.push('/login');
  }
  else if(error.error == 'access_denied'){
    Alert.error({message: messages.error.accessDenied});
  }
  else {
    Alert.error({message:error.message});
  }
}

function headers(){
  var token = AuthenticationFunc.getToken('teravin');
  var cookies = AuthenticationFunc.csrftoken(64);
  const headers = new Headers();

  headers.append('Authorization', `Bearer ${token}`);
  headers.append('X-CSRF-TOKEN', cookies);

  return headers;
}

function headersWithContentType(){
  var token = AuthenticationFunc.getToken('teravin');
  var cookies = AuthenticationFunc.csrftoken(64);

  const headers = new Headers();

  headers.append('Content-Type', 'application/json');
  headers.append('Authorization', `Bearer ${token}`);
  headers.append('X-CSRF-TOKEN', cookies);

  return headers;
}

export default request;
