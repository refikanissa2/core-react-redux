import EN from './en';
import ID from './id';

const language = {
  getLanguage
};

function getLanguage(){
  const lang = localStorage.getItem('_locale');
  let messages = EN;
  if(lang){
    switch(lang){
    case 'en':
      messages = EN;
      break;
    case 'id':
      messages = ID;
      break;
    default: messages = EN;
    }
  }
  return messages;
}

export default language;
