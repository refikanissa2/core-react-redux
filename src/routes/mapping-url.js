import React from 'react';
import Bundle from '@/helpers/Bundle';

const mappingUrl = {
  'home': {
    'title': 'Home',
    'path': '/',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/home'))
  },
  'login': {
    'title': '',
    'path': '/login',
    'private': false,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Auth/index'))
  },
  'errorServer': {
    'title': '',
    'path': '/error/500',
    'private': false,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Errors/500'))
  },
  'myTask': {
    'title': 'My Task',
    'path': '/my_task/approval_maintenance',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/MyTask/ApprovalMaintenance/list-myTask'))
  },
  'financialInstitution': {
    'title': 'Financial Institution',
    'path': '/maintenance/financial_institution',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/FinancialInstitution/list-fi'))
  },
  'detailFinancialInstitution': {
    'title': 'Financial Institution',
    'path': '/maintenance/financial_institution/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/FinancialInstitution/detail-fi'))
  },
  'editFinancialInstitution': {
    'title': 'Financial Institution',
    'path': '/maintenance/financial_institution/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/FinancialInstitution/add-edit-fi'))
  },
  'addFinancialInstitution': {
    'title': 'Financial Institution',
    'path': '/maintenance/financial_institution/add',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/FinancialInstitution/add-edit-fi'))
  },
  'apprvShowFinancialInstitution': {
    'title': 'Financial Institution',
    'path': '/maintenance/financial_institution/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/FinancialInstitution/apprv-show-fi'))
  },
  'detailApprovalFinancialInstitution': {
    'title': 'Financial Institution',
    'path': '/my_task/approval_maintenance/financial_institution/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/FinancialInstitution/detail-apprv-fi'))
  },
  'country': {
    'title': 'Country',
    'path': '/maintenance/country',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Country/list-country'))
  },
  'detailCountry': {
    'title': 'Country',
    'path': '/maintenance/country/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Country/detail-country'))
  },
  'editCountry': {
    'title': 'Country',
    'path': '/maintenance/country/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Country/add-edit-country'))
  },
  'addCountry': {
    'title': 'Country',
    'path': '/maintenance/country/add',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Country/add-edit-country'))
  },
  'apprvShowCountry': {
    'title': 'Country',
    'path': '/maintenance/country/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Country/apprv-show-country'))
  },
  'detailApprovalCountry': {
    'title': 'Country',
    'path': '/my_task/approval_maintenance/country/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Country/apprv-detail-country'))
  },
  'province': {
    'title': 'Province',
    'path': '/maintenance/state',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Province/list-province'))
  },
  'detailProvince': {
    'title': 'Province',
    'path': '/maintenance/state/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Province/detail-province'))
  },
  'editProvince': {
    'title': 'Province',
    'path': '/maintenance/state/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Province/add-edit-province'))
  },
  'addProvince': {
    'title': 'Province',
    'path': '/maintenance/state/add',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Province/add-edit-province'))
  },
  'apprvShowProvince': {
    'title': 'Province',
    'path': '/maintenance/state/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Province/apprv-show-province'))
  },
  'detailApprovalProvince': {
    'title': 'Province',
    'path': '/my_task/approval_maintenance/province/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Province/detail-apprv-province'))
  },
  'city': {
    'title': 'City',
    'path': '/maintenance/city',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/City/list-city'))
  },
  'detailCity': {
    'title': 'City',
    'path': '/maintenance/city/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/City/detail-city'))
  },
  'editCity': {
    'title': 'City',
    'path': '/maintenance/city/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/City/add-edit-city'))
  },
  'addCity': {
    'title': 'City',
    'path': '/maintenance/city/add',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/City/add-edit-city'))
  },
  'apprvShowCity': {
    'title': 'City',
    'path': '/maintenance/city/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/City/apprv-show-city'))
  },
  'detailApprovalCity': {
    'title': 'City',
    'path': '/my_task/approval_maintenance/city/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/City/detail-apprv-city'))
  },
  'district': {
    'title': 'District',
    'path': '/maintenance/district',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/District/list-district'))
  },
  'detailDistrict': {
    'title': 'District',
    'path': '/maintenance/district/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/District/detail-district'))
  },
  'editDistrict': {
    'title': 'District',
    'path': '/maintenance/district/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/District/add-edit-district'))
  },
  'addDistrict': {
    'title': 'District',
    'path': '/maintenance/district/add',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/District/add-edit-district'))
  },
  'apprvShowDistrict': {
    'title': 'District',
    'path': '/maintenance/district/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/District/apprv-show-district'))
  },
  'detailApprovalDistrict': {
    'title': 'District',
    'path': '/my_task/approval_maintenance/district/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/District/detail-apprv-district'))
  },
  'subDistrict': {
    'title': 'Sub District',
    'path': '/maintenance/sub_district',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SubDistrict/list-subDistrict'))
  },
  'detailSubDistrict': {
    'title': 'Sub District',
    'path': '/maintenance/sub_district/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SubDistrict/detail-subDistrict'))
  },
  'editSubDistrict': {
    'title': 'Sub District',
    'path': '/maintenance/sub_district/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SubDistrict/add-edit-subDistrict'))
  },
  'addSubDistrict': {
    'title': 'Sub District',
    'path': '/maintenance/sub_district/add',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SubDistrict/add-edit-subDistrict'))
  },
  'apprvShowSubDistrict': {
    'title': 'Sub District',
    'path': '/maintenance/sub_district/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SubDistrict/apprv-show-subDistrict'))
  },
  'detailApprovalSubDistrict': {
    'title': 'Sub District',
    'path': '/my_task/approval_maintenance/sub_district/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SubDistrict/detail-apprv-subDistrict'))
  },
  'branch': {
    'title': 'Branch',
    'path': '/maintenance/branch',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Branch/list-branch'))
  },
  'detailBranch': {
    'title': 'Branch',
    'path': '/maintenance/branch/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Branch/detail-branch'))
  },
  'editBranch': {
    'title': 'Branch',
    'path': '/maintenance/branch/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Branch/add-edit-branch'))
  },
  'addBranch': {
    'title': 'Branch',
    'path': '/maintenance/branch/add',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Branch/add-edit-branch'))
  },
  'apprvShowBranch': {
    'title': 'Branch',
    'path': '/maintenance/branch/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Branch/apprv-show-branch'))
  },
  'detailApprovalBranch': {
    'title': 'Branch',
    'path': '/my_task/approval_maintenance/branch/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/Branch/detail-apprv-branch'))
  },
  'systemConfiguration': {
    'title': 'System Configuration',
    'path': '/maintenance/system_configuration',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SystemConfiguration/list-sysConfig'))
  },
  'detailSystemConfiguration': {
    'title': 'System Configuration',
    'path': '/maintenance/system_configuration/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SystemConfiguration/detail-sysConfig'))
  },
  'editSystemConfiguration': {
    'title': 'System Configuration',
    'path': '/maintenance/system_configuration/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SystemConfiguration/edit-sysConfig'))
  },
  'apprvShowSystemConfiguration': {
    'title': 'System Configuration',
    'path': '/maintenance/system_configuration/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SystemConfiguration/apprv-show-sysConfig'))
  },
  'detailApprovalSystemConfiguration': {
    'title': 'System Configuration',
    'path': '/my_task/approval_maintenance/sys_config/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/SystemConfiguration/detail-apprv-sysConfig'))
  },
  'transactionErrorCode': {
    'title': 'Transaction Error Code',
    'path': '/maintenance/transaction_error_code',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/TransactionErrorCode/list-tec'))
  },
  'apprvShowTransactionErrorCode': {
    'title': 'Transaction Error Code',
    'path': '/maintenance/transaction_error_code/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/TransactionErrorCode/apprv-show-tec'))
  },
  'detailApprovalTransactionErrorCode': {
    'title': 'Transaction Error Code',
    'path': '/my_task/approval_maintenance/trx_error_code/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Maintenance/TransactionErrorCode/detail-apprv-tec'))
  },
  'user': {
    'title': 'User Management',
    'path': '/security/user',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/User/list-user'))
  },
  'detailUser': {
    'title': 'User Management',
    'path': '/security/user/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/User/detail-user'))
  },
  'editUser': {
    'title': 'User Management',
    'path': '/security/user/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/User/add-edit-user'))
  },
  'addUser': {
    'title': 'User Management',
    'path': '/security/user/add',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/User/add-edit-user'))
  },
  'apprvShowUser': {
    'title': 'User Management',
    'path': '/security/user/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/User/apprv-show-user'))
  },
  'detailApprovalUser': {
    'title': 'User',
    'path': '/my_task/approval_maintenance/user/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/User/detail-apprv-user'))
  },
  'apprvAddBulkUserManagement': {
    'title': 'User Management',
    'path': '/my_task/approval_maintenance/user_management/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Services/UploadFile/User/detail-apprv-bulk-user'))
  },
  'role': {
    'title': 'Role Management',
    'path': '/security/role',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/Role/list-role'))
  },
  'detailRole': {
    'title': 'Role Management',
    'path': '/security/role/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/Role/detail-role'))
  },
  'editRole': {
    'title': 'Role Management',
    'path': '/security/role/update',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/Role/add-edit-role'))
  },
  'addRole': {
    'title': 'Role Management',
    'path': '/security/role/add',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/Role/add-edit-role'))
  },
  'apprvShowRole': {
    'title': 'Role Management',
    'path': '/security/role/showApproval',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/Role/apprv-show-role'))
  },
  'detailApprovalRole': {
    'title': 'Role Management',
    'path': '/my_task/approval_maintenance/role/:taskType',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Security/Role/detail-apprv-role'))
  },
  'reportStatus': {
    'title': 'Report Status',
    'path': '/report/report_status',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Report/ReportStatus/list-rs'))
  },
  'customerRegistered': {
    'title': 'Report Customer Registered',
    'path': '/report/registered_customer',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Report/RegisteredCustomer/form-cr'))
  },
  'auditTrail': {
    'title': 'Audit Trail',
    'path': '/report/audit_trail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Report/AuditTrail/containers/list-at'))
  },
  'detailAuditTrail': {
    'title': 'Audit Trail',
    'path': '/report/audit_trail/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Report/AuditTrail/containers/detail-at'))
  },
  'customerManagement': {
    'title': 'Customer Management',
    'path': '/customer/customer_management',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Customer/CustomerManagement/list-cm'))
  },
  'detailCustomerManagement': {
    'title': 'Customer Management',
    'path': '/customer/customer_management/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Customer/CustomerManagement/detail-cm'))
  },
  'listUploadedFile':{
    'title': 'Upload File',
    'path': '/service/status_upload_bulk',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Services/UploadFile/list-suf'))
  },
  'failedUploadedFile':{
    'title': 'Upload File',
    'path': '/service/status_upload_bulk/failed',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Services/UploadFile/list-failed-suf'))
  },
  'detailBulkUser': {
    'title': 'Upload File',
    'path': '/service/status_upload_bulk/user/detail',
    'private': true,
    'exact': true,
    'component': getComponent(require('bundle-loader?lazy!../pages/Services/UploadFile/User/detail-bulk-user'))
  }
};

function getComponent(component){
  return (props) => (
    <Bundle load={component}>
      {(Comp) => <Comp {...props}/>}
    </Bundle>
  );
}

export default mappingUrl;
