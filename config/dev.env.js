var merge = require('webpack-merge')
var prodEnv = require('./prod.env')
var PORT = require('./port').PORT

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  DESC_ENV: '"development"',
  API_ENDPOINT: '"http://localhost:'+PORT+'/api"',
  // API_ENDPOINT2: '"http://localhost:'+PORT+'/logindata"',
})
