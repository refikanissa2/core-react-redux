module.exports = {
  NODE_ENV: '"production"',
  BUILD_ENV: '"production"',
  DESC_ENV: '"productionRelative"',
  PATH_ENDPOINT: '"/api/v1"',
}
