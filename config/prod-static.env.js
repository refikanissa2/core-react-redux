module.exports = {
  NODE_ENV: '"production"',
  BUILD_ENV: '"production"',
  DESC_ENV: '"productionStatic"',
  API_ENDPOINT: '"https://2itpacabkd.execute-api.ap-southeast-1.amazonaws.com"',
}
